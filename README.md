## Wordpress bare

HTML prototyp
Denna återfinns inuti ./src/html/ och innehåller endast nödvändig frontend-kod och filer.

Wordpress
Allt wordpressrelaterat finns inuti /src/wp/.
En ren installation av wordpress kopieras in i ./src/wp/core/.
Mappen är som standard helt ignorerad från git.

Mappen wp-content är flyttad till ./src/wp/custom.
Här finns tema, plugins, översättning och uploads.
(Mappen ./core/wp-content ignoreras helt.)

Git kommer att synka allt inom ./custom förutom ./custom/uploads.
Grafik och bilder som är viktiga för design bör därför finnas med i front-end.

För detaljerad konfiguration av wordpress se:
./src/wp/wp-config.php
./src/wp/config/dev-config.php
./src/wp/config/staging-config.php
./src/wp/config/production-config.php

(Efter installation av wordpress så kan det vara nödvändigt att flytta den genererade databas-konfigurationen till lämplig config/config.php fil).