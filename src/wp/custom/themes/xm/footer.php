<?php

$footerMenuItems = wp_get_nav_menu_items('Footer menu');
$footerMenuRightItems = wp_get_nav_menu_items('Footer menu right');

?>

<footer class="site-footer">

    <?php

    $hide_get_started_array = get_field('hide_get_started', 'options');

    $slug = get_post_field('post_name', get_post());
    $current_post = get_post();
    $current_id = $current_post->ID;

    if (in_array($current_id, $hide_get_started_array) == false) {

        ?>

        <section class="getStartedLine">
            <div class="container container-lg">
                <div class="row">
                    <div class="col-md-8 text-center text-md-left">
                        <?php the_field('get_started_box', 'options'); ?>
                    </div>
                    <div class="col-md-4 text-center text-md-right">

                        <?php

                        $link = get_field('get_started_button', 'options');

                        $id = url_to_postid(esc_url($link['url']));
                        $page = get_page_by_path($link['url']);


                        if ($link) :

                            $link_url = $link['url'];
                            $link_title = $link['title'];

                            ?>

                            <a href="<?php echo esc_url($link_url); ?>"
                               class="btn btn-primary mt-3 mt-md-0 btn-xl"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </section>


    <?php } else { ?>

        <div class="mb-5"></div>

    <?php } ?>

    <div class="container">

        <div class="row">
            <div class="col-12 col-md-3 text-center text-md-left">
                <div class="info-col-inner">
                    <a href="<?php echo home_url(); ?>" class="logo"><img
                                src="<?php echo get_template_directory_uri(); ?>/assets/img/logo_black.svg"
                                loading="lazy" alt="xmreality.com"/></a>

                    <div class="mt-4">
                        <?php the_field('first_column', 'options'); ?>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-3 text-center text-md-left">
                <div class="navigation-col-inner">
                    <h4>Visit us at</h4>

                    <div class="social-links">
                        <a href="https://www.facebook.com/pages/XMReality-AB/341239405910594" target="_blank"><i
                                    class="fab fa-facebook-f"></i><span>Facebook</span></a>
                        <a href="https://www.linkedin.com/company/xmreality" target="_blank"><i
                                    class="fab fa-linkedin-in"></i>LinkedIn</a>
                        <a href="https://www.youtube.com/user/xmreality" target="_blank"><i class="fab fa-youtube"></i>Youtube</a>
                        <a href="https://twitter.com/xmreality" target="_blank"><i
                                    class="fab fa-twitter"></i>Twitter</a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-3 text-center text-md-left">
                <div class="navigation-col-inner">
                    <?php the_field('third_column', 'options'); ?>
                </div>
            </div>

            <div class="col-12 col-md-3 text-center text-md-left">
                <div class="newsletter-col-inner">
                    <?php the_field('right_column', 'options'); ?>
                </div>
            </div>
        </div>
    </div>


    <div class="container disclaimer">
        <div class="row">
            <div class="col-12 col-md-4 text-center text-md-left">
                <p><?php the_field('copyright', 'options'); ?></p>
            </div>
            <div class="col-12 col-md-8 mt-md-0 mt-2 text-center text-md-right ">

                <?php
                $legalMenuItems = wp_get_nav_menu_items('bottom menu');
                ?>

                <p>
                    <?php if ($legalMenuItems && count($legalMenuItems) > 0) : ?>
                        <?php foreach ($legalMenuItems as $legalMenuItem) : ?>

                            <a href="<?php echo $legalMenuItem->url; ?>"
                               class="mr-3 ml-0 ml-md-3 mr-md-0"><?php echo $legalMenuItem->title; ?></a>

                        <?php endforeach; ?>
                    <?php endif; ?>
                </p>

            </div>
        </div>
    </div>

    <div id="cookie-bar" class="cookie-bar">
        <div class="container container-cookie">
            <div class="d-lg-inline mb-3 mt-3 mb-lg-0 text-center text-lg-left">

                <div class="cookie-accept-information">
                    <div class="row align-items-center">
                        <div class="col-lg-10"><?php the_field('cookie_information', 'options'); ?></div>
                        <div class="col-lg-2">
                            <button class="btn btn-primary ml-lg-4 mt-3 cookie-opt-in-button">Accept</button>
                        </div>
                    </div>
                </div>
                <div class="cookie-decline-information">
                    <div class="row align-items-center">
                        <div class="col-lg-10"><?php the_field('cookie_information', 'options'); ?></div>
                        <div class="col-lg-2 mt-3 mt-lg-0">
                            <button class="btn btn-outline-primary cookie-opt-out-button">Revoke</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>


<?php //require 'partials/component/modals/newsletter.php'; ?>
<?php global $post; ?>
<?php if (!(is_page(264) || $post->post_parent == 264)) : ?>

    <script>
        window.Ebbot = {
            botId: 'ebvvas2mvdqhquzeo12ndk3zsleovp',
        };
    </script>
    <script>!function (t) {
            var e = "init-js-widget";
            if (!t.getElementById(e)) {
                var i = t.createElement("script");
                i.id = e, i.src = "https://ebbot-v2.storage.googleapis.com/ebbot-web/init.js?t=" + Math.random(), t.querySelector("body").appendChild(i)
            }
        }(document);</script>

<?php endif; ?>

<?php wp_footer(); ?>
