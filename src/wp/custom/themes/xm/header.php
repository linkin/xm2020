<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="google-site-verification" content="jgSKEpSeQrqm_pug6BZAkaxyyyaZaVfb1i5Wthi3dQQ"/>
  <meta name="facebook-domain-verification" content="p8mljle4jy1j0v7pn128xqn2da6tvi" />

  <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png"/>

	<?php wp_head(); ?>

  <script type="text/javascript">
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
  </script>



</head>

  <?php if ( is_page_template( 'template-sub-menu.php' ) ) { ?>
  <body data-spy="scroll" data-target="#navbar-example3" data-offset="137">
  <?php } else {  ?>
  <body>
  <?php } ?>

  <?php

  $siteheaderclass = "invert";
  if (get_field('invertMenu')) {
  	$siteheaderclass = "";
  }

  ?>

  <header class="site-header <?php echo $siteheaderclass; ?>">

    <div class="inner-top-menu d-none d-lg-block">
      <div class="container container-full">
      <div class="row align-items-center no-gutters">
        <?php
          $topMenuItems = wp_get_nav_menu_items('Top menu');
        ?>

        <div class="col-12 col-lg site-navigation-extra pt-0">
          <ul class="top-menu justify-content-lg-end text-center">

              <?php if ($topMenuItems) : ?>
                <?php if (count($topMenuItems) > 0) : ?>
                <?php foreach ($topMenuItems as $topMenuItem) : ?>

                  <li class="nav-item">
                      <a href="<?php echo $topMenuItem->url; ?>" class="nav-link"><?php echo $topMenuItem->title; ?></a>
                  </li>

                <?php endforeach; ?>
              <?php endif; ?>
            <?php endif; ?>

          </ul>

        </div>
      </div>
    </div>
    </div>

    <div class="inner">
      <div class="container container-full">
        <div class="row align-items-center no-gutters">

          <div class="col-12 col-lg">
            <button id="menuToggle" class="navicon float-right d-lg-none"><span></span></button>
            <a href="<?php echo home_url(); ?>" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.svg" loading="lazy" alt="xmreality.com" /></a>
          </div>

          <div class="site-navigation col-12 col-lg-8">



          <?php
            $menuItems = wp_get_nav_menu_items('Main menu');
          ?>

          <?php if (count($menuItems) > 0) : ?>


            <ul class="nav justify-content-center">

  						<?php foreach ($menuItems as $menuItem) : ?>
  							<?php if ($menuItem->menu_item_parent == 0) : ?>

  								<?php
  								$subItems = array_filter($menuItems, function ($item) use ($menuItem) {
  									return $item->menu_item_parent == $menuItem->ID;
  								});
  								?>

                  <li class="nav-item <?php if (count($subItems) > 0) { echo "has-menu"; } ?>">
                  <a href="<?php echo $menuItem->url; ?>" class="nav-link"><?php echo $menuItem->title; ?></a>


                  <?php if (count($subItems) > 0) { ?>
                  <a href="#" class="submenu-arrow"></a>
                  <?php } ?>


                  <ul class="sub-menu" aria-labelledby="navbarDropdownProduct">

  										<?php foreach ($subItems as $subItem) : ?>

  											<?php
  											$subSubItems = array_filter($menuItems, function ($item) use ($subItem) {
  												return $item->menu_item_parent == $subItem->ID;
  											});
  											?>

  											<?php switch ($subItem->object) :
  												case 'page':
  												case 'custom':
  											?>

                        <li>

                          <?php
                            $clickable = get_field('clickable', $subItem);
                          ?>

                          <a class="<?php echo get_field('icon', $subItem); ?>" href="<?php echo $subItem->url ?>">
														<?php echo $subItem->title; ?><span><?php echo get_field('description', $subItem); ?></span>
                          </a>

                        </li>

                        <?php
  											break;
  											case 'nav_menu_box':
                        ?>

                          <li>
                            <div class="special text-center"><?php echo get_post_field('post_content', $subItem->object_id); ?></div>
                          </li>

  											<?php
  											break;
  											endswitch; ?>
  										<?php endforeach; ?>
                    </ul>
                  </li>

  							<?php endif; ?>
  						<?php endforeach; ?>
            </ul>


            <?php endif; ?>

            <div class="inner-top-menu d-lg-none">
                <?php
                  $topMenuItems = wp_get_nav_menu_items('Top menu');
                ?>

                <ul class="top-menu">

                    <?php if ($topMenuItems) : ?>
                      <?php if (count($topMenuItems) > 0) : ?>
                      <?php foreach ($topMenuItems as $topMenuItem) : ?>

                        <li class="nav-item">
                            <a href="<?php echo $topMenuItem->url; ?>" class="nav-link"><?php echo $topMenuItem->title; ?></a>
                        </li>

                      <?php endforeach; ?>
                    <?php endif; ?>
                  <?php endif; ?>

                </ul>
            </div>


            <div class="d-lg-none">
              <div class="site-header-contact">
                <p>XMReality<br/><a href="tel:4613-211110">+46 (0) 13 21 11 10</a></p>
                <p><a href="mailto:info@xmreality.com">info@xmreality.com</a></p>
                <p><strong><a href=""><u>For more contacts click here</u></a></strong></p>
              </div>
            </div>
          </div>

  				<?php
            $sideMenuItems = wp_get_nav_menu_items('Side menu');
  				?>

          <div class="col-12 col-lg site-navigation-extra pt-0">
              <ul class="nav justify-content-lg-end text-center">

                <?php if ($sideMenuItems) : ?>
                  <?php if (count($sideMenuItems) > 0) : ?>
    							<?php foreach ($sideMenuItems as $sideMenuItem) : ?>
    								<?php

    								switch (get_field('appearance', $sideMenuItem->ID)) :

    									case 'link':

    								?>

                      <li class="nav-item">
                          <a href="<?php echo $sideMenuItem->url; ?>" target="_blank" class="nav-link"><?php echo $sideMenuItem->title; ?></a>
                      </li>

  									<?php

  									break;

  									case 'button':
                    ?>

                      <li class="ml-lg-2">
                          <button class="btn btn-primary btn-block" data-toggle="modal" data-target="<?php echo $sideMenuItem->url; ?>"><?php echo $sideMenuItem->title; ?></button>
                      </li>

  									<?php

  									break;

  									case 'buttonLink':

  									?>

                        <li class="ml-lg-2">
                            <a class="btn btn-primary btn-block" href="<?php echo $sideMenuItem->url; ?>"><?php echo $sideMenuItem->title; ?></a>
                        </li>

										<?php

										break;
								    endswitch;

                    ?>
    							<?php endforeach; ?>
    	          <?php endif; ?>
              <?php endif; ?>

          </ul>

        </div>
      </div>
    </div>
  </div>

</header>
