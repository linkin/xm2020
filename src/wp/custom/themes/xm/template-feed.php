<?php

/*
Template Name: Feed Archive
*/

?>
<?php

$title = get_the_title();
$smallTitle = get_field('small_title');
$lead = get_field('lead');

if (get_field('alt_title')) {
    $title = get_field('alt_title');
}

?>

<?php get_header(); ?>
<?php if (!post_password_required()) { ?>


    <div class="" id="">

        <div class="page-cover article-cover full-width-graphics no-cover-image lightblue">
            <div class="cover-content">
                <div class="container">

                    <div class="row row-lg">
                        <div class="col-md-7">

                            <?php if ($title) : ?>
                                <h1><?php echo $title; ?></h1>
                            <?php endif; ?>

                            <?php if (get_field('lead')) : ?>
                                <div class="intro mb-4">
                                    <div class="lead">
                                        <?php echo get_field('lead'); ?>
                                    </div>
                                </div>
                            <?php
                            endif;
                            ?>

                        </div>
                    </div>


                    <div class="graphic-container">
                        <div class="graphics">
                            <div id="parallax5" class="square yellow medium green pos1"></div>
                            <div id="parallax6" class="square pink small blue pos2"></div>
                            <div id="parallax7" class="square small blue pos2"></div>
                            <div id="parallax8" class="square small lightblue pos2"></div>
                            <div class="dotts dottsarticle"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="has-header" id="site-content">
            <div class="container">
                <div class="section topMarginNone">
                    <div class="">
                        <div class="row">
                            <?php

                            $post_type = get_field('post_type', get_the_ID());
                            $amount = get_field('amount', get_the_ID());
                            $open_in_popup = get_field('open_in_popup', get_the_ID());

                            $args = array('post_type' => $post_type, 'posts_per_page' => $amount);
                            $the_query = new WP_Query($args);
                            ?>
                            <?php if ($the_query->have_posts()) : ?>
                                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                                    <div class="col-lg-4 mb-5">

                                        <?php

                                        $customer = get_field('customer');

                                        if ($customer) {

                                        $customer_logo = get_field('black_logotype', $customer->ID)['sizes']['medium'];
                                        $customer_name = get_the_title($customer->ID);

                                        }

                                        $imagesize = 'big';
                                        $image = '';
                                        $img_css = '';

                                        if (get_field('thumbnail_background') === 'image') {
                                            $image = get_field('thumbnail_image');
                                        }

                                        $link = get_permalink();
                                        $category = '';
                                        $gray_label = '';
                                        $title = get_the_title();
                                        $excerpt = excerpt(13);

                                        $terms = get_the_terms(get_the_ID(), 'story_category');

                                        if ($terms && !is_wp_error($terms)) {
                                            $categories = array();

                                            foreach ($terms as $term) {
                                                $categories[] = $term->name;
                                            }

                                            $category = join(", ", $categories);
                                        }

                                        if ($post_type == 'event') {
                                            $category = get_field('date', get_the_ID());
                                            $imagesize = 'logotype';
                                        }

                                        require get_template_directory() . '/partials/component/article-card.php';

                                        if ($open_in_popup) : ?>
                                            <div class="modal fade-scale" id="feedModal-<?php echo get_the_ID(); ?>"
                                                 tabindex="-1" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-top">
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <i class="fal fa-times"></i>
                                                            </button>

                                                        </div>
                                                        <div class="modal-body mt-5">

                                                            <?php if (is_array($image)) : ?>
                                                                <div class="popup-logotype">

                                                                    <?php
                                                                    echo wp_get_attachment_image(
                                                                        $image['ID'],
                                                                        'medium',
                                                                        '',
                                                                        array(
                                                                            'class' => 'img-fluid image' . $img_css,
                                                                            'srcset' => $image['sizes']['large'] . ' 1024w,' . $image['sizes']['medium_large'] . ' 768w,' . $image['sizes']['medium'] . ' 300w',
                                                                            'sizes' => '(min-width: 991px) 33vw, 90vw'
                                                                        )
                                                                    );
                                                                    ?>

                                                                </div>
                                                            <?php endif; ?>


                                                            <p class="label">
                                                            <span class="category">
                                                              <?php echo $category; ?>
                                                              </span>
                                                            </p>
                                                            <h1><?php echo $title; ?></h1>
                                                            <?php the_content(); ?>
                                                            <div>

                                                            <?php if ($post_type == 'event' && get_field('links_title')) : ?>
                                                              <h4 class="mt-4"><?php echo get_field('links_title') ?></h4>
                                                            <?php endif; ?>

                                                            <?php if ($post_type == 'event' && get_field('link_list')) : ?>
                                                              <div class="list <?php if (!get_field('links_title')){ echo "no-headline"; }  ?>">
                                                                <?php
                                                                  if( have_rows('link_list') ):
                                                                  while( have_rows('link_list') ) : the_row();
                                                                 ?>
                                                                  <a class="<?php if(get_sub_field('link')['target'] == "_blank"){ echo "external"; }  ?>" href="<?php echo get_sub_field('link')['url'];  ?>" target="<?php echo get_sub_field('link')['target'];  ?>">
                                                                    <?php echo get_sub_field('link')['title'];  ?>
                                                                  </a>
                                                                <?php
                                                                  endwhile;
                                                                  endif;
                                                                ?>
                                                              </div>
                                                            <?php endif; ?>
                                                          </div>
                                                          

                                                            <?php if ($post_type == 'event' && get_field('signup_button')) : ?>
                                                                <a class="btn btn-primary" target="_blank" href="<?php echo get_field('signup_button')['url'];  ?>"><?php echo get_field('signup_button')['title'];  ?></a>
                                                            <?php endif; ?>

                                                        </div>

                                                        <div class="modal-footer justify-content-center">
                                                            <button type="button" class="btn btn-link"
                                                                    data-dismiss="modal"><i
                                                                        class="fal fa-times mr-1"></i> Close
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php
                                        endif;

                                        ?>

                                        <?php wp_reset_postdata(); ?>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


<?php } else { ?>
    <div class="container text-center mt-5 mb-5 pb-5 pt-5">
        <?php echo get_the_password_form(); ?>
    </div>
<?php } ?>

<?php get_footer();
