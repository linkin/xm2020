<?php

/*
Template Name: Whitepapers - archive
*/

?>
<?php

$title = get_the_title();
$smallTitle = get_field('small_title');
$lead = get_field('lead');

if ( get_field('alt_title') ) {
	$title = get_field('alt_title');
}

?>

<?php get_header(); ?>
	<?php if ( !post_password_required() ) {  ?>


		<div class="" id="">

	    <div class="page-cover article-cover no-cover-image lightblue">
	    	<div class="cover-content">
	        <div class="container">

	            <div class="row row-lg">
	              <div class="col-md-7">

	              <?php if ($title) : ?>
	                <h1><?php echo esc_html( $title ); ?></h1>
	              <?php endif; ?>

		            <?php  if(get_field('lead')) : ?>
			            <div class="intro mb-4">
			              <div class="lead">
			                <?php echo get_field('lead'); ?>
			              </div>
			            </div>
		            <?php
		            endif;
		            ?>

	        		</div>
	        	</div>


						<div class="graphic-container">
							<div class="graphics">
								<div id="parallax5" class="square yellow medium green pos1"></div>
								<div id="parallax6" class="square pink small blue pos2"></div>
								<div id="parallax7" class="square small blue pos2"></div>
								<div id="parallax8" class="square small lightblue pos2"></div>
	              <div class="dotts dottsarticle"></div>
							</div>
						</div>

	    		</div>
	    	</div>
	    </div>



			<div class="has-header" id="site-content">
			<div class="container">
			<div class="section topMarginNone">
				<div class="content-list">

					<div class="row">
						<?php
						$args = array( 'post_type' => 'whitepaper', 'posts_per_page' => 10 );
						$the_query = new WP_Query( $args );
						?>
						<?php if ( $the_query->have_posts() ) : ?>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

							<?php
								$customer = get_field('customer');
								$link = get_the_permalink();
								$image = get_field('image')['sizes']['large'];
								$title = get_the_title();
							?>


                        <?php
						$url =  get_permalink();
						$target = false;
						if (get_field('external_url')) {
							$url = get_field('external_url');
							$target = true;
						}
						?>

							<div class="col-lg-6">
                                <a href="<?php echo $url ?>" <?php echo ($target) ? 'target="_blank"' : '' ?>>
									<div class="row">
										<div class="col-5">
											<img src="<?php echo esc_url( get_field('image')['sizes']['medium'] ); ?>" loading="lazy" alt="<?php echo esc_html( get_field('image')['alt']); ?>" class="shaddow" />
										</div>
										<div class="col-7">
											<h4><?php echo esc_html( get_the_title() ); ?></h4>
											<p><?php echo excerpt(20); ?></p>
											<p class="arrow">Download whitepaper</p>
										</div>
									</div>
								</a>
							</div>

						<?php wp_reset_postdata(); ?>
						<?php endwhile; ?>
						<?php endif; ?>
					</div>
			  </div>


				</div>
		  </div>
		</div>
	</div>



	<?php }else{ ?>
			<div class="container text-center mt-5 mb-5 pb-5 pt-5">
				<?php echo get_the_password_form(); ?>
			</div>
	<?php } ?>

<?php get_footer();
