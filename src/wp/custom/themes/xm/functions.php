<?php

require_once 'api/exception/restexception.php';


define('XM_API_URL', "https://server-v6-demo.xmreality.com/");
define('XM_API_URL_PREFIX', "api/v9/");
define('XM_API_USERNAME', "business.admin.dev@xmreality.se");
define('XM_API_PASSWORD', "PIvjyPPuYz058E8fMan9");

/* Configuration */

function clean_setup () {
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wp_shortlink_wp_head');
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10);
    remove_action('wp_head', 'print_emoji_detection_script', 7 );
    remove_action('wp_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
    add_filter( 'emoji_svg_url', '__return_false' );
    add_filter('the_generator', '__return_false');
}
add_action('after_setup_theme', 'clean_setup');

//Security
require_once 'inc/config/remove-generator.php';
require_once 'inc/config/remove-comments.php';

//Ajax
require_once 'inc/config/ajax-url.php';

//Menu
require_once 'inc/config/register-menu.php';


/* Actions */
require_once 'inc/actions/ajax_actions.php';

require_once 'inc/config/custom_post_types.php';


function my_deregister_scripts(){
  wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );


function my_default_script_enqueue() {
	$theme = wp_get_theme();

	// Theme / CSS
	wp_enqueue_style('style', get_template_directory_uri() . '/css/main.min.css',array(), filemtime(get_template_directory() . '/css/main.min.css'));
	// Theme / JS
	wp_enqueue_script('scripts-js', get_template_directory_uri() . '/js/main.js', array('jquery'), $theme->get( 'Version' ), true );


    // Third party libraries / CSS
    wp_enqueue_script('tooltipster', get_template_directory_uri() . '/js/tooltipster.bundle.min.js', array(),'1.0.0', true);

    // Third party libraries / JS
    wp_enqueue_script('slick-js', get_template_directory_uri() . '/js/lib/slick.min.js', array('jquery'),'1.8.1', true);

    wp_enqueue_script('viewportchecker-js', get_template_directory_uri() . '/js/jquery.viewportchecker.min.js', array(),'1.0.0', true);
    wp_enqueue_script('popper-js', get_template_directory_uri() . '/js/lib/popper.min.js', array('jquery'), '1.14.0', true);
    wp_enqueue_script('bs-js', get_template_directory_uri() . '/js/lib/bootstrap.min.js', array(), '4.1.0', true);

    wp_enqueue_script('gsap', get_template_directory_uri() . '/js/lib/gsap.min.js', array(), '3.2.4', true);
    wp_enqueue_script('ScrollMagic', get_template_directory_uri() . '/js/lib/ScrollMagic.min.js', array(), '2.0.7', true);
    wp_enqueue_script('ScrollMagicAnimation', get_template_directory_uri() . '/js/lib/animation.gsap.min.js', array(), '2.0.7', true);

	// QR code JS not exsisting in theme.
    // wp_enqueue_script('qrcode-js', get_template_directory_uri() . '/js/qrcode.min.js', array(), '1.0.0', false);
}
add_action( 'wp_enqueue_scripts', 'my_default_script_enqueue');



function add_additional_class_on_a($classes, $item, $args)
{
    if (isset($args->add_a_class)) {
        $classes['class'] = $args->add_a_class;
    }
    return $classes;
}
add_filter('nav_menu_link_attributes', 'add_additional_class_on_a', 1, 3);




function my_default_theme_setup() {
  add_theme_support( 'title-tag' );
}
add_action('init', 'my_default_theme_setup');


if( function_exists('acf_add_options_page') ) {
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Site settings',
		'menu_title' 	=> 'Site settings',
		'parent_slug' 	=> 'options-general.php',
	));
}


add_shortcode('block', 'myfunction');
function myfunction($attr, $content, $shortcode) {
 return '<div class="highlighted-block">'.$content.'</div>';
}



//GET TOP PAGE ID
function get_top_page_id() {
	global $post;
	if ($post->post_parent) {
		$ancestors = array_reverse(get_post_ancestors($post->ID));
		return $ancestors[0];
	}
	return $post->ID;
}

function has_children() {
	global $post;
	$pages = get_pages('child_of=' . $post->ID);
	return count($pages);
}

function excerpt($limit, $id = null) {


  if ($id) {
    $excerpt = explode(' ', get_the_excerpt($id), $limit);
  } else {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
  }

  if (count($excerpt) >= $limit) {
    array_pop($excerpt);
    $excerpt = implode(" ", $excerpt) . '...';
  } else {
    $excerpt = implode(" ", $excerpt);
  }

  $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);

  return $excerpt;
}


function callXMProxy(string $endpoint, string $type, array $args = [], $token = null, $prefix = XM_API_URL_PREFIX)
{
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HEADER, 0);
	curl_setopt($curl, CURLOPT_URL, XM_API_URL . $prefix . $endpoint);
	curl_setopt($curl, CURLOPT_USERAGENT, '');

	$data = json_encode($args, JSON_FORCE_OBJECT);

	$headers = [
		'Accept: application/json',
		'Content-Type: application/json',
		'Content-Length: '.strlen($data)
	];

	if ($token !== null) {
		$headers[] = 'Auth-Token: '.$token;
	}

	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

	if ($type == 'DELETE' || $type == 'PUT') {
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
	} else {
		curl_setopt($curl, CURLOPT_POST, $type === 'POST');
	}

	$curl_response = curl_exec($curl);
	$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (empty($curl_response)) {
		throw new RestException(__('Server error'), 500);
	}

	$json = json_decode($curl_response);

	if ($http_status !== 200) {

		$message = "Error when handling request. (".$endpoint.")".$curl_response;
		if (isset($json->message)) {
			$message = $json->message;
		}

		throw new RestException(__($message), $http_status);
	}

	return $json;
}

function getServerAdminToken()
{
	$result = callXMProxy("authentication/password", 'POST', ['Email' => XM_API_USERNAME, 'Password' => XM_API_PASSWORD]);
	return $result->session->token;
}



function xm_start_live_demo() {

	try {
		$token = getServerAdminToken();
		$result = callXMProxy("domains/xmreality/demo", 'POST', [], $token);

		echo json_encode((object) [
			'primary' => $result[0]->url.'?guide=true',
			'secondary' => $result[1]->url
		]);

	} catch (RestException $rex) {
		echo $rex->getMessage().$rex->getCode();
	}

	wp_die();
}

add_action( 'wp_ajax_start_live_demo', 'xm_start_live_demo' );
add_action( 'wp_ajax_nopriv_start_live_demo', 'xm_start_live_demo' );



function cision_curly($service_url) {
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_URL, $service_url);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	$curl_response = curl_exec($curl);
	curl_close($curl);
	return json_decode($curl_response);
}

function xm_curl($service_url) {
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_URL, $service_url);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	$curl_response = curl_exec($curl);
	curl_close($curl);
	return $curl_response;
}


function cision_newsfeed() {

	$releases = [];

	$numPages = 2;
	for ($i = 1; $i <= $numPages; $i++) {
		$feed = cision_curly(get_field('cision_feed_url','options').'&pageSize=50&pageIndex='.$i);
		$releases = array_merge($releases, $feed->Releases);
	}

	return $releases;
}

function filterCisionReleases($releases, $withFiles = false) {
	$return = array();
	foreach ($releases as $release) {
		if ((count($release->Files) > 0 && $withFiles) || (count($release->Files) == 0 && !$withFiles)) {
			$return[] = $release;
		}
	}
	return $return;
}

add_action('init', function () {
	/*
	add_filter('cron_schedules', function ($schedules) {
		$schedules['every_five_minutes'] = [
			'interval' => 60 * 5,
			'display'  => __('Every 5 minutes'),
		];

		return $schedules;
	});

	if (!wp_next_scheduled('xm_cision_sync')) {
		wp_schedule_event(time(), 'every_five_minutes', 'xm_cision_sync');
	}*/
});

add_action('rest_api_init', function() {
	register_rest_route('xm/v2', '/sync/', array(
		'methods'  => ['GET', 'POST'],
		'callback' => 'xm_cision_sync',
		'args'     => [

		]
	));
});

function xm_cision_sync()
{
	$authKeys = [
		'F2A4A865FF3545158AC78B36A8E1E085'
	];

	$date = date("Y-m-d", strtotime("-10 years"));
	$page = 1;
	$pressReleases = [];

	foreach ($authKeys as $authKey) {
		$cisionURL = 'http://publish.ne.cision.com/papi/NewsFeed/'.$authKey.'?format=json&detailLevel=detail&pageSize=50&startDate='.$date.'&pageIndex='.$page;
		if ($result = cision_curly($cisionURL)) {
			$pressReleases = array_merge($pressReleases, $result->Releases);
		}
	}


	$countPress = insertPressFromCision($pressReleases);

	return [
		'date' => $date,
		'press' => $countPress
	];

}


function insertPressFromCision($items)
{
	$query = new WP_Query([
		'posts_per_page' => -1,
		'post_type'      => 'financial',
		'fields'         => 'ids'
	]);

	$cached = [];

	foreach ($query->get_posts() as $id) {
		$cached[] = (object) ['id' => $id, 'cision_id' => get_field('cision_id', $id)];
	}

	$result = 0;
	foreach ($items as $pressRelease) {

		$index = array_search($pressRelease->Id, array_map(function ($item) {
			return $item->cision_id;
		}, $cached));

		$pressObj = [
			'post_author'  => 1,
			'post_type'    => 'financial',
			'post_status'  => 'publish',
			'post_title'   => $pressRelease->Title,
			'post_date'    => $pressRelease->PublishDate,
			'post_content' => str_replace("&nbsp;", '', strip_tags($pressRelease->HtmlBody, '<br><p><i><h1><h2><h3><strong>'))
		];

		if ($index !== false && $index >= 0) {
			$pressObj['ID'] = $cached[$index]->id;
		}

		$categories = [];
		$category   = null;
		if ($pressRelease->InformationType !== null) {
			$category = get_term_by('slug', esc_attr($pressRelease->InformationType), 'financial_category', ARRAY_A);
			if ($category == false) {
				$category = wp_insert_term($pressRelease->InformationType, 'financial_category');
				if (is_array($category)) {
					$categories[] = (int) $category['term_id'];
				}
			} else {
				$categories[] = (int) $category['term_id'];
			}
		}

		$parent_post_id = wp_insert_post($pressObj);
		if ($parent_post_id > 0) {
			update_field('cision_id', $pressRelease->Id, $parent_post_id);
			if (count($categories) > 0) {
				wp_set_object_terms($parent_post_id, $categories, 'financial_category');
			}

			update_field('is_regulatory', $pressRelease->IsRegulatory, $parent_post_id);

			//Set document year only if first time
			if (!get_field('publish_year', $parent_post_id)) {
				$documentYear = (int) date('Y', strtotime($pressRelease->PublishDate));

				if (in_array(strtolower($pressRelease->InformationType), ['rdv', 'kmk'])) {
					$documentYear = $documentYear - 1;
				}

				update_field('publish_year', $documentYear, $parent_post_id);
			}

			if (count($pressRelease->Files) > 0) {
				$files = [];
				foreach ($pressRelease->Files as $file) {
					$files[] = [
						'title' => $file->Title,
						'url'   => $file->Url
					];
				}
				update_field('files', $files, $parent_post_id);
			}

			$result ++;
		}
	}

	return $result;
}

function RSSFromHubspot()
{
	$url = 'https://blog.xmreality.com/rss.xml';
	$result = xm_curl($url);
	$xml = new SimpleXmlElement($result, LIBXML_NOCDATA);
	$items = [];
	foreach ($xml->channel->item as $item) {

		$descriptionDOM = new DOMDocument();
		$descriptionDOM->loadHTML((string) $item->description);

		$image = '';
		$description = '';

		if ($descriptionDOM->getElementsByTagName('img')->length > 0) {
			$image = $descriptionDOM->getElementsByTagName('img')->item(0)->getAttribute('src');
		}

		if ($descriptionDOM->getElementsByTagName('p')->length > 0) {
			$description = $descriptionDOM->getElementsByTagName('p')->item(0)->C14N();
		}


		$categories = [];
		foreach ($item->category as $cat) {
			$categories[] = (string) $cat;
		}

		$items[] = (object) [
			'title' => (string) $item->title,
			'url' => (string) $item->link,
			'description' => $description,
			'image' => $image,
			'categories' => $categories
		];


	}

	return $items;
}

function smartwp_remove_wp_block_library_css(){
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    wp_dequeue_style( 'wc-block-style' ); // Remove WooCommerce block CSS
}
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100 );

