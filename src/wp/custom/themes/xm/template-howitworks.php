<?php


/*
Template Name: How it works
*/

?>

<?php get_header(); ?>
	<?php if ( !post_password_required() ) {  ?>






		<div class="space" id="start"></div>

		<div id="call-link"></div>
		<div class="trigger-message">
		  <div class="trigger" id="trigger-message1"></div>
		  <div class="trigger" id="trigger-message2"></div>
		  <div class="trigger" id="trigger-message3"></div>
		  <div class="trigger" id="trigger-message4"></div>
		</div>


		<div id="trigger-paralax1"></div>
		<div id="trigger-parallax1"></div>
		<div id="trigger-parallax2"></div>
		<div class="section start-section">
		    <div class="container">
		        <div class="row row-lg align-items-center">
		            <div class="col-6 fadeInUp" id="info-first">

		                <p class="top-title">HOW IT WORKS</p>
		                <h2 class="big-title fadeInUp">Remotely guiding someone by voice just isn't enough.</h2>
		                <p>XMReality Remote Guidance allows you to see what your customer or colleague sees and guide as if you were there. </p>

		    						<div class="graphic-container">
		    							<div class="graphics">
		    								<div class="square medium green pos1"></div>
		    								<div class="square small blue pos2" id="parallax1"></div>
		    								<div class="dotts"></div>
		    							</div>
		    						</div>
		            </div>

		            <div class="col-6">
		                <div class="square2" id=""></div>
		                <div id="target">
		                  <div class="mobile">
		                      <div class="mobile_overlay"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/phone_small.png" loading="lazy" alt="" /></div>
		                      <div class="screens">
		                          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/template_2.png" loading="lazy" alt="" class="" id="mobile_2" />

		                          <div class="message message_1" id="message1"></div>
		                          <div class="message message_2" id="message2"></div>
		                          <div class="message message_3" id="message3"></div>
		                          <div class="message message_4" id="message4"></div>
		                          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/template_1.png" loading="lazy" alt=""  class="" id="mobile_1" />
		                      </div>
		                  </div>
		                </div>

		            </div>
		        </div>
		    </div>
		</div>


		<div id="trigger-paralax2"></div>
		<div id="trigger-second"></div>
		<div id="end-first-mobile"></div>
		<div class="section" id="info-second">
		    <div class="container">
		        <div class="row row-lg align-items-center">
		            <div class="col-6">

		              <div>
		                <p class="top-title">USABILITY</p>
		                <h2 class="big-title">It only takes a text message to start </h2>
		                <p>By using a call Link there is no need for your customer to install an app. The session is started in the web browser and you can see what your customer sees and assist them. </p>
		                <p>If you need to help a colleague or someone who has XMReality you ccan call them directly from the app. </p>
		              </div>
		              <div id="trigger-third"></div>
		            </div>

		            <div class="col-6">
		                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/phone_small.png" loading="lazy" alt="" class="ghost" />
		                <div class="graphic-container">
		                  <div class="graphics">
		                    <div class="square medium green pos1"></div>
		                    <div class="square small blue pos2" id="parallax2"></div>
		                    <div class="dotts"></div>
		                  </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>



		<div class="trigger-features">
		  <div class="trigger" id="trigger-feature1"></div>
		  <div class="trigger" id="trigger-feature2"></div>
		  <div class="trigger" id="trigger-feature3"></div>
		  <div class="trigger" id="trigger-feature4"></div>
		  <div class="trigger" id="trigger-feature5"></div>
		  <div class="trigger" id="trigger-feature6"></div>
		</div>
		<div id="trigger-feature"></div>
		<div class="section" id="target_feature">
		<div id="feature"></div>
		    <div class="container">
		        <div class="col-8 offset-2 text-center">
		          <div id="info-third">
		            <p class="top-title">THIS IS WHAT YOU GET</p>
		            <h2 class="big-title">A wide variety of tools here to help you.</h2>
		          </div>

		        </div>

		        <div class="row row-lg align-items-center">
		            <div class="col-4">
		                <div class="feature-list left text-right">
		                    <div class="feature" id="feature1">
		                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon.png" loading="lazy" alt="" class="icon" />
		                        <h4>Feature lorem ipsum</h4>
		                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</p>
		                    </div>
		                    <div class="feature" id="feature3">
		                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon.png" loading="lazy" alt="" class="icon" />
		                        <h4>Feature lorem ipsum</h4>
		                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</p>
		                    </div>
		                    <div class="feature" id="feature5">
		                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon.png" loading="lazy" alt="" class="icon" />
		                        <h4>Feature lorem ipsum</h4>
		                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</p>
		                    </div>
		                </div>
		            </div>

		            <div class="col-4">
		              <div class="mobile feature-mobile">
		                  <div class="mobile_overlay"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/phone_small.png" loading="lazy" alt="" /></div>
		                  <div class="screens">
		                      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/template_1.png" loading="lazy" alt="" class="show" id="mobile_2" />
		                      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/feature_1.jpg" loading="lazy" alt="" class="" id="feature1_image" />
		                      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/feature_2.jpg" loading="lazy" alt="" class="" id="feature2_image" />
		                      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/feature_3.jpg" loading="lazy" alt="" class="" id="feature3_image" />
		                      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/feature_1.jpg" loading="lazy" alt="" class="" id="feature4_image" />
		                      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/feature_2.jpg" loading="lazy" alt="" class="" id="feature5_image" />
		                      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/feature_3.jpg" loading="lazy" alt="" class="" id="feature6_image" />
		                  </div>
		              </div>
		              <div class="text-center"><a href="#" class="btn btn-secondary-outlined">View all features</a></div>
		                <div id="feature-spacer"></div>
		            </div>

		            <div class="col-4">
		                <div class="feature-list" id="feature-left-right">
		                    <div class="feature" id="feature2">
		                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon.png" loading="lazy" alt="" class="icon" />
		                        <h4>Feature lorem ipsum</h4>
		                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</p>
		                    </div>
		                    <div class="feature" id="feature4">
		                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon.png" loading="lazy" alt="" class="icon" />
		                        <h4>Feature lorem ipsum</h4>
		                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</p>
		                    </div>
		                    <div class="feature" id="feature6">
		                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon.png" loading="lazy" alt="" class="icon" />
		                        <h4>Feature lorem ipsum</h4>
		                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</p>
		                    </div>
		                </div>
		        </div>
		    </div>
		    </div>
		</div>

		<div id="trigger-intergrate"></div>
		<div class="section" id="logo">
		    <div class="container">
		        <div class="row">
		            <div class="offset-3 col-6 text-center">
		                <div class="intergrate_logos">
		                  <div class="intergrate_logo logo_1" id="logo_1"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" loading="lazy" alt="" class="" /></div>
		                  <div class="intergrate_logo logo_2 small" id="logo_2"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" loading="lazy" alt="" class="" /></div>
		                  <div class="intergrate_logo logo_3" id="logo_3"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" loading="lazy" alt="" class="" /></div>
		                  <div class="intergrate_logo logo_4 small" id="logo_4"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" loading="lazy" alt="" class="" /></div>
		                  <div class="intergrate_logo logo_5" id="logo_5"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" loading="lazy" alt="" class="" /></div>
		                  <div class="intergrate_logo logo_6" id="logo_6"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" loading="lazy" alt="" class="" /></div>
		                  <div class="intergrate_logo logo_7 small" id="logo_7"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" loading="lazy" alt="" class="" /></div>
		                </div>
		                <p class="top-title">OTHER TOOLS</p>
		                <h2 class="big-title">Integrates with the tools that you love</h2>
		                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
		                <p><a href="#">Read more</a></p>
		            </div>
		        </div>
		    </div>
		</div>


		<div class="" id="">
			 <?php require_once 'partials/partial-sections.php'; ?>
		</div>

		<?php if (have_rows('second-sections')): ?>
		<div class="second-sections">
				<?php require_once 'partials/partial-second-sections.php'; ?>
		</div>
		<?php endif; ?>



		<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.4/gsap.min.js"></script>
	  <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.js"></script>
	  <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>
	  <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.js"></script>


		<script>
			// init controller

			jQuery(function ($) {

			var controller = new ScrollMagic.Controller();

		    // build tween

			/*new ScrollMagic.Scene({triggerElement: "#sec2"})
							.setClassToggle("#high2", "active") // add class toggle
							.addIndicators() // add indicators (requires plugin)
							.addTo(controller);*/
		    var offset = $("#end-first-mobile").offset();
		    console.log(offset.top);

		    new ScrollMagic.Scene({triggerElement: "#start", offset: "600px", duration: offset.top - 200})
		    .setPin("#target", {pushFollowers: false})
		    .addTo(controller);

		    new ScrollMagic.Scene({triggerElement: "#call-link"})
		    .setTween(TweenMax.to("#mobile_1", 0.2, {opacity: "0", ease: Linear.easeNone}))
		    .addTo(controller);

		    new ScrollMagic.Scene({triggerElement: "#call-link"})
		    .setTween(TweenMax.to("#mobile_2", 0.2, {opacity: "1", ease: Linear.easeNone}))
		    .addTo(controller);

		    new ScrollMagic.Scene({triggerElement: "#trigger-message1"})
		    .setTween(TweenMax.to("#message1", 0.2, {opacity: "0", ease: Linear.easeNone}))
		    .addTo(controller);

		    new ScrollMagic.Scene({triggerElement: "#trigger-message2"})
		    .setTween(TweenMax.to("#message2", 0.2, {opacity: "0", ease: Linear.easeNone}))
		    .addTo(controller);

		    new ScrollMagic.Scene({triggerElement: "#trigger-message3"})
		    .setTween(TweenMax.to("#message3", 0.2, {opacity: "0", ease: Linear.easeNone}))
		    .addTo(controller);

		    new ScrollMagic.Scene({triggerElement: "#trigger-message4"})
		    .setTween(TweenMax.to("#message4", 0.2, {opacity: "0", ease: Linear.easeNone}))
		    .addTo(controller);


		    new ScrollMagic.Scene({triggerElement: "#trigger-second"})
		  	.setClassToggle("#info-second", "fadeIn")
		    .addTo(controller);


		    new ScrollMagic.Scene({triggerElement: "#trigger-third"})
		  	.setClassToggle("#info-third", "fadeIn")
		    .addTo(controller);











		  	var controller_features = new ScrollMagic.Controller();
		    new ScrollMagic.Scene({triggerElement: "#trigger-feature", offset: "500px", duration: "1800px"})
		    .setPin("#target_feature")
		    .addTo(controller_features);

		    new ScrollMagic.Scene({triggerElement: "#trigger-feature1", offset: "500px", duration: "300px"})
		  	.setClassToggle("#feature1", "active")
		    .addTo(controller_features);

		    new ScrollMagic.Scene({triggerElement: "#trigger-feature1", offset: "500px"})
		  	.setClassToggle("#feature1", "active")
		    .setTween(TweenMax.to("#feature1_image", 1, {opacity: "1", ease: Linear.easeNone}))
		    .addTo(controller_features);

		    new ScrollMagic.Scene({triggerElement: "#trigger-feature2", offset: "500px", duration: "300px"})
		  	.setClassToggle("#feature2", "active")
		    .addTo(controller_features);

		    new ScrollMagic.Scene({triggerElement: "#trigger-feature2", offset: "500px"})
		    .setTween(TweenMax.to("#feature2_image", 1, {opacity: "1", ease: Linear.easeNone}))
		    .addTo(controller_features);

		    new ScrollMagic.Scene({triggerElement: "#trigger-feature3", offset: "500px", duration: "300px"})
		  	.setClassToggle("#feature3", "active")
		    .addTo(controller_features);

		    new ScrollMagic.Scene({triggerElement: "#trigger-feature3", offset: "500px"})
		    .setTween(TweenMax.to("#feature3_image", 1, {opacity: "1", ease: Linear.easeNone}))
		    .addTo(controller_features);



		    new ScrollMagic.Scene({triggerElement: "#trigger-feature4", offset: "500px", duration: "300px"})
		  	.setClassToggle("#feature4", "active")
		    .addTo(controller_features);

		    new ScrollMagic.Scene({triggerElement: "#trigger-feature4", offset: "500px"})
		    .setTween(TweenMax.to("#feature4_image", 1, {opacity: "1", ease: Linear.easeNone}))
		    .addTo(controller_features);

		    new ScrollMagic.Scene({triggerElement: "#trigger-feature5", offset: "500px", duration: "300px"})
		  	.setClassToggle("#feature5", "active")
		    .addTo(controller_features);

		    new ScrollMagic.Scene({triggerElement: "#trigger-feature5", offset: "500px"})
		    .setTween(TweenMax.to("#feature5_image", 1, {opacity: "1", ease: Linear.easeNone}))
		    .addTo(controller_features);


		    new ScrollMagic.Scene({triggerElement: "#trigger-feature6", offset: "500px", duration: "300px"})
		  	.setClassToggle("#feature6", "active")
		    .addTo(controller_features);

		    new ScrollMagic.Scene({triggerElement: "#trigger-feature6", offset: "500px"})
		    .setTween(TweenMax.to("#feature6_image", 1, {opacity: "1", ease: Linear.easeNone}))
		    .addTo(controller_features);









		    var controllerIntergrate = new ScrollMagic.Controller();

		  	new ScrollMagic.Scene({triggerElement: "#trigger-intergrate"})
				.setTween(TweenMax.from("#logo_1", 1, {opacity:0, top: "30%",marginLeft: 0, delay:0.15, ease: Expo.easeOut}))
				.addTo(controllerIntergrate);

		  	new ScrollMagic.Scene({triggerElement: "#trigger-intergrate"})
				.setTween(TweenMax.from("#logo_2", 1, {opacity:0, top: "30%", marginLeft: 0, delay:0.2, ease: Expo.easeOut}))
				.addTo(controllerIntergrate);

		  	new ScrollMagic.Scene({triggerElement: "#trigger-intergrate"})
				.setTween(TweenMax.from("#logo_3", 1, {opacity:0, top: "30%", marginLeft: 0, delay:0.25, ease: Expo.easeOut}))
				.addTo(controllerIntergrate);


		  	new ScrollMagic.Scene({triggerElement: "#trigger-intergrate"})
				.setTween(TweenMax.from("#logo_4", 1, {opacity:0, top: "30%", marginLeft: 0, delay:0.3, ease: Expo.easeOut}))
				.addTo(controllerIntergrate);

		  	new ScrollMagic.Scene({triggerElement: "#trigger-intergrate"})
				.setTween(TweenMax.from("#logo_5", 1, {opacity:0, top: "30%", marginLeft: 0, delay:0.35, ease: Expo.easeOut}))
				.addTo(controllerIntergrate);

		  	new ScrollMagic.Scene({triggerElement: "#trigger-intergrate"})
				.setTween(TweenMax.from("#logo_6", 1, {opacity:0, top: "30%", marginLeft: 0, delay:0.4, ease: Expo.easeOut}))
				.addTo(controllerIntergrate);


		  	new ScrollMagic.Scene({triggerElement: "#trigger-intergrate"})
				.setTween(TweenMax.from("#logo_7", 1, {opacity:0, top: "30%", marginLeft: 0, delay:0.45, ease: Expo.easeOut}))
				.addTo(controllerIntergrate);





		    var controllerParallax = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

		  	new ScrollMagic.Scene({triggerElement: "#trigger-parallax1"})
				.setTween("#parallax1", {y: "2000%", ease: Linear.easeNone})
				.addTo(controllerParallax);

		  	new ScrollMagic.Scene({triggerElement: "#trigger-parallax2"})
				.setTween("#parallax2", {y: "2000%", ease: Linear.easeNone})
				.addTo(controllerParallax);

				});
		</script>


	<?php }else{ ?>
			<div class="container text-center mt-5 mb-5 pb-5 pt-5">
				<?php echo get_the_password_form(); ?>
			</div>
	<?php } ?>

<?php get_footer();
