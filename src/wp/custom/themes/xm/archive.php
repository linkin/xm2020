<?php
/**
 * The template for displaying global archives.
 *
 * @package XMreality
 * @since Twenty Fifteen 1.1
 */


get_header();

?>

<div class="page-cover article-cover no-cover-image full-width-graphics lightblue">
	<div class="cover-content">
		<div class="container container-xl">

			<div class="row row-lg">
				<div class="col-md-7 offset-md-2">
					<?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
					<?php the_archive_description(); ?>
				</div>
			</div><!-- /.row -->

			<div class="graphic-container">
				<div class="graphics">
					<div id="parallax5" class="square yellow medium green pos1"></div>
					<div id="parallax6" class="square pink small blue pos2"></div>
					<div id="parallax7" class="square small blue pos2"></div>
					<div id="parallax8" class="square small lightblue pos2"></div>
					<div class="dotts dottsarticle"></div>
				</div>
			</div><!-- /.graphic-container -->

		</div><!-- /.container -->
	</div>
</div><!-- /.page-cover  -->


<div class="page-article">
	<div class="container container-xl">
		<div class="row row-lg">
			<div class="col-md-7 offset-md-2">
				<div class="page-article-inner">

					<?php
					if ( have_posts() ) :
						while( have_posts()) : the_post();
							// Get post type name.
							$post_type       = get_post_type_object( get_post_type() );
							$post_type_title = ( $post_type ) ? esc_html( $post_type->labels->singular_name ) : '';
							$post_type_slug  = ( $post_type ) ? esc_html( $post_type->name ) . '-' : 'post-';
					?>

							<article <?php post_class( 'post' ); ?> aria-labelledby="<?php echo $post_type_slug . get_the_ID(); ?>">
								<?php the_title( '<h2 id="' . $post_type_slug . get_the_ID() . '" class="mt-2"><a href="' . esc_url( get_the_permalink() ) . '">', '</a></h2>' ); ?>

								<?php if ( $post_type_title ) : ?>
									<p class="label"><?php echo esc_attr( $post_type_title ); ?></p>
								<?php endif; ?>

								<?php the_excerpt(); ?>

							</article>

						<?php
						endwhile; // while have_posts.

					else :

						echo '<p role="status"></p>Content not found.</p>';

					endif; // if have_posts.
					?>

					<div class="d-flex justify-content-between">
						<?php posts_nav_link(); ?>
					</div>

				</div><!-- /.page-article-inner -->
			</div>

			<div class="col-md-3">
				<div class="right-column">
					<?php the_field('right_column_article', 'options'); ?>
				</div>
			</div>

		</div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- /.page-article -->




<?php get_footer();