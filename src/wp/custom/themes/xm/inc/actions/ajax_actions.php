<?php

function barebone_custom_ajax()
{
	wp_die();
}

/**
 * Replace [action-name] with name of choice.
 * no_priv_ adds actions to non-logged in users, required if used in front-office.
 */
//add_action('wp_ajax_[action-name]', 'barebone_custom_ajax');
//add_action('wp_ajax_nopriv_[action-name]', 'barebone_custom_ajax');