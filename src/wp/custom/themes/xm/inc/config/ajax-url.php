<?php

/**
 * Adds ajax_url to front-end javascript, so ajax-calls can easily be made.
 */

function add_barebone_ajaxurl() {
	echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
}
add_action('wp_head', 'add_barebone_ajaxurl');
