<?php

/**
 * Adds menus to wordpress back-office
 */

function register_barebone_menu() {
	register_nav_menu('main-menu', __('Main menu'));
	register_nav_menu('side-menu', __('Side menu'));
	register_nav_menu('top-menu', __('Top menu'));
	register_nav_menu('footer-menu', __('Footer menu'));
	register_nav_menu('footer-menu-right', __('Footer menu right'));
	register_nav_menu('legal-menu', __('Legal menu'));
	register_nav_menu('investor-menu', __('Investor menu'));
	register_nav_menu('bottom-menu', __('Bottom menu'));
}

add_action('init', 'register_barebone_menu');
