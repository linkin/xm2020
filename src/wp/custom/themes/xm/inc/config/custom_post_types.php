<?php

register_post_type('article',
	array(
		'labels'       => array(
			'name'          => __('Articles'),
			'singular_name' => __('Article'),
		),
		'public'       => true,
		'has_archive'  => false,
		'menu_icon'    => 'dashicons-id',
		'supports'     => ['title', 'editor', 'revisions'],
		'rewrite'      => ['slug' => 'article'],
	)
);


register_taxonomy('article_category', array('article'), array(
		'hierarchical' => true,
		'label' => 'Categories',
		'singular_label' => 'Category'
	)
);

register_post_type('event',
    array(
        'labels'       => array(
            'name'          => __('Events'),
            'singular_name' => __('Event'),
        ),
        'public'       => true,
        'has_archive'  => false,
        'menu_icon'    => 'dashicons-id',
        'supports'     => ['title', 'editor', 'revisions']
        //'rewrite'      => ['slug' => 'event'],
    )
);

register_post_type('whitepaper',
	array(
		'labels'       => array(
			'name'          => __('Whitepapers'),
			'singular_name' => __('Whitepaper'),
		),
		'public'       => true,
		'has_archive'  => false,
		'menu_icon'    => 'dashicons-id',
		'supports'     => ['title', 'editor', 'revisions'],
		'rewrite'      => ['slug' => 'whitepaper'],
	)
);


register_post_type('video',
	array(
		'labels'       => array(
			'name'          => __('Videos'),
			'singular_name' => __('Video'),
		),
		'public'       => true,
		'has_archive'  => false,
		'menu_icon'    => 'dashicons-id',
		'supports'     => ['title', 'editor', 'revisions'],
		'rewrite'      => ['slug' => 'video'],
	)
);



register_post_type('story',
	array(
		'labels'       => array(
			'name'          => __('Stories'),
			'singular_name' => __('Story'),
		),
		'public'       => true,
		'has_archive'  => false,
		'menu_icon'    => 'dashicons-id',
		'supports'     => ['title', 'editor', 'revisions'],
		'rewrite'      => ['slug' => 'story'],
	)
);


register_taxonomy('story_category', array('story'), array(
		'hierarchical' => true,
		'label' => 'Categories',
		'singular_label' => 'Category'
	)
);


register_post_type('customer',
	array(
		'labels'       => array(
			'name'          => __('Customers'),
			'singular_name' => __('Customer'),
		),
		'public'       => true,
		'has_archive'  => false,
		'menu_icon'    => 'dashicons-id',
		'supports'     => ['title', 'editor', 'revisions'],
		'rewrite'      => ['slug' => 'customer'],
	)
);




flush_rewrite_rules();
