<?php get_header(); ?>

<?php
if (have_posts()) :
    while (have_posts()) : the_post(); ?>

        <?php if (!post_password_required()) { ?>

            <?php

            $title = get_the_title();
            $smallTitle = get_field('small_title');
            $lead = get_field('lead');

            if (get_field('alt_title')) {
                $title = get_field('alt_title');
            }

            $category_label = "article_category";
            $post = get_queried_object();
            $postType = get_post_type_object(get_post_type($post));
            if ($postType) {
                $postType = esc_html($postType->labels->singular_name);
            }
            if ($postType == "Story") {
                $category_label = "story_category";
            }

            $pageCoverCSS = "page-cover article-cover full-width-graphics lightblue";

            if (!get_field('cover_image')) {
                $pageCoverCSS = "page-cover article-cover no-cover-image full-width-graphics lightblue";
            }

            $archivePage = 14;

            $readTime = '2 min';

            if (get_post_type($post) == 'event') {
                $archivePage = 5528;
                $readTime = get_field('date');
            }

            ?>

            <div class="<?php echo $pageCoverCSS; ?>">
                <div class="cover-content">
                    <div class="container container-xl">

                        <div class="row row-lg">
                            <div class="col-md-7 offset-md-2">

                                <p class="top-title"><a href="<?php echo esc_url(get_permalink($archivePage)); ?>"><i
                                                class="fal fa-angle-left"></i> Back</a></p>

                                <?php if ($title) : ?>
                                    <h1><?php echo $title; ?></h1>
                                <?php endif; ?>

                                <p class="label">
                                    <span class="category">
                                      <?php
                                      if (get_the_terms(get_the_ID(), $category_label)) {
                                          foreach (get_the_terms(get_the_ID(), $category_label) as $cat) {
                                              echo $cat->name;
                                          }
                                      }
                                      ?>
                                      </span>
                                    <span class="gray dot-divider"><?php echo $readTime; ?></span></p>
                                </p>


                                <?php if (get_field('lead')) : ?>
                                    <div class="intro mb-4">
                                        <div class="lead">
                                            <?php echo get_field('lead'); ?>
                                        </div>
                                    </div>
                                <?php
                                endif;
                                ?>

                            </div>
                        </div>


                        <div class="graphic-container">
                            <div class="graphics">
                                <div id="parallax5" class="square yellow medium green pos1"></div>
                                <div id="parallax6" class="square pink small blue pos2"></div>
                                <div id="parallax7" class="square small blue pos2"></div>
                                <div id="parallax8" class="square small lightblue pos2"></div>
                                <div class="dotts dottsarticle"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="page-article">

                <?php if (get_field('cover_image')) { ?>
                    <div class="container container-xl">
                        <div class="article-image"
                             style='background-image: url(<?php echo get_field("cover_image")["sizes"]["large"]; ?>)'></div>
                    </div>
                <?php } ?>

                <div class="container container-xl">
                    <div class="row row-lg">
                        <div class="col-md-7 offset-md-2">
                            <div class="page-article-inner">

                                <?php the_content(); ?>

                                <?php if ($postType == 'Event' && get_field('signup_button')) : ?>
                                    <a class="btn btn-primary" target="_blank" href="<?php echo get_field('signup_button')['url']  ?>"><?php echo get_field('signup_button')['title'];  ?></a>
                                <?php endif; ?>

                                <?php if (get_field('whitepaper_headline')) { ?>
                                    <?php $headline = get_field('whitepaper_headline'); ?>

                                    <?php
                                    $post_object = get_field('whitepaper');

                                    if ($post_object):
                                        $post = $post_object;
                                        setup_postdata($post);

                                        ?>

                                        <div class="content-list whitepaper">
                                            <a href="<?php echo get_permalink(); ?>">
                                                <div class="row">
                                                    <div class="col-2">
                                                        <img src="<?php echo get_field('image')['sizes']['large']; ?>"
                                                             loading="lazy"
                                                             alt="<?php echo esc_html(get_field('image')['alt']); ?>"
                                                             class="shaddow"/>
                                                    </div>
                                                    <div class="col-10">
                                                        <h4 class="mb-3"><?php echo $headline; ?></h4>
                                                        <p class="arrow mb-0">Download whitepaper</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
                                        ?>
                                    <?php endif; ?>
                                <?php } ?>


                                <?php if (get_field('pdf_file')) { ?>
                                    <?php
                                    $headline = get_field('pdf_headline');
                                    $pdf_image = '';

                                    if (get_field('pdf_image')) {
                                        $pdf_image = get_field('pdf_image')['sizes']['large'];
                                    } else {
                                        $pdf_image = $post['icon'];
                                    }

                                    ?>

                                    <?php
                                    $post_object = get_field('pdf_file');

                                    if ($post_object):
                                        $post = $post_object;
                                        setup_postdata($post);

                                        if ($pdf_image == "") {
                                            $pdf_image = $post['icon'];
                                        }
                                        ?>

                                        <a href="<?php echo $post_object["url"]; ?>" target="_blank">
                                            <div class="content-list whitepaper">
                                                <div class="row align-items-center">
                                                    <div class="col-4">
                                                        <img src="<?php echo $pdf_image; ?>" loading="lazy" alt=""
                                                             class="shaddow2"/>
                                                    </div>
                                                    <div class="col-6">
                                                        <h3 class="mb-3"><?php echo $headline; ?></h3>
                                                        <p class="btn btn-primary"><i class="fas fa-file-download"></i>
                                                            Download PDF file</p>
                                                    </div>
                                                </div>

                                                <div class="graphic-container">
                                                    <div class="graphics">
                                                        <div id="parallax5"
                                                             class="square yellow medium green pos1"></div>
                                                        <div id="parallax6" class="square pink small blue pos2"></div>
                                                        <div class="dotts dottsarticle"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
                                        ?>
                                    <?php endif; ?>
                                <?php } ?>


                                <hr class="end"/>
                                <p class="small-text mb-1">SHARE ON SOCIAL MEDIA</p>
                                <div class="social-share">
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink() ?>"
                                       target="_blank"><i class="fab fa-facebook-f"></i></a>
                                    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink() ?>"
                                       target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                    <a href="https://twitter.com/home?status=<?php echo get_permalink() ?>"
                                       target="_blank"><i class="fab fa-twitter"></i></a>
                                    <a href="mailto:info@example.com?&subject=&body=<?php echo get_permalink() ?>"
                                       target="_blank"><i class="fa fa-envelope"></i></a>
                                    <a href="javascript:void(0)" onclick="CopyURL();"><i class="fa fa-link"></i><span
                                                class="text">Copy link</span></a>
                                </div>


                                <?php the_field('description_under_article', 'options'); ?>
                            </div>

                        </div>
                        <div class="col-md-3">
                            <div class="right-column">
                                <?php the_field('right_column_article', 'options'); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php } ?>

    <?php endwhile;
endif;
?>

<?php

$args = array('post_type' => $postType, 'posts_per_page' => 3, 'post__not_in' => [get_the_ID()]);
$the_query = new WP_Query($args);

if ($the_query->post_count > 0) :

    ?>
    <div class="second-sections">

        <div class="container">


            <div class="section-header text-left">
                <div class="row">

                    <?php if ($postType == "Story") { ?>

                        <div class="col-6">
                            <h3>Related customer stories</h3>
                        </div>
                        <div class="col-6 text-right">
                            <a href="<?php echo esc_url(get_permalink(700)); ?>" class="arrow">View all stories</a>
                        </div>

                    <?php } else if ($postType == "Article") { ?>

                        <div class="col-6">
                            <h3>Related articles</h3>
                        </div>
                        <div class="col-6 text-right">
                            <a href="<?php echo esc_url(get_permalink(717)); ?>" class="arrow">View all articles</a>
                        </div>

                    <?php } else if ($postType == "Event") { ?>

                        <div class="col-6">
                            <h3>Related events</h3>
                        </div>
                        <div class="col-6 text-right">
                            <a href="<?php echo esc_url(get_permalink(5528)); ?>" class="arrow">View all events</a>
                        </div>

                    <?php } ?>


                </div>
            </div>


            <div class="content-list">
                <div class="slick-slider article-slider">
                    <?php
                    ?>
                    <?php if ($the_query->have_posts()) : ?>
                        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                            <?php

                            $link = get_permalink();
                            $category = '';
                            $gray_label = '';
                            $title = get_the_title();
                            $excerpt = excerpt(13);
                            $imagesize = '';
                            $image = '';

                            if (get_field('thumbnail_background') === 'image') {
                                $image = get_field('thumbnail_image');
                            }


                            if ($postType === 'Story') {
                                $customer = get_field('customer');
                                $customer_logo = get_field('black_logotype', $customer->ID)['sizes']['medium'];
                                $imagesize = 'big';
                            }

                            $terms = get_the_terms(get_the_ID(), $category_label);

                            if ($terms && !is_wp_error($terms)) {
                                $categories = array();

                                foreach ($terms as $term) {
                                    $categories[] = $term->name;
                                }

                                $category = join(", ", $categories);
                            }

                            require get_template_directory() . '/partials/component/article-card.php';
                            ?>


                            <?php wp_reset_postdata(); ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>
<?php endif; ?>

<?php get_footer();
