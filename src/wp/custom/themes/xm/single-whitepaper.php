<?php get_header(); ?>

<?php
  $post_type_data = get_post_type_object( $post_type );
  $post_type_slug = $post_type_data->rewrite['slug'];
  $post_type_name= $post_type_data->labels->name;
 ?>


 <?php
 if( have_posts() ) :
   while( have_posts()) : the_post(); ?>

     <div class="page-cover lightblue">
     	<div class="cover-content">
         <div class="container">

             <div class="row row-lg">
               <div class="col-md-4 mb-5">
                <div class="graphic-container">
                   <img src="<?php echo esc_url( get_field('image')['sizes']['medium_large'] ); ?>" loading="lazy" alt="<?php echo esc_html( get_field('image')['alt'] ); ?>" class="shaddow" />
                   <div class="graphics">
  										<div id="parallax1" class="square medium green pos1"></div>
  										<div id="parallax2" class="square small blue pos2"></div>
  										<div class="dotts"></div>
  									</div>
                  </div>
               </div>
               <div class="col-md-7 text-left">

               <p class="top-title"><a href="<?php echo esc_url( get_permalink(14) ); ?>"><i class="fal fa-angle-left"></i> Back</a></p>

               <h1><?php the_title(); ?></h1>

               <?php the_content(); ?>

               <div class="mt-5 text-left">
                <?php if (get_field('requires_input')) : ?>
                    <h3>Fill in the form to get a download link</h3>
                    <?php the_field('form'); ?>
                <?php else : ?>
                    <a href="<?php echo get_field('file')['url']; ?>" class="btn btn-outline-primary mt-4"><?php the_field('button_text'); ?></a>
                <?php endif; ?>
              </div>

         		</div>
         	</div>

     		</div>
     	</div>
     </div>

   <?php endwhile;
 endif;
 ?>

<div class="modal fade-scale" id="downloadModal">
  <div class="modal-dialog modal-dialog-centered modal-bg-header">

    <div class="modal-content">
      <div class="modal-top text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fal fa-times"></i>
        </button>
        <h1><?php the_field('button_text'); ?></h1>
      </div>
      <div class="modal-body p-0">
        <?php the_field('form'); ?>
      </div>
    </div>
  </div>
</div>

<?php get_footer();
