<?php

/*
Template Name: Market
*/

?>

<?php get_header(); ?>
	<?php if ( !post_password_required() ) {  ?>

			<div>
			   <?php require_once 'partials/partial-cover.php'; ?>
			</div>

			<?php if( have_rows('usp') ): ?>

			<div class="usp-box">
				<div class="container">
					<div class="inner">
						<div class="row">

						<?php while (have_rows('usp')) : the_row(); ?>
							<div class="col-12 col-md">
								<div class="checklist">
									<h4><?php echo get_sub_field('title'); ?></h4>
									<p><?php echo get_sub_field('text'); ?></p>
								</div>
							</div>
						<?php endwhile; ?>

						</div>
					</div>
				</div>
			</div>

			<?php endif; ?>

			<div class="has-header" id="site-content">
			   <?php require_once 'partials/partial-sections.php'; ?>
			</div>

			<?php if (have_rows('second-sections')): ?>
			<div class="second-sections">
				 	<?php require_once 'partials/partial-second-sections.php'; ?>
			</div>
			<?php endif; ?>

	<?php }else{ ?>
			<div class="container text-center mt-5 mb-5 pb-5 pt-5">
				<?php echo get_the_password_form(); ?>
			</div>
	<?php } ?>

<?php get_footer();
