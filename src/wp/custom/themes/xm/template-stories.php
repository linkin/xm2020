<?php

/*
Template Name: Stories - archive
*/

?>
<?php

$title = get_the_title();
$smallTitle = get_field('small_title');
$lead = get_field('lead');

if (get_field('alt_title')) {
	$title = get_field('alt_title');
}

?>

<?php get_header(); ?>
	<?php if ( !post_password_required() ) {  ?>


		<div class="" id="">

	    <div class="page-cover article-cover full-width-graphics no-cover-image lightblue">
	    	<div class="cover-content">
	        <div class="container">

	            <div class="row row-lg">
	              <div class="col-md-7">

	              <?php if ($title) : ?>
	                <h1><?php echo $title; ?></h1>
	              <?php endif; ?>

		            <?php  if(get_field('lead')) : ?>
			            <div class="intro mb-4">
			              <div class="lead">
			                <?php echo get_field('lead'); ?>
			              </div>
			            </div>
		            <?php
		            endif;
		            ?>

	        		</div>
	        	</div>


						<div class="graphic-container">
							<div class="graphics">
								<div id="parallax5" class="square yellow medium green pos1"></div>
								<div id="parallax6" class="square pink small blue pos2"></div>
								<div id="parallax7" class="square small blue pos2"></div>
								<div id="parallax8" class="square small lightblue pos2"></div>
	              <div class="dotts dottsarticle"></div>
							</div>
						</div>

	    		</div>
	    	</div>
	    </div>



			<div class="has-header" id="site-content">
			<div class="container">
			<div class="section topMarginNone">
				<div class="content-list">
					<div class="row">
						<?php
						$args = array( 'post_type' => 'story', 'posts_per_page' => 10 );
						$the_query = new WP_Query( $args );
						?>
						<?php if ( $the_query->have_posts() ) : ?>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

							<div class="col-lg-4 mb-4">

							<?php
							$customer      = get_field('customer');
							$customer_logo = get_field('black_logotype', $customer->ID)['sizes']['medium'];
							$customer_name = get_the_title($customer->ID);
							$imagesize     = 'big';
							$image         = '';

							if ( get_field('thumbnail_background') === 'image' ){
								$image = get_field('thumbnail_image');
							}

							$link       = get_permalink();
							$category   = '';
							$gray_label = '';
							$title      = get_the_title();
							$excerpt    = excerpt(13);

							$terms = get_the_terms(get_the_ID(), 'story_category');

							if ( $terms && ! is_wp_error( $terms ) ){
								$categories = array();

								foreach ( $terms as $term ) {
									$categories[] = $term->name;
								}

								$category = join( ", ", $categories );
							}

							require get_template_directory() . '/partials/component/article-card.php';
							?>

						<?php wp_reset_postdata(); ?>
						</div>
						<?php endwhile; ?>
						<?php endif; ?>
						</div>
						</div>


				</div>
		  </div>
		</div>
	</div>



	<?php }else{ ?>
			<div class="container text-center mt-5 mb-5 pb-5 pt-5">
				<?php echo get_the_password_form(); ?>
			</div>
	<?php } ?>

<?php get_footer();
