<?php get_header(); ?>
<?php
if( have_posts() ) :
  while( have_posts()) : the_post();

$title = get_the_title();
$smallTitle = get_field('small_title');
$lead = get_field('lead');

if (get_field('alt_title')) {
	$title = get_field('alt_title');
}

?>

<div class="" id="">

  <div class="site-content" id="">

    <div class="container container-m text-md-center">

      <?php if ($smallTitle) : ?>
        <p class="top-title"><?php echo $smallTitle; ?></p>
      <?php endif; ?>

      <?php if ($title) : ?>
        <h1><?php echo $title; ?></h1>
      <?php endif; ?>

      <?php if ($lead) : ?>
       <div class="lead"><p><?php echo $lead; ?></p></div>
      <?php endif; ?>

    </div>

    <div class="container container-s">
      <div class="">
        <hr class="small-divider">
        <?php
          the_content();
        ?>

      </div>
    </div>
  </div>
</div>

<?php
  endwhile;
endif;
?>

<?php get_footer();
