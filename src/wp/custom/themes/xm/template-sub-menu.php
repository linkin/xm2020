<?php

/*
Template Name: Subpage - With menu
*/

?>

<?php get_header(); ?>


<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php

		$menu = 'Legal menu';
		$siteCss = "site-content";

		if(get_field('side_menu') == "investor"){
				$menu = 'investor menu';
				$siteCss = "site-content has-sub-menu";
		}

		if(get_field('side_menu') == "support"){
				$menu = 'support';
				$siteCss = "site-content has-sub-menu";
		}

	?>

	<?php if(get_field('side_menu') == "investor"){ ?>
		<div class="page-cover small-cover lightblue">
			<div class="cover-content">
		    <div class="container container-md">

					<div class="content">

						<h1>Investor relations (Swedish)</h1>

					</div>

					<div class="graphic-container">
						<div class="graphics">
							<div id="parallax1" class="square yellow medium green pos1"></div>
							<div id="parallax2" class="square pink small blue pos2"></div>
							<div id="parallax3" class="square small blue pos2"></div>
							<div id="parallax4" class="square small lightblue pos2"></div>
							<div class="dotts"></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	<?php } ?>


	<?php if(get_field('side_menu') == "support"){ ?>
		<div class="page-cover small-cover lightblue">
			<div class="cover-content">
		    <div class="container container-md">

					<div class="content">

						<h1>Support</h1>

					</div>

					<div class="graphic-container">
						<div class="graphics">
							<div id="parallax1" class="square yellow medium green pos1"></div>
							<div id="parallax2" class="square pink small blue pos2"></div>
							<div id="parallax3" class="square small blue pos2"></div>
							<div id="parallax4" class="square small lightblue pos2"></div>
							<div class="dotts"></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	<?php } ?>

    <div class="<?php echo $siteCss; ?>">
        <div class="container">
            <div class="row row-lg">
                <div class="col-lg-4 mb-5 mb-lg-0">

										<button data-toggle="collapse" data-target="#sideNavCollapse" class="side-menu-btn d-lg-none">
											<?php the_title(); ?><i class="fal fa-angle-down ml-2"></i>
										</button>

                    <div id="sideNavCollapse" class="collapse make-me-sticky d-lg-block">

											<nav class="nav nav-pills flex-column"  id="navbar-example3">
												<?php

													wp_nav_menu( array(
  													'menu' => $menu,
                            'menu_class' => 'side-menu',
        										'add_a_class' => 'nav-link'
													));

												?>
											</nav>


                    </div>
                </div>
                <div class="col-lg-8">

										<div class="page-article with-sub-menu">
											<nav aria-label="breadcrumb" class="d-none d-lg-block">
												<?php if (function_exists('the_breadcrumb')) {
													the_breadcrumb();
												} ?>
											</nav>

											<?php if ( !post_password_required() ) {  ?>

                      	<?php if(!get_field('hide_headline')) : ?>

	                        <h1><?php the_title(); ?></h1>
	                        <hr class="slim left"/>

                        <?php endif; ?>

											<?php }  ?>
											<div class="mt-5">
												<div class="intro lead mb-4"><?php the_field('lead'); ?></div>
												<?php require_once 'partials/partial-submenu-sections.php'; ?>
											</div>

										</div>

                </div>
            </div>
        </div>
    </div>

<?php endwhile; endif; ?>

<?php get_footer();
