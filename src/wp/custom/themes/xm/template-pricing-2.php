<?php

$title = get_the_title();
$smallTitle = get_field('small_title');
$lead = get_field('lead');
$content_under = get_field('content_under');

if (get_field('alt_title')) {
	$title = get_field('alt_title');
}


$cta = get_field('cta');

/*
Template Name: Pricing 2.0
*/

?>

<?php get_header(); ?>
	<?php if ( !post_password_required() ) {  ?>



		<div class="page-cover lightblue">
			<div class="cover-content smallGraphics">

				<div class="text-center">
		      <?php if ($smallTitle) : ?>
		        <p class="top-title"><?php echo $smallTitle; ?></p>
		      <?php endif; ?>

		      <?php if ($title) : ?>
		        <h1><?php echo $title; ?></h1>
		      <?php endif; ?>
				</div>


		    <div class="container container-cover-title">

					<div class="graphic-container">

						<div class="graphics centerTop">
							<div id="" class="square small blue position6"></div>
						</div>

						<div class="to-front">


				      <?php if ($lead) : ?>
				       <div class="lead"><?php echo $lead; ?></div>
				      <?php endif; ?>


							<?php if ($cta) : ?>
								<?php while (have_rows('cta')) : the_row(); ?>

										<?php
										$buttonCSS = "btn btn-green btn-xl mb-2";

										if (get_sub_field('outlined')){
											$buttonCSS = "btn btn-outline-primary btn-xl mb-2";
										}
										?>
										<a href="<?php the_sub_field('link'); ?>" class="<?php echo $buttonCSS; ?>">
											<?php the_sub_field('name'); ?>
										</a>

								<?php endwhile; ?>
							<?php endif; ?>


				      <?php if ($content_under) : ?>
				       <div class="gray mt-3 mb-3"><?php echo $content_under; ?></div>
				      <?php endif; ?>

						</div>

						<div class="graphics centerBottom">
							<div id="" class="square yellow medium position1"></div>
							<div id="" class="square pink small position2"></div>
		          <div class="dotts"></div>
						</div>
					</div>

				</div>



			</div>
		</div>

			<div class="has-header" id="site-content">
				<div class="page-content">

				<div class="section comparison bottomMarginSmall">

					<div class="section-header">
						<div class="container container-s mb-5 pb-lg-2">

							<h2><?php the_field('tiers_title'); ?></h2>

						</div>
					</div>



					<div class="mb-5 pb-5">
					<?php require 'partials/component/price_boxes.php'; ?>
					</div>


					<div id="compbarison" style="margin-top:-5rem;position: absolute;"></div>

					<div class="section-header">
						<div class="container container-s">
							<h2><?php the_field('comparison_title'); ?></h2>
						</div>
					</div>

					<div class="container">
						<div class="comparison-table-wrapper">

						<div class="comparison-table showAll" id="comparison-table">

							<div class="comparison-features">

								<div class="row product">
									<div class="col col-3 empty"></div>

									<?php if (get_field('express_show_on_website')) { ?>

									<div class="col">
										<div class="header">
											<h3><?php the_field('express_comparison_title_1'); ?></h3>
										</div>
									</div>

									<?php } ?>

									<?php if (get_field('business_show_on_website')) { ?>

									<div class="col">
										<div class="header">
											<h3><?php the_field('business_comparison_title_1'); ?></h3>
										</div>
									</div>

									<?php } ?>

									<?php if (get_field('enterprise_show_on_website')) { ?>

									<div class="col">
										<div class="header">
											<h3><?php the_field('enterprise_comparison_title_1'); ?></h3>
										</div>
									</div>

									<?php } ?>
								</div>

								<div id="features-list">

								<?php while (have_rows('pricing_features')) : the_row(); ?>

								<?php
									if (get_sub_field('headline_row')) {
								?>

								<a href="#" class="pricelist_feature_header">
									<div class="row divideheader">
											<div class="col col-12">
												<div class="price-label headline">
													<span><?php the_sub_field('label'); ?></span>
												</div>
											</div>
									</div>
								</a>

								<?php
									}else{
								?>

									<div class="row feature">
										<div class="col col-3">
											<div class="price-label">
												<span><?php the_sub_field('label'); ?></span>
												<i class="fal fa-question-circle tooltip" title="<?php the_sub_field('description'); ?>"></i>
											</div>
						        </div>

										<?php if (get_field('express_show_on_website')) { ?>
										<div class="col">
											<?php
												if (get_sub_field('express_value') == 'YES') {
									        echo '<i class="far fa-check"></i>';
									      }else if(get_sub_field('express_value') != ''){
													echo the_sub_field('express_value');
												}
											?>
						        </div>
										<?php } ?>

										<?php if (get_field('business_show_on_website')) { ?>
										<div class="col">
											<?php
												if (get_sub_field('business_value') == 'YES') {
									        echo '<i class="far fa-check"></i>';
									      }else if(get_sub_field('business_value') != ''){
													echo the_sub_field('business_value');
												}
											?>
						        </div>
										<?php } ?>

										<?php if (get_field('enterprise_show_on_website')) { ?>
										<div class="col">
											<?php
												if (get_sub_field('enterprice_value') == 'YES') {
									        echo '<i class="far fa-check"></i>';
									      }else if(get_sub_field('enterprice_value') != ''){
													echo the_sub_field('enterprice_value');
												}
											?>
						        </div>
										<?php } ?>

									</div>

								<?php
									}
								?>

								<?php endwhile; ?>
							</div>



							<div class="row product">
								<div class="col col-3 empty"></div>

								<?php if (get_field('express_show_on_website')) { ?>

								<div class="col">
									<div class="header">

							      <?php
							        $link = get_field('express_link');

						            	if ($link) :

						              $link_url = $link['url'];
						              $link_title = $link['title'];

						              $buttonCSS = "btn btn-primary small-bt";

						              if (get_field('express_outlined')){
						                $buttonCSS = "btn btn-outline-primary small-bt";
						              }
						              ?>

						              <a href="<?php echo esc_url( $link_url ); ?>" class="<?php echo $buttonCSS; ?>"><?php echo esc_html( $link_title ); ?></a>

						            <?php endif; ?>

										<p class="price"><?php the_field('express_comparison_title_2'); ?></p>
										<p class="price_unit"><?php the_field('express_comparison_title_3'); ?></p>
									</div>
								</div>

								<?php } ?>

								<?php if (get_field('business_show_on_website')) { ?>

								<div class="col">
									<div class="header">
										<?php
											$link = get_field('business_link');

											if ($link) :

											$link_url = $link['url'];
											$link_title = $link['title'];

											$buttonCSS = "btn btn-primary small-bt";

											if (get_field('business_outlined')){
												$buttonCSS = "btn btn-outline-primary small-bt";
											}
											?>

											<a href="<?php echo esc_url( $link_url ); ?>" class="<?php echo $buttonCSS; ?>"><?php echo esc_html( $link_title ); ?></a>

										<?php endif; ?>
										<p class="price"><?php the_field('business_comparison_title_2'); ?></p>
										<p class="price_unit"><?php the_field('business_comparison_title_3'); ?></p>
									</div>
								</div>

								<?php } ?>

								<?php if (get_field('enterprise_show_on_website')) { ?>

								<div class="col">
									<div class="header">
										<?php
											$link = get_field('enterprise_link');

											if ($link) :

											$link_url = $link['url'];
											$link_title = $link['title'];

											$buttonCSS = "btn btn-primary small-bt";

											if (get_field('enterprise_outlined')){
												$buttonCSS = "btn btn-outline-primary small-bt";
											}
											?>

											<a href="<?php echo esc_url( $link_url ); ?>" class="<?php echo $buttonCSS; ?>"><?php echo esc_html( $link_title ); ?></a>

										<?php endif; ?>
										<p class="price"><?php the_field('enterprise_comparison_title_2'); ?></p>
										<p class="price_unit"><?php the_field('enterprise_comparison_title_3'); ?></p>
									</div>
								</div>

								<?php } ?>
							</div>
						</div>

						</div>
					</div>
				</div>
			</div>



				<div class="container d-none d-lg-block"><hr/></div>
			  <?php require_once 'partials/partial-sections.php'; ?>
			</div>
		</div>
	<?php }else{ ?>
			<div class="container text-center mt-5 mb-5 pb-5 pt-5">
				<?php echo get_the_password_form(); ?>
			</div>
	<?php } ?>

<?php get_footer();
