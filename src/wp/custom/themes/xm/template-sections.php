<?php

/*
Template Name: Sections
*/

?>

<?php get_header(); ?>
	<?php if ( !post_password_required() ) {  ?>

			<div class="overflowX">
			   <?php require_once 'partials/partial-cover.php'; ?>
			<div class="has-header" id="site-content">
			   <?php require_once 'partials/partial-sections.php'; ?>
			</div>

			<?php if (have_rows('second-sections')): ?>
			<div class="second-sections">
				 	<?php require_once 'partials/partial-second-sections.php'; ?>
			</div>
	 </div>
			<?php endif; ?>

	<?php }else{ ?>
			<div class="container text-center mt-5 mb-5 pb-5 pt-5">
				<?php echo get_the_password_form(); ?>
			</div>
	<?php } ?>

<?php get_footer();
