<?php

$title = get_the_title();
$smallTitle = get_field('small_title');
$lead = get_field('lead');

if (get_field('alt_title')) {
	$title = get_field('alt_title');
}

/*
Template Name: Pricing
*/

?>

<?php get_header(); ?>
	<?php if ( !post_password_required() ) {  ?>

		<div class="page-cover full-width-graphics gradiant">
			<div class="cover-content">
				<div id="trigger-parallax1"></div>
		    <div class="container container-md">

		      <?php if ($smallTitle) : ?>
		        <p class="top-title"><?php echo $smallTitle; ?></p>
		      <?php endif; ?>

		      <?php if ($title) : ?>
		        <h1><?php echo $title; ?></h1>
		      <?php endif; ?>

		      <?php if ($lead) : ?>
		       <div class="lead"><p><?php echo $lead; ?></p></div>
		      <?php endif; ?>

				</div>

				<div class="section topMarginSmall bottomMarginSmall">

						<div class="content">
						<?php require 'partials/component/price_boxes.php'; ?>
						</div>

						<div class="graphic-container">
							<div class="graphics">
								<div id="parallax1" class="square yellow medium green pos1"></div>
								<div id="parallax2" class="square pink small blue pos2"></div>
								<div id="parallax3" class="square small blue pos2"></div>
								<div id="parallax4" class="square small lightblue pos2"></div>
							</div>
						</div>
				</div>
			</div>
		</div>





			<div class="has-header" id="site-content">
				<div class="page-content">

				<div class="section comparison bottomMarginSmall">

					<div class="section-header">
						<div class="container container-s">
							<h2>This is what you get</h2>
						</div>
					</div>

					<div class="container">
						<div class="comparison-table-wrapper">

						<div class="comparison-table" id="comparison-table">

							<div class="comparison-features">

								<div class="row product">
									<div class="col col-3 empty"></div>

									<?php if (get_field('express_show_on_website')) { ?>

									<div class="col">
										<div class="header">
											<h3><?php the_field('express_comparison_title_1'); ?></h3>

								      <?php
								        $link = get_field('express_link', 10);

					            	if ($link) :

					              $link_url = $link['url'];
					              $link_title = $link['title'];

					              $buttonCSS = "btn btn-primary small-bt";

					              if (get_field('express_outlined')){
					                $buttonCSS = "btn btn-outline-primary small-bt";
					              }
					              ?>

					              <a href="<?php echo esc_url( $link_url ); ?>" class="<?php echo $buttonCSS; ?>"><?php echo esc_html( $link_title ); ?></a>

					            <?php endif; ?>

											<p class="price"><?php the_field('express_comparison_title_2'); ?></p>
											<p class="price_unit"><?php the_field('express_comparison_title_3'); ?></p>
										</div>
									</div>

									<?php } ?>

									<?php if (get_field('business_show_on_website')) { ?>

									<div class="col">
										<div class="header">
											<h3><?php the_field('business_comparison_title_1'); ?></h3>
											<?php
												$link = get_field('business_link', 10);

												if ($link) :

												$link_url = $link['url'];
												$link_title = $link['title'];

												$buttonCSS = "btn btn-primary small-bt";

												if (get_field('business_outlined')){
													$buttonCSS = "btn btn-outline-primary small-bt";
												}
												?>

												<a href="<?php echo esc_url( $link_url ); ?>" class="<?php echo $buttonCSS; ?>"><?php echo esc_html( $link_title ); ?></a>

											<?php endif; ?>
											<p class="price"><?php the_field('business_comparison_title_2'); ?></p>
											<p class="price_unit"><?php the_field('business_comparison_title_3'); ?></p>
										</div>
									</div>

									<?php } ?>

									<?php if (get_field('enterprise_show_on_website')) { ?>

									<div class="col">
										<div class="header">
											<h3><?php the_field('enterprise_comparison_title_1'); ?></h3>
											<?php
												$link = get_field('enterprise_link', 10);

												if ($link) :

												$link_url = $link['url'];
												$link_title = $link['title'];

												$buttonCSS = "btn btn-primary small-bt";

												if (get_field('enterprise_outlined')){
													$buttonCSS = "btn btn-outline-primary small-bt";
												}
												?>

												<a href="<?php echo esc_url( $link_url ); ?>" class="<?php echo $buttonCSS; ?>"><?php echo esc_html( $link_title ); ?></a>

											<?php endif; ?>
											<p class="price"><?php the_field('enterprise_comparison_title_2'); ?></p>
											<p class="price_unit"><?php the_field('enterprise_comparison_title_3'); ?></p>
										</div>
									</div>

									<?php } ?>
								</div>

								<div id="features-list">

								<?php while (have_rows('pricing_features')) : the_row(); ?>

								<?php
									if (get_sub_field('headline_row')) {
								?>

								<a href="#" class="pricelist_feature_header">
									<div class="row divideheader">
											<div class="col col-12">
												<div class="price-label headline">
													<span><?php the_sub_field('label'); ?></span>
												</div>
											</div>
									</div>
								</a>

								<?php
									}else{
								?>

									<div class="row feature">
										<div class="col col-3">
											<div class="price-label">
												<span><?php the_sub_field('label'); ?></span>
												<i class="fal fa-question-circle tooltip" title="<?php the_sub_field('description'); ?>"></i>
											</div>
						        </div>

										<?php if (get_field('express_show_on_website')) { ?>
										<div class="col">
											<?php
												if (get_sub_field('express_value') == 'YES') {
									        echo '<i class="far fa-check"></i>';
									      }else if(get_sub_field('express_value') != ''){
													echo the_sub_field('express_value');
												}
											?>
						        </div>
										<?php } ?>

										<?php if (get_field('business_show_on_website')) { ?>
										<div class="col">
											<?php
												if (get_sub_field('business_value') == 'YES') {
									        echo '<i class="far fa-check"></i>';
									      }else if(get_sub_field('business_value') != ''){
													echo the_sub_field('business_value');
												}
											?>
						        </div>
										<?php } ?>

										<?php if (get_field('enterprise_show_on_website')) { ?>
										<div class="col">
											<?php
												if (get_sub_field('enterprice_value') == 'YES') {
									        echo '<i class="far fa-check"></i>';
									      }else if(get_sub_field('enterprice_value') != ''){
													echo the_sub_field('enterprice_value');
												}
											?>
						        </div>
										<?php } ?>

									</div>

								<?php
									}
								?>

								<?php endwhile; ?>
							</div>
							<div class="expand"><div class="expand-button"><a href="#" class="btn btn-light" id="expand-pricetable"><i class="far fa-chevron-double-down"></i> View all features</a></div></div>
						</div>

						</div>
					</div>
				</div>
			</div>



				<div class="container d-none d-lg-block"><hr/></div>
			  <?php require_once 'partials/partial-sections.php'; ?>
			</div>
		</div>
	<?php }else{ ?>
			<div class="container text-center mt-5 mb-5 pb-5 pt-5">
				<?php echo get_the_password_form(); ?>
			</div>
	<?php } ?>

<?php get_footer();
