<?php

/*
Template Name: Live demo
*/

get_header(); ?>

    <script>
	    jQuery(document).ready(function($) {
		    $("#start-live-demo").click(function(){

			    $.ajax({
				    url: ajaxurl,
				    type: 'post',
                    data: {
				        action: 'start_live_demo'
                    },
				    dataType: 'json',
				    success: function(response){
                $('#iframe-container').attr('src', response.primary).show();
                $('#guide-container span').html(response.secondary);
		            $('#guide-container').show();
				    },
			    });

			    return false;
		    });
	    });
    </script>
	<div class="container text-center">

  <div class="live-demo-mobile">
    <div class="mobile">
        <div class="mobile_overlay"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/phone_small.png" loading="lazy" alt="" /></div>
        <div class="screens">

      		<button id="start-live-demo" class="btn btn-outline-primary btn-xl">Try it out!</button>
          <iframe id="iframe-container" width="100%" height="600px" allow="camera;microphone" style="display: none;">

          </iframe>
          <div id="guide-container" style="display: none;">
              Open the following url on your mobile phone.
              <span></span>
          </div>

        </div>
    </div>
  </div>

	<div style="height: 10vh">

	</div>
</div>

<?php get_footer();
