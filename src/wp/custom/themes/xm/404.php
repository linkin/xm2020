<?php get_header(); ?>


<div class="" id="">


  <div class="pt-lg-5 mt-lg-5 mb-lg-5">
  	<div class="pt-5 pb-5">
      <div class="container container-cover-title pt-5 mt-5 mb-5 pb-5 text-center">

        <img src="<?php echo esc_url( get_field('404_image', 'options')['sizes']['medium'] ); ?>" alt="<?php echo esc_html( get_field('404_image', 'options')['alt'] ); ?>" loading="lazy" style="position: relative; z-index: -1; margin-bottom: -3rem; max-width: 340px; width: 100%;" />
        <h1 class="big-title"><strong><?php the_field('404_headline', 'options'); ?></strong></h1>

       <div class="lead"><p><strong><?php the_field('404_lead', 'options'); ?></strong></p></div>
       <?php the_field('404_text', 'options'); ?>

  		</div>
  	</div>
  </div>
</div>


<?php get_footer();
