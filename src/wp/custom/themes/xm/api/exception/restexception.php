<?php

class RestException extends Exception {

	private $_data;

	public function getData()
	{
		return $this->_data;
	}

	public function __construct(string $message, int $code, array $data = []) {
		$this->_data = $data;
		parent::__construct($message, $code);
	}
}

