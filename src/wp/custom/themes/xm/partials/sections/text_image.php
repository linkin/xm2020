<?php

$sectionClass = "section";
$containerClass = "container container-md";
$sectionId = "";
$sectionId = get_sub_field('anchorlink_id');
$top_margin = get_sub_field_object('top_margin')['value'];
$bottom_margin = get_sub_field_object('bottom_margin')['value'];
$sectionBackground = get_sub_field_object('background')['value'];
$backgroundGraphics = get_sub_field_object('background_graphics')['value'];
$round_image = get_sub_field('round_image');

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">


	<?php

	$imageOrVideo    = get_sub_field('image_or_video');

	$img_css = "";

	$rowCss = "row row-lg align-items-center";

	$col_1_css = "col-12 col-lg-6 mb-4 mb-lg-0";
	$col_2_css = "col-12 col-lg-6";


	if (get_sub_field('column_size') == 'largeImage') {

		$col_1_css = "col-12 col-lg-7 mb-4 mb-lg-0";
		$col_2_css = "col-12 col-lg-5";

	}else if (get_sub_field('column_size') == 'extraLargeImage') {

		$col_1_css = "col-12 col-lg-6 mb-4 mb-lg-0";
		$col_2_css = "col-12 col-lg-4";

		if (get_sub_field('image_position') == 'right') {
			$col_2_css = "col-12 col-lg-4 offset-lg-2";
		}
		$containerClass = "container container-xl";

	}

	if (get_sub_field('image_position') == 'right') {
		$col_1_css = $col_1_css . " order-lg-2";
	}

	if (get_sub_field('image_position') == 'top') {
		$col_1_css = "col-12 text-center mb-4";
		$col_2_css = "col-lg-8 offset-lg-2 text-center";
	}

	?>


	<div class="<?php echo $containerClass; ?>">

			<?php if ($imageOrVideo == "video") { ?>
				<div class="section-video" data="video">
			<?php }else{ ?>
				<div>
			<?php } ?>

					<div class="<?php echo $rowCss; ?>">
						<div class="<?php echo $col_1_css; ?>">

								<div class="graphic-container">

									<?php if ($imageOrVideo == "video") { ?>

										<div class="inline-video">
											<video class="" src="<?php echo get_sub_field('video_file')['url']; ?>" muted="" playsinline="" loop="" class="full"></video>
										</div>

									<?php }else if($imageOrVideo == "embeded") { ?>

										<div class="videoWrapper"><?php echo get_sub_field('embeded_video'); ?></div>

									<?php }else{ ?>

										<?php if ($round_image) { ?>

											<div class=""><div class="round-image">
						<?php
							echo wp_get_attachment_image(
								get_sub_field('image')['ID'],
								'medium_large',
								'',
								array(
									'class'  => 'img-fluid image',
									'srcset' => get_sub_field('image')['sizes']['large'] . ' 1024w,' . get_sub_field('image')['sizes']['medium_large'] . ' 768w,' .get_sub_field('image')['sizes']['medium'] . ' 300w',
									'sizes'  => '(min-width: 991px) 50vw, 90vw'
								)
							);
						?>
						</div></div>
										<?php }else{

											echo wp_get_attachment_image(
												get_sub_field('image')['ID'],
												'large',
												'',
												array(
													'class'  => 'img-fluid image ' . $img_css,
													'srcset' => get_sub_field('image')['sizes']['large'] . ' 1024w,' . get_sub_field('image')['sizes']['medium_large'] . ' 768w,' . get_sub_field('image')['sizes']['medium'] . ' 300w',
													'sizes'  => '(min-width: 991px) 1024px, 90vw'
												)
											);

										} ?>

									<?php } ?>

									<div class="graphics <?php echo $backgroundGraphics; ?>">
										<div id="" class="square medium green position1"></div>
										<div id="" class="square small blue position2"></div>
										<div class="dotts"></div>
									</div>
								</div>

						</div>
						<div class="<?php echo $col_2_css; ?>">

							<div class="zebra-content">
								<p class="top-title"><?php the_sub_field('small_title'); ?></p>
								<h2><?php the_sub_field('title'); ?></h2>
								<div><?php the_sub_field('text'); ?></div>
							</div>

						</div>
					</div>
				</div>
	</div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
