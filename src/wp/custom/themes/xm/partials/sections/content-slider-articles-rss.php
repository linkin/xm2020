<?php

$sectionClass = "section";
$sectionId = "";
$sectionId = get_sub_field('anchorlink_id');
$top_margin = get_sub_field_object('top_margin')['value'];
$bottom_margin = get_sub_field_object('bottom_margin')['value'];
$sectionBackground = get_sub_field_object('background')['value'];

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<div class="container">
	<?php if (get_sub_field('title')) : ?>
		<div class="section-header text-left">
			<div class="row align-items-end">
				<div class="col-lg-6">
					<?php if (get_sub_field('small_title')) : ?><p class="top-title"><?php echo get_sub_field('small_title'); ?></p><?php endif; ?>
					<h3><?php echo get_sub_field('title'); ?></h3>
				</div>
				<div class="col-lg-6 text-lg-right d-none d-lg-block">
					<a href="<?php the_sub_field('view_more_url'); ?>" target="_blank" class="arrow">View all articles</a>
				</div>
			</div>
		</div>
	<?php endif; ?>


		<div class="content-list">


				<div class="slick-slider article-slider">
				<?php  $items = RSSFromHubspot();	?>
				<?php if (count($items) > 0) : ?>
				<?php foreach ($items as $item): ?>

					<?php

					$link = $item->url;
					$gray_label = "";
					$title = $item->title;
					$excerpt = wp_trim_words($item->description, 15, "&hellip;");
                    $imagesize = "";
                    $category = join( ", ", $item->categories );
                    $target = '_blank';

					$image = $item->image;

					$terms = '';

					require get_template_directory() . '/partials/component/article-card.php';
					?>

				<?php endforeach; ?>
				<?php endif; ?>
				</div>



  </div>
	<div class="d-block d-lg-none mt-4 text-center">
		<a href="<?php the_sub_field('view_more_url'); ?>" target="_blank" class="arrow">View all articles</a>
	</div>
</div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
