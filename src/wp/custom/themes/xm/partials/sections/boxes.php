<?php

$sectionClass = "section";
$sectionId = "";
$sectionId = get_sub_field('anchorlink_id');
$top_margin = get_sub_field_object('top_margin')['value'];
$bottom_margin = get_sub_field_object('bottom_margin')['value'];
$sectionBackground = get_sub_field_object('background')['value'];

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<?php if (get_sub_field('title')) : ?>
		<div class="section-header">
			<div class="container container-s">
				<?php if (get_sub_field('small_title')) : ?>
					<p class="top-title"><?php echo get_sub_field('small_title'); ?></p>
				<?php endif; ?>

				<?php if (get_sub_field('title')) : ?>
					<h2><?php echo get_sub_field('title'); ?></h2>
				<?php endif; ?>

				<?php if (get_sub_field('lead')) : ?>
				 <div class="lead"><p><?php echo get_sub_field('lead'); ?></p></div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

	<div class="container container-md">

			<div class="row justify-content-center mb-5">

			<?php while (have_rows('boxes')): the_row(); ?>

				<div class="col-12 mb-3 col-md-4 mb-md-0">

					<div class="textbox text-center">

						<?php if (get_sub_field('title')) { ?>
							<p class="top-title"><?php the_sub_field('title'); ?></p>
						<?php } ?>

						<?php if (get_sub_field('content')) { ?>
							<div><?php the_sub_field('content'); ?></div>
						<?php } ?>

					</div>
				</div>

			<?php endwhile; ?>

			</div>
		</div>
	</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
