<?php

$sectionClass = "section";
$sectionId = '';
$sectionId = get_sub_field('anchorlink_id');
$top_margin = get_sub_field_object('top_margin')['value'];
$bottom_margin = get_sub_field_object('bottom_margin')['value'];
$sectionBackground = get_sub_field_object('background')['value'];

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<div class="container">
	<?php if (get_sub_field('title')) : ?>
		<div class="section-header text-left">
			<div class="row">
				<div class="col-lg-6">
					<?php if (get_sub_field('small_title')) : ?><p class="top-title"><?php echo get_sub_field('small_title'); ?></p><?php endif; ?>
					<h3><?php echo get_sub_field('title'); ?></h3>
				</div>
				<div class="col-lg-6 text-lg-right d-none d-lg-block">
					<a href="<?php echo esc_url( get_permalink(12) ); ?>" class="arrow">View more insights</a>
				</div>
			</div>
		</div>
	<?php endif; ?>


		<div class="content-list">


				<div class="slick-slider article-slider">

			<?php
			$post_objects = get_sub_field('items');

			if( $post_objects ): ?>
					<?php foreach( $post_objects as $post_object): ?>

					<?php


			    $postType = get_post_type_object(get_post_type($post_object->ID));
			    if ($postType) {
			        $postType = esc_html($postType->labels->singular_name);
			    }


					$link          = get_permalink($post_object->ID);
					$category      = '';
					$gray_label    = '';
					$title         = get_the_title($post_object->ID);
					$excerpt       = excerpt(13, $post_object->ID);
          			$imagesize     = '';
					$image         = '';
					$customer      = '';
					$customer_logo = '';
					$customer_name = '';

					if ( get_field('thumbnail_background', $post_object->ID ) === 'image' && get_field( 'thumbnail_image', $post_object->ID ) ) {
						$image = get_field('thumbnail_image', $post_object->ID);
					}


					if ($postType === 'Story') {
							$category      = 'Customer Story';
							$customer      = get_field('customer', $post_object->ID);
							$customer_logo = get_field('black_logotype', $customer->ID)['sizes']['medium'];
							$customer_name = get_the_title($customer->ID);
					} else if ($postType === 'Article') {
							$category = 'Article';
					}


					require get_template_directory() . '/partials/component/article-card.php';
					?>

						<?php endforeach; ?>
				<?php endif; ?>
				</div>



  </div>

	<div class="d-block d-lg-none mt-4 text-center">
		<a href="<?php echo esc_url( get_permalink(12) ); ?>" class="arrow">View more insights</a>
	</div>

</div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
