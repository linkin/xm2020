<?php

$sectionClass = "section";
$sectionId = '';
$sectionId = get_sub_field('anchorlink_id');
$top_margin = get_sub_field_object('top_margin')['value'];
$bottom_margin = get_sub_field_object('bottom_margin')['value'];
$sectionBackground = get_sub_field_object('background')['value'];

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<div class="container">
	<?php if (get_sub_field('title')) : ?>
		<div class="section-header text-left">
			<div class="row align-items-end">
				<div class="col-lg-6">
					<?php if (get_sub_field('small_title')) : ?><p class="top-title"><?php echo get_sub_field('small_title'); ?></p><?php endif; ?>
					<h3><?php echo get_sub_field('title'); ?></h3>
				</div>
				<div class="col-lg-6 text-lg-right">
					<a href="<?php echo esc_url( get_permalink(700) ); ?>" class="arrow">View all case studies</a>
				</div>
			</div>
	  </div>
	<?php endif; ?>


		<div class="slick-slider stories-slider">



			<?php if (get_sub_field('all_stories')) : ?>



				<?php
				$args = array( 'post_type' => 'story', 'posts_per_page' => 10 );
				$the_query = new WP_Query( $args );
				?>
				<?php if ( $the_query->have_posts() ) : ?>
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

					<?php
						$background    = get_field('thumbnail_background');
						$customer      = get_field('customer');
						$customer_logo = get_field('black_logotype', $customer->ID)['sizes']['medium'];
						$category      = '';
						$link          = get_the_permalink();
						$image         = '';
						$title         = '';

						if ( get_field('thumbnail_background') === 'image' ) {
							if ( get_field('thumbnail_image') ) {
								$image = get_field('thumbnail_image');
							}
							$customer_logo = get_field('white_logotype', $customer->ID)['sizes']['medium'];
							$cardClass     = "card white";
						}

						$terms = get_the_terms(get_the_ID(), 'story_category');

						if ( ! empty( $post_categories ) && ! is_wp_error( $post_categories ) ) {
						    //$category = join(', ', wp_list_pluck($terms, 'name'));
						}

						$catchphrase = get_field('quote');

						require get_template_directory() . '/partials/component/card.php';
					?>

				<?php wp_reset_postdata(); ?>
				<?php endwhile; ?>
				<?php endif; ?>

			<?php else: ?>


				<?php
				$post_objects = get_sub_field('stories');

				if( $post_objects ): ?>
						<?php foreach( $post_objects as $post_object): ?>


							<?php
								$background    = get_field('thumbnail_background', $post_object->ID);
								$customer      = get_field('customer', $post_object->ID);
								$customer_logo = get_field('black_logotype', $customer->ID)['sizes']['medium'];
								$category      = "Digital";
								$link          = get_the_permalink();
								$image         = '';

								if ( get_field('thumbnail_background', $post_object->ID) === 'image' ) {

									if ( get_field('thumbnail_image', $post_object->ID) ) {
										$image = get_field('thumbnail_image', $post_object->ID);
									}
									$customer_logo = get_field('white_logotype', $customer->ID)['sizes']['medium'];
									$cardClass     = "card white";
								}

								$catchphrase = get_field('quote', $post_object->ID);

								require get_template_directory() . '/partials/component/card.php';
							?>


						<?php endforeach; ?>
				<?php endif; ?>

			<?php endif; ?>


		</div>
  </div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
