<?php

$sectionClass = "section";
$sectionId = "";
$sectionId = get_sub_field('anchorlink_id');
$top_margin = get_sub_field_object('top_margin')['value'];
$bottom_margin = get_sub_field_object('bottom_margin')['value'];
$sectionBackground = get_sub_field_object('background')['value'];
$backgroundGraphics = get_sub_field_object('background_graphics')['value'];

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">


  <div class="container container-md">

      <?php

      $img_css = "";

      $rowCss = "row row-lg";

      $col_1_css = "col-12 col-lg-6 mb-4 mb-lg-0";
      $col_2_css = "col-12 col-lg-6 mt-3 mt-lg-0 pt-lg-6";


      if (get_sub_field('image_position') == 'right') {
        $col_1_css = $col_1_css . " order-lg-2";
      }

      ?>


			<div class="tab-content" id="pills-tabContent" data="video">
				<div class="<?php echo $rowCss; ?>">
					<div class="<?php echo $col_1_css; ?>">
						<div class="graphic-container">

						<?php $i = 0; ?>
						<?php while (have_rows('slides')): the_row(); ?>

							<div class="tab-pane fade <?php echo ($i == 0 ? 'show' : '' ) ?>" id="tab-image-<?php echo $i; ?>">

							<?php if (get_sub_field('video_file')) { ?>

								  <div class="section-video" data="video">
									<div class="d-none d-lg-block inline-video" id="tab-video-<?php echo $i; ?>">
										<video class="" src="<?php echo get_sub_field('video_file')['url']; ?>" muted="" playsinline="" loop="" class="full"></video>
									</div>

									<?php if (get_sub_field('image')) { ?>
										<div class="d-block d-lg-none">

											<?php
											echo wp_get_attachment_image(
												get_sub_field('image')['ID'],
												'large',
												'',
												array(
													'class'  => 'img-fluid image ' . $img_css,
													'srcset' => get_sub_field('image')['sizes']['large'] . ' 1024w,' . get_sub_field('image')['sizes']['medium_large'] . ' 768w,' . get_sub_field('image')['sizes']['medium'] . ' 300w',
													'sizes'  => '(min-width: 991px) 1024px, 90vw'
												)
											);
											?>

										</div>
									<?php } ?>
									</div>


							<?php }else{ ?>

									<?php if (get_sub_field('image')) {

									echo wp_get_attachment_image(
										get_sub_field('image')['ID'],
										'large',
										'',
										array(
											'class' => 'img-fluid image ' . $img_css,
											'srcset' => get_sub_field('image')['sizes']['large'] . ' 1024w,' . get_sub_field('image')['sizes']['medium_large'] . ' 768w,' . get_sub_field('image')['sizes']['medium'] . ' 300w',
											'sizes' => '(min-width: 991px) 1024px, 90vw'
										)
									);
										
									} ?>

							<?php } ?>

						</div>

						<?php $i++; ?>
						<?php endwhile; ?>


						<div class="graphics <?php echo $backgroundGraphics; ?>">
							<div id="" class="square medium green position1"></div>
							<div id="" class="square small blue position2"></div>
							<div class="dotts"></div>
						</div>
						</div>
					</div>

					<div class="<?php echo $col_2_css; ?>">

						<?php if (get_sub_field('small_title')) : ?>
							<p class="top-title"><?php echo get_sub_field('small_title'); ?></p>
						<?php endif; ?>

						<?php if (get_sub_field('title')) : ?>
							<h2><?php echo get_sub_field('title'); ?></h2>
						<?php endif; ?>

						<?php if (get_sub_field('lead')) : ?>
						 <div class="lead"><p><?php echo get_sub_field('lead'); ?></p></div>
						<?php endif; ?>



						<div class="tabs-left mb-4">
							<ul id="tabNav" class="nav nav-pills flex-nowrap flex-lg-wrap" role="tablist">
								<?php $i = 0; ?>
								<?php while (have_rows('slides')): the_row(); ?>
									<li class="nav-item">
										<a class="nav-link <?php echo ($i == 0 ? 'active' : '' ) ?>" data-id="<?php echo $i; ?>" data-toggle="tab" href="#tab-<?php echo $i; ?>" aria-selected="true">
											<?php the_sub_field('button_name'); ?>
										</a>
									</li>
									<?php $i++; ?>
								<?php endwhile; ?>
							</ul>
						</div>

						<?php
							$i = 0;
							$slidesCount = count(get_sub_field('slides'));
						?>
						<?php while (have_rows('slides')): the_row(); ?>

							<div class="tab-pane fade <?php echo ($i == 0 ? 'show' : '' ) ?>" id="tab-content-<?php echo $i; ?>">
		            <div class="zebra-content">
									<?php if (get_sub_field('content')) { ?>
										<div><?php the_sub_field('content'); ?></div>
									<?php } ?>
		            </div>

								<div class="mt-3">
								<?php if ($i < $slidesCount-1) : ?>
									<strong><a class="next-tab" data-id="<?php echo $i+1; ?>" href="#tab-<?php echo $i + 1; ?>"><i class="far fa-chevron-circle-right"></i> Next</a></strong>
								<?php else: ?>

									<?php if (get_the_ID() == 8) : ?>
									<strong><a class="scrollTo" href="#features"><i class="far fa-chevron-circle-down"></i> See all features</a></strong>
									<?php else: ?>
									<strong><a class="" href="<?php echo get_home_url(); ?>/how-it-works#features"><i class="far fa-chevron-circle-right"></i> See all features</a></strong>
									<?php endif; ?>

								<?php endif; ?>
								</div>

							</div>

						<?php $i++; ?>
						<?php endwhile; ?>



					</div>

				</div>
			</div>


  </div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
