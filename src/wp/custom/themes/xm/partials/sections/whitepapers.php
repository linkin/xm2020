<?php

$sectionClass = "section";
$sectionId = "";
$sectionId = get_sub_field('anchorlink_id');

$top_margin = "";
$bottom_margin = "";
$sectionBackground = "";

if (get_sub_field('top_margin')) {
	$top_margin = get_sub_field_object('top_margin')['value'];
}
if (get_sub_field('bottom_margin')) {
	$bottom_margin = get_sub_field_object('bottom_margin')['value'];
}
if (get_sub_field('background')) {
	$sectionBackground = get_sub_field_object('background')['value'];
}

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}©

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<div class="container">
	<?php if (get_sub_field('title')) : ?>
		<div class="section-header text-left">
				<div class="row align-items-end">
					<div class="col-lg-6">
						<?php if (get_sub_field('small_title')) : ?><p class="top-title"><?php echo get_sub_field('small_title'); ?></p><?php endif; ?>
						<h3><?php echo get_sub_field('title'); ?></h3>
					</div>
					<div class="col-lg-6 text-lg-right d-none d-lg-block">
						<a href="<?php echo esc_url( get_permalink(774) ); ?>" class="arrow">View all whitepapers</a>
					</div>
				</div>
	  </div>
	<?php endif; ?>

	<div class="content-list">
		<div class="row">
			<?php
			$post_objects = get_sub_field('whitepapers');

			if( $post_objects ): ?>
				<?php foreach( $post_objects as $post_object): ?>
					<?php setup_postdata($post); ?>
					<div class="col-lg-6 mb-lg-5">
                        <?php
                            $url =  get_permalink($post_object->ID);
                            $target = false;
                            if (get_field('external_url', $post_object->ID)) {
                                $url = get_field('external_url', $post_object->ID);
                                $target = true;
                            }
                        ?>
						<a href="<?php echo $url ?>" <?php echo ($target) ? 'target="_blank"' : '' ?>>
							<div class="row">
								<div class="col-4 col-lg-5">
									<img src="<?php echo esc_url( get_field('image', $post_object->ID)['sizes']['medium'] ); ?>" loading="lazy" alt="<?php echo esc_html( get_field('image', $post_object->ID)['alt'] ); ?>" class="shaddow" />
								</div>
								<div class="col-8 col-lg-7">
									<h4><?php echo get_the_title($post_object->ID); ?></h4>
									<p><?php echo excerpt(20, $post_object->ID); ?></p>
									<p class="arrow">Download whitepaper</p>
								</div>
							</div>
						</a>
					</div>
					<?php wp_reset_postdata(); ?>
				<?php endforeach; ?>
			<?php else: ?>

				<?php
				$args = array( 'post_type' => 'whitepaper', 'posts_per_page' => 4 );
				$the_query = new WP_Query( $args );
				?>
				<?php if ( $the_query->have_posts() ) : ?>
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

					<?php
						$customer = get_field('customer');
						$link = get_the_permalink();
						$image = get_field('image')['sizes']['large'];
						$title = get_the_title();
					?>

						<?php
						$url =  get_permalink();
						$target = false;
						if (get_field('external_url')) {
							$url = get_field('external_url');
							$target = true;
						}
						?>

					<div class="col-lg-6 mb-lg-5">
                        <a href="<?php echo $url ?>" <?php echo ($target) ? 'target="_blank"' : '' ?>>
							<div class="row">
								<div class="col-4 col-lg-5">
									<img src="<?php echo esc_url( get_field('image')['sizes']['medium'] ); ?>" loading="lazy" alt="<?php echo esc_html( get_field('image')['alt'] ); ?>" class="shaddow" />
								</div>
								<div class="col-8 col-lg-7">
									<h4><?php echo get_the_title(); ?></h4>
									<p><?php echo excerpt(20); ?></p>
									<p class="arrow">Download whitepaper</p>
								</div>
							</div>
						</a>
					</div>

				<?php wp_reset_postdata(); ?>
				<?php endwhile; ?>
				<?php endif; ?>


			<?php endif; ?>
	  </div>
	  </div>
		<div class="d-block d-lg-none mt-4 text-center">
			<a href="<?php echo esc_url( get_permalink(774) ); ?>" class="arrow">View all whitepapers</a>
		</div>
	</div>
</div>


<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
