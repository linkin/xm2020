<?php

$sectionClass = "section";
$sectionId = "";
$sectionId = get_sub_field('anchorlink_id');

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<div class="container">
	<?php if (get_sub_field('title')) : ?>
		<div class="section-header text-left">
				<div class="row align-items-end">
					<div class="col-lg-6">
						<?php if (get_sub_field('small_title')) : ?><p class="top-title"><?php echo get_sub_field('small_title'); ?></p><?php endif; ?>
						<h3><?php echo get_sub_field('title'); ?></h3>
					</div>
					<div class="col-lg-6 text-lg-right d-none d-lg-block">
						<a href="<?php echo esc_url( get_permalink(717) ); ?>" class="arrow">View all articles</a>
					</div>
				</div>
	  </div>
	<?php endif; ?>


	<div class="content-list">
		<div class="row">


			<?php
			$post_objects = get_sub_field('articles');

			if( $post_objects ): ?>
				<?php foreach( $post_objects as $post_object): ?>

					<div class="col-lg-4">
						<a href="<?php echo get_permalink($post_object->ID); ?>">
							<div class="row">
								<div class="col-4 col-lg-5 mb-3 mb-lg-0">
									<img src="<?php echo get_field('thumbnail_image', $post_object->ID)['sizes']['thumbnail']; ?>" loading="lazy" class="round-corners" />
								</div>
								<div class="col-8 col-lg-7">
									<h4><?php echo get_the_title($post_object->ID); ?></h4>
								</div>
							</div>
						</a>
					</div>

				<?php endforeach; ?>
			<?php endif; ?>




	  </div>
	</div>
	<div class="d-block d-lg-none mt-4">
		<a href="<?php echo esc_url( get_permalink(717) ); ?>" class="arrow">View all articles</a>
	</div>
	</div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
