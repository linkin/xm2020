<?php

$sectionClass = "section";
$sectionId = "";
$sectionId = get_sub_field('anchorlink_id');
$top_margin = get_sub_field_object('top_margin')['value'];
$bottom_margin = get_sub_field_object('bottom_margin')['value'];
$sectionBackground = get_sub_field_object('background')['value'];

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<?php if (get_sub_field('title')) : ?>
		<div class="section-header">
			<div class="container container-s">
				<?php if (get_sub_field('small_title')) : ?>
					<p class="top-title"><?php echo get_sub_field('small_title'); ?></p>
				<?php endif; ?>

				<?php if (get_sub_field('title')) : ?>
					<h2><?php echo get_sub_field('title'); ?></h2>
				<?php endif; ?>

				<?php if (get_sub_field('lead')) : ?>
				 <div class="lead"><p><?php echo get_sub_field('lead'); ?></p></div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

	<div class="container container-s">
			<div class="row">
					<div class="col-12">
	            <table class="table table-hover list-table">
	                <tbody>
										<?php while (have_rows('list')) : the_row(); ?>
	                    <tr>
	                        <td>
	                            <strong><a href="<?php the_sub_field('file'); ?>"><?php the_sub_field('title'); ?></a></strong>
	                        </td>
	                        <td class="text-right"><?php the_sub_field('description'); ?></td>
	                    </tr>
										<?php endwhile; ?>
	                </tbody>
	         		</table>
	         </div>
			</div>
		</div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
