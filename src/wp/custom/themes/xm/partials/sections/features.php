<?php

$sectionClass = "section";
$sectionId = "";
$sectionId = get_sub_field('anchorlink_id');

$top_margin = "";
$bottom_margin = "";
$sectionBackground = "";

if (get_sub_field('top_margin')) {
	$top_margin = get_sub_field_object('top_margin')['value'];
}
if (get_sub_field('bottom_margin')) {
	$bottom_margin = get_sub_field_object('bottom_margin')['value'];
}
if (get_sub_field('background')) {
	$sectionBackground = get_sub_field_object('background')['value'];
}

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}


?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">



	<?php if (get_sub_field('title')) : ?>
		<div class="section-header">
			<div class="container container-s">
				<?php if (get_sub_field('small_title')) : ?>
					<p class="top-title"><?php echo get_sub_field('small_title'); ?></p>
				<?php endif; ?>

				<?php if (get_sub_field('title')) : ?>
					<h2><?php echo get_sub_field('title'); ?></h2>
				<?php endif; ?>

				<?php if (get_sub_field('lead')) : ?>
				 <div class="lead"><p><?php echo get_sub_field('lead'); ?></p></div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>


	<div class="container" data="video">
			<div class="row row-lg align-items-center">

					<div class="col-lg-4 order-lg-2">

						<div class="graphic-container">
							<div class="graphics feature-graphics">
								<div id="parallax5" class="square medium blue feature1"></div>
								<div id="parallax6" class="square small green feature2"></div>
							</div>
						</div>

						<div class="mobile feature-mobile">

								<div class="mobile_overlay"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/phone_small.png" loading="lazy" alt="" /></div>
								<div class="screens" id="screens">

									<?php
										$rows = get_sub_field('features');

										if ($rows) :
										$count = 1;
										$imageCss = "show";
									?>

										<?php foreach($rows as $row) : ?>

												<div class="content <?php echo $imageCss; ?>" id="feature-image-<?php echo $count; ?>">

												<?php if ($row['video_link']) { ?>

														<div class="section-video" data="video">
														<div class="d-none d-lg-block inline-video" id="tab-video-<?php echo $i; ?>">
															<video class="" src="<?php echo $row['video_link']['url']; ?>" muted="" playsinline="" loop="" class="full"></video>
														</div>

														<?php if ($row['image']) { ?>
															<div class="d-block d-lg-none">
																<img src="<?php echo esc_url( $row['image']['sizes']['medium'] ); ?>" loading="lazy" alt="<?php echo esc_html( $row['image']['alt'] ); ?>" />
															</div>
														<?php } ?>
														</div>


												<?php }else{ ?>

														<?php if ($row['image']) { ?>
															<img src="<?php echo esc_url( $row['image']['sizes']['medium'] ); ?>" loading="lazy" alt="<?php echo esc_html( $row['image']['alt'] ); ?>" />
														<?php } ?>

												<?php } ?>

												</div>

												<?php
													if ($count == 1) {
														$imageCss = "";
													}
													$count++;
												?>

										<?php endforeach; ?>

									<?php endif; ?>

								</div>
						</div>

						<div class="graphic-container">
							<div class="graphics feature-graphics">
								<div id="" class="square big pink feature3"></div>
								<div id="" class="square small yellow feature4"></div>
							</div>
						</div>
					</div>

					<div class="col-lg-4 order-lg-1">
							<div class="feature-list left text-lg-right" id="feature-list-left">

									<?php

										if ($rows) :
										$count = 1;
										$linkCss = "active";
									?>

										<?php foreach($rows as $row) : ?>

												<div class="feature">
													<a href="#" data-id="<?php echo $count; ?>" id="" class="<?php echo $linkCss; ?>">
															<div class="row row-small">
																<div class="col-2 col-lg-12 text-center text-lg-right">
																	<img src="<?php echo $row['icon']['sizes']['thumbnail']; ?>" loading="lazy" alt="" class="icon" />
																</div>
																<div class="col-10 col-lg-12">
																	<?php if ($row['title']) { ?><h4><?php echo $row['title']; ?></h4><?php } ?>
																	<?php if ($row['content']) { ?><p><?php echo $row['content']; ?></p><?php } ?>
																</div>
															</div>
													</a>
												</div>

												<?php
													$count++;
											    if ($count == 2) {
														$linkCss = "";
											    }else if ($count == 4) {
											      break;
											    }
												?>

										<?php endforeach; ?>

									<?php endif; ?>

							</div>
					</div>

					<div class="col-lg-4 order-lg-3">
							<div class="feature-list" id="feature-list-right">

								<?php
									if ($rows) :
									$count = 1;
								?>

									<?php foreach($rows as $row) : ?>

											<?php
												if ($count > 3) :
											?>

											<div class="feature">
												<a href="#" data-id="<?php echo $count; ?>" id="">
													<div class="row row-small">
														<div class="col-2 col-lg-12 text-center text-lg-left">
															<img src="<?php echo $row['icon']['sizes']['thumbnail']; ?>" loading="lazy" alt="" class="icon" />
														</div>
														<div class="col-10 col-lg-12">
															<?php if ($row['title']) { ?><h4><?php echo $row['title']; ?></h4><?php } ?>
															<?php if ($row['content']) { ?><p><?php echo $row['content']; ?></p><?php } ?>
														</div>
													</div>
												</a>
											</div>

										<?php	 endif; ?>

										<?php
											$count++;
										?>
									<?php endforeach; ?>

								<?php endif; ?>


							</div>
					</div>
		</div>
	</div>

</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
