<?php

$sectionClass = "section";
$sectionId = "";

$logoGroupClass = "logo-group";
$colClass = "col-4 col-lg-2 mb-5";

if(get_sub_field('bigger_logotypes')){
	$colClass = "col-4 col-lg-3 mb-5";
	$logoGroupClass = "logo-group big";
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<div class="container">

	<?php if (get_sub_field('title') || get_sub_field('small_title')) : ?>
		<div class="section-header text-left">
			<p class="top-title"><?php echo get_sub_field('small_title'); ?></p>
			<h3><?php echo get_sub_field('title'); ?></h3>
		</div>
	<?php endif; ?>

		<div class="row row-lg justify-content-left align-items-left text-left <?php echo $logoGroupClass; ?>">
			<?php
			$count = 0;

			if(get_sub_field('selected')){
			$post_objects = get_sub_field('customers');

			if( $post_objects ): ?>
			    <?php foreach( $post_objects as $post_object): ?>


					<div class="<?php echo $colClass; ?>">
						<img src="<?php echo esc_url( get_field('color_logotype', $post_object->ID)['sizes']['medium'] ); ?>" loading="lazy" alt="<?php echo esc_url( get_field('color_logotype', $post_object->ID)['alt']); ?>" />
					</div>


			    <?php endforeach; ?>
			<?php endif; ?>

			<?php
			}else{
			?>

			<?php
			$args = array( 'post_type' => 'customer' );
			$the_query = new WP_Query( $args );
			?>
			<?php if ( $the_query->have_posts() ) : ?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


				<div class="<?php echo $colClass; ?>">
					<img src="<?php  echo esc_url( get_field('color_logotype')['sizes']['medium'] ); ?>" loading="lazy" alt="<?php echo esc_url( get_field('color_logotype')['alt']); ?>" />
				</div>


			<?php wp_reset_postdata(); ?>
			<?php endwhile; ?>
			<?php endif; ?>
			<?php } ?>


		</div>
	</div>
</div>
