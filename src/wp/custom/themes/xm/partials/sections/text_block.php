<?php

$sectionClass = "section";
$sectionId = "";
$contentContainerClass = "container container-s";

$sectionId = get_sub_field('anchorlink_id');
$top_margin = get_sub_field_object('top_margin')['value'];
$bottom_margin = get_sub_field_object('bottom_margin')['value'];
$sectionBackground = get_sub_field_object('background')['value'];
$contentWidth = get_sub_field_object('width')['value'];

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}

if ($contentWidth == "medium") {
	$contentContainerClass = "container";
}else if ($contentWidth == "big") {
	$contentContainerClass = "container container-xl";
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">
  <div>

		<?php if (get_sub_field('title')) : ?>
		  <div class="section-header">
		    <div class="container container-s">
					<?php if (get_sub_field('small_title')) : ?>
						<p class="top-title"><?php echo get_sub_field('small_title'); ?></p>
					<?php endif; ?>

					<?php if (get_sub_field('title')) : ?>
						<h2><?php echo get_sub_field('title'); ?></h2>
					<?php endif; ?>

					<?php if (get_sub_field('lead')) : ?>
					 <div class="lead"><p><?php echo get_sub_field('lead'); ?></p></div>
					<?php endif; ?>
		    </div>
		  </div>
		<?php endif; ?>
  </div>

	<?php if (get_sub_field('content')) : ?>
			<div class="<?php echo $contentContainerClass; ?>">
				<?php echo get_sub_field('content'); ?>
			</div>
	<?php endif; ?>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
