<?php

$sectionClass = "section";
$sectionId = "";
$sectionId = get_sub_field('anchorlink_id');

$top_margin = "";
$bottom_margin = "";
$sectionBackground = "";

$logoGroupClass = "logo-group";
$colClass = "col-4 col-lg-2 mb-4";

if(get_sub_field('bigger_logotypes')){
	$colClass = "col-4 col-lg-3 mb-4";
	$logoGroupClass = "logo-group big";
}

if (get_sub_field('top_margin')) {
	$top_margin = get_sub_field_object('top_margin')['value'];
}
if (get_sub_field('bottom_margin')) {
	$bottom_margin = get_sub_field_object('bottom_margin')['value'];
}
if (get_sub_field('background')) {
	$sectionBackground = get_sub_field_object('background')['value'];
}

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}


?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<div class="container">
	<?php if (get_sub_field('title') || get_sub_field('small_title')) : ?>
		<div class="section-header text-center">
			<?php if (get_sub_field('small_title')) : ?><p class="top-title"><?php echo get_sub_field('small_title'); ?></p><?php endif; ?>
			<?php if (get_sub_field('title')) : ?><h3><?php echo get_sub_field('title'); ?></h3><?php endif; ?>
		</div>
	<?php endif; ?>

		<div class="text-center">
			<div class="row row-lg justify-content-center align-items-center text-center <?php echo $logoGroupClass; ?>">
			<?php
			$count = 0;

			if(get_sub_field('selected')){
			$post_objects = get_sub_field('customers');

			if( $post_objects ): ?>
			    <?php foreach( $post_objects as $post_object): ?>
						  <div class="<?php echo $colClass; ?>">
								<img src="<?php echo esc_url( get_field('color_logotype', $post_object->ID)['sizes']['medium'] ); ?>" loading="lazy" alt="<?php echo esc_url( get_field('color_logotype', $post_object->ID)['alt']); ?>" />
					    </div>


			    <?php endforeach; ?>
			<?php endif; ?>

			<?php
			}else{
			?>

			<?php
			$args = array( 'post_type' => 'customer' );
			$the_query = new WP_Query( $args );
			?>
			<?php if ( $the_query->have_posts() ) : ?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


				<div class="<?php echo $colClass; ?>">
					<img src="<?php  echo esc_url( get_field('color_logotype')['sizes']['medium'] ); ?>" loading="lazy" alt="<?php echo esc_url( get_field('color_logotype')['alt']); ?>" />
				</div>


			<?php wp_reset_postdata(); ?>
			<?php endwhile; ?>
			<?php endif; ?>
			<?php } ?>


			</div>
		</div>
	</div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
