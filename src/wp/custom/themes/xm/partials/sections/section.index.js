import React from 'react';
import SectionStandard from './section.standard';
import SectionAccordion from "./section.accordion";

const Section = (props) => {

	switch (props.acf_fc_layout) {
		case 'standard':
			return <SectionStandard {...props} />
		case 'accordion':
			return <SectionAccordion {...props} />
	}
	return null;
}

export default Section;
