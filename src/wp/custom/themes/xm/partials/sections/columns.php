<?php

$sectionClass = "section";
$sectionId = "";
$sectionId = get_sub_field('anchorlink_id');
$top_margin = get_sub_field_object('top_margin')['value'];
$bottom_margin = get_sub_field_object('bottom_margin')['value'];
$sectionBackground = get_sub_field_object('background')['value'];

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<?php if (get_sub_field('title')) : ?>
		<div class="section-header">
			<div class="container container-s">
				<?php if (get_sub_field('small_title')) : ?>
					<p class="top-title"><?php echo get_sub_field('small_title'); ?></p>
				<?php endif; ?>

				<?php if (get_sub_field('title')) : ?>
					<h2><?php echo get_sub_field('title'); ?></h2>
				<?php endif; ?>

				<?php if (get_sub_field('lead')) : ?>
				 <div class="lead"><p><?php echo get_sub_field('lead'); ?></p></div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

	<div class="container">

			<?php

			$columns = count(get_sub_field('columns'));

			$colClass  = "col-12 mb-3 col-md-6 mb-md-0";
			$rowClass  = "row row-lg";

			if ($columns == "3") {
				$colClass = "col-12 mb-3 col-md-4 mb-md-0";
			}

			if ($columns == "4") {
				$colClass = "col-6 mb-3 col-md-3 mb-md-0";
			}

			?>

			<div class="<?php echo $rowClass ?>">

			<?php while (have_rows('columns')): the_row(); ?>

				<?php
				$image    = get_sub_field('icon')['sizes']['large'];
				$cssClass = "text-md-center";

				?>

				<div class="<?php echo $colClass; ?>">
					<div class="<?php echo $cssClass; ?>">

						<?php if ($image) { ?>
							<img src="<?php echo esc_url( get_sub_field('icon')['sizes']['thumbnail'] ); ?>" loading="lazy" alt="" class="icon" />
						<?php } ?>

						<?php if (get_sub_field('title')) { ?>
							<h3 style="font-size: 1.15rem;"><?php the_sub_field('title'); ?></h3>
						<?php } ?>

						<?php if (get_sub_field('content')) { ?>
							<div><?php the_sub_field('content'); ?></div>
						<?php } ?>

					</div>
				</div>

			<?php endwhile; ?>

		</div>
	</div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
