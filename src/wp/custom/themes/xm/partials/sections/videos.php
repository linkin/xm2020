<?php

$sectionClass = "section";
$sectionId = "";
$sectionId = get_sub_field('anchorlink_id');

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<div class="container">
	<?php if (get_sub_field('title')) : ?>
		<div class="section-header text-left">
			<div class="row">
				<div class="col-lg-6">
					<?php if (get_sub_field('small_title')) : ?><p class="top-title"><?php echo get_sub_field('small_title'); ?></p><?php endif; ?>
					<h3><?php echo get_sub_field('title'); ?></h3>
				</div>
			</div>
	  </div>
	<?php endif; ?>

	<div class="video-list">
		<div class="row">

				<?php
				$i = 0;
				$args = array( 'post_type' => 'video', 'posts_per_page' => 10 );
				$the_query = new WP_Query( $args );
				?>
				<?php if ( $the_query->have_posts() ) : ?>
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


					<?php

					$video = get_field('video');
					preg_match('/src="(.+?)"/', $video, $matches);

					$src     = $matches[1];
					$params  = array('rel' => 0);
					$new_src = add_query_arg($params, $src);
					$video   = str_replace($src, $new_src, $video);

					preg_match('/src="(.+?)"/', $video, $matches_url );
					$src = $matches_url[1];

					preg_match('/embed(.*?)?feature/', $src, $matches_id );
					$id = $matches_id[1];
					$id = str_replace( str_split( '?/' ), '', $id );

					$thumbnail = "http://img.youtube.com/vi/".$id."/hqdefault.jpg";

					$i ++;
					?>

					<div class="col-lg-6 mb-4">
						<div class="card article">
							<a href="#">
							  <div class="card-img card-img-bg plate small"><div class="image" style='background-image: url("<?php echo $thumbnail ?>")'></div></div>
							  <div class="card-img-overlay centered">
							    <h4 class="card-title"><?php echo get_the_title(); ?></h4>
								  <?php if (get_field('video_text')) : ?><p class="excerpt"><?php echo get_field('video_text'); ?></p><?php endif; ?>
									<button class="btn btn-outline-white mt-3" data-toggle="modal" data-target="#modal-video-<?php echo $i; ?>">
											<i class="fas fa-play"></i> <span>Play video</span>
									</button>
							  </div>
							</a>
						</div>
					</div>


					<div class="modal modal-video" id="modal-video-<?php echo $i; ?>" tabindex="-1" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered">
									<div class="modal-content">
											<div class="video">
												<?php echo get_field('video'); ?>
											</div>
											<div class="text-center mt-5">
													<button type="button" class="btn btn-outline-white" data-dismiss="modal"><i class="fal fa-times"></i> Close </button>
											</div>
									</div>
							</div>
					</div>

				<?php wp_reset_postdata(); ?>
				<?php $i ++; endwhile; ?>
				<?php endif; ?>


			</div>
	  </div>
	</div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
