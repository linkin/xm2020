<?php

$sectionClass = "section ";
$sectionId = "";
$sectionId = get_sub_field('anchorlink_id');
$top_margin = get_sub_field_object('top_margin')['value'];
$bottom_margin = get_sub_field_object('bottom_margin')['value'];
$sectionBackground = get_sub_field_object('background')['value'];

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<?php if (get_sub_field('title')) : ?>
	  <div class="section-header">
	    <div class="container container-s">
	      <h2><?php echo get_sub_field('title'); ?></h2>
	    </div>
	  </div>
	<?php endif; ?>

  <div class="container container-xl relative">

		<?php
		$post_objects = get_sub_field('page_links');
		$countPages = count(get_sub_field('page_links'));
		$rowClass = "row row-small";
		if($countPages <= 3){
			$rowClass = "row row-small justify-content-center";
		}

		?>

		<div class="<?php echo $rowClass; ?>">

			<?php

			if( $post_objects ): ?>
				<?php foreach( $post_objects as $post_object): ?>

					<div class="col-md-12 col-md-6 col-lg-4 mb-4">
						<?php
							$background  = "image";
							$title       = get_the_title($post_object->ID);
							$link        = get_permalink($post_object->ID);
							$image       = get_field('page_image', $post_object->ID);
							$catchphrase = get_field('catchphrase', $post_object->ID);

							require get_template_directory() . '/partials/component/card.php';

						?>
					</div>

				<?php endforeach; ?>
			<?php endif; ?>

		</div>

		<div class="graphic-container pos_absolute_100">
			<div class="graphics">
				<div id="square_type_2" class="square blue small blue pos2"></div>
				<div id="square_type_3" class="square small green pos2"></div>
				<div id="square_type_4" class="square small blue pos2"></div>
				<div id="dots_type_1" class="dotts"></div>
			</div>
		</div>

  </div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
