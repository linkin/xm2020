<?php

$sectionClass = "section";
$sectionId = '';
$sectionId = get_sub_field('anchorlink_id');
$top_margin = get_sub_field_object('top_margin')['value'];
$bottom_margin = get_sub_field_object('bottom_margin')['value'];
$sectionBackground = get_sub_field_object('background')['value'];

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<?php if (get_sub_field('title')) : ?>
	  <div class="section-header">
	    <div class="container container-s">
	      <h2><?php echo get_sub_field('title'); ?></h2>
	    </div>
	  </div>
	<?php endif; ?>

  <div class="container container-xl">
		<div class="row">

      <div class="col-lg-6 mb-4">

				<?php
				$post_object = get_sub_field('content_grid_featured');

				if( $post_object ): ?>

							<?php

								$post = $post_object;
								setup_postdata( $post );

								$title      = '';
								$background = get_field('thumbnail_background', $post_object->ID);
								$category   = '';
								$link       = get_permalink();
								$image      = '';

								if ( get_field('thumbnail_background', $post_object->ID) === 'image'){
									$image     = get_field('thumbnail_image', $post_object->ID);
									$cardClass = "card white";
								}

								if (get_field('quote', $post_object->ID)) {
									$catchphrase = get_field('quote', $post_object->ID);
								}else{
									$title = get_the_title($post_object->ID);
								}

						    $postType = get_post_type_object(get_post_type($post_object->ID));
						    if ($postType) {
						        $postType = esc_html($postType->labels->singular_name);
						    }
						    if ($postType == "Story") {
						        $category = "Customer Story";
										$customer = get_field('customer', $post_object->ID);
										$customer_logo = get_field('black_logotype', $customer->ID)['sizes']['medium'];
										if(get_field('thumbnail_background', $post_object->ID) == "image"){
											$customer_logo = get_field('white_logotype', $customer->ID)['sizes']['medium'];
										}
						    }else if ($postType == "Article") {
						        $category = "Article";
						    }

								require get_template_directory() . '/partials/component/card.php';

								wp_reset_postdata();
							?>

				<?php endif; ?>

			</div>

      <div class="col-lg-6">
	      <div class="row">

					<?php
					$post_objects = get_sub_field('content_grid_articles');

					if( $post_objects ): ?>
							<?php foreach( $post_objects as $post_object): ?>


								<?php

									$title = get_the_title($post_object->ID);
									$background = get_field('thumbnail_background', $post_object->ID);
									$customer = '';
									$customer_logo = '';
									$catchphrase = '';
									$category = '';
									$link = get_permalink($post_object->ID);
									$image = '';


									if(get_field('customer', $post_object->ID)){
										$customer = get_field('customer', $post_object->ID);
										$customer_logo = get_field('black_logotype', $customer->ID)['sizes']['medium'];
									}


									if(get_field('thumbnail_background', $post_object->ID) == "image"){
										$image = get_field('thumbnail_image', $post_object->ID);
										$cardClass = "card white";
										if(get_field('customer', $post_object->ID)){
											$customer_logo = get_field('white_logotype', $customer->ID)['sizes']['medium'];
										}
									}

									if(get_field('customer', $post_object->ID)){
										$catchphrase = get_field('quote', $post_object->ID);
									}

							    $postType = get_post_type_object(get_post_type($post_object->ID));

							    if ($postType) {
							        $postType = esc_html($postType->labels->singular_name);
							    }

							    if ($postType == "Story") {
							        $category = "Customer Story";
							    }else if ($postType == "Article") {
							        $category = "Article";
							    }else if ($postType == "Whitepaper") {
							        $category = "Whitepaper";
							    }

									echo "<div class='col-12 col-lg-6 mb-4'>";
									require get_template_directory() . '/partials/component/card.php';
									echo "</div>";
								?>


							<?php endforeach; ?>
					<?php endif; ?>

				</div>
			</div>


    </div>
  </div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
