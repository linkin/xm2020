<div class="">

	<div class="row row-lg align-items-center pb-5 cookie-opt">
			<div class="col-12">
					<h3><?php the_sub_field('title'); ?></h3>
					<div class="cookie-opt-in d-none">
							<p><?php the_sub_field('opt_in_text'); ?></p>
							<button class="btn btn-primary cookie-opt-in-button"><?php the_sub_field('opt_in_button'); ?></button>
					</div>
					<div class="cookie-opt-out d-none">
							<p><?php the_sub_field('opt_out_text'); ?></p>
							<button class="btn btn-primary cookie-opt-out-button"><?php the_sub_field('opt_out_button'); ?></button>
					</div>
			</div>
	</div>

</div>
