<?php $releases = cision_newsfeed(); ?>
<?php if (count($releases) > 0) : ?>
<?php $reportFileNames = "Årsredovisning|Årsredovisning|Delårsrapport|Delårsrapport|Kvartalsrapport|Bokslutskommuniké|Bokslutskommunike|Annual|Interim"; ?>

				<div class="block article lists">
						<div class="row row-lg ">
								<div class="col-xs-12">
										<h3><?php the_sub_field('title'); ?></h3>
										<ul class="list list-files">
				<?php foreach ($releases as $release) : ?>
					<?php if (count($release->Files) > 0) : ?>
						<?php foreach ($release->Files as $file) : ?>
							<?php if (preg_match('/(' . $reportFileNames . ')/ui', $file->Title) === 1) : ?>
																				<li>
																						<a href="<?php echo $file->Url ?>"
																							 target="_blank"><?php echo $file->Title; ?></a>
																				</li>
							<?php endif; ?>
						<?php endforeach; ?>
					<?php endif; ?>
				<?php endforeach; ?>
										</ul>
								</div>
						</div>
				</div>

<?php endif; ?>
