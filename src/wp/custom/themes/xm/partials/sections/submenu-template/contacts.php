<div class="board-group">
		<div class="row">

		<?php
		$i = 0;
		while (have_rows('contacts')) : the_row(); ?>

				<div class="col-lg-4">
						<div class="board-group-item">
								<img src="<?php echo esc_url( get_sub_field('image')['sizes']['medium'] ); ?>" loading="lazy" alt="<?php echo esc_html( get_sub_field('image')['alt'] ); ?>" />
								<h4 class="mt-0 mb-3"><?php the_sub_field('name'); ?></h4>
								<hr class="slim left small mb-3"/>
								<p class="mb-2"><?php the_sub_field('description'); ?></p>
								<?php if (get_sub_field('full_description')) : ?>
										<button data-toggle="modal" data-target="#boardModal-<?php echo $sectionIndex . '-' . $i; ?>">
											<i class="fal fa-plus-circle mr-1"></i> Läs mer
										</button>
								<?php endif; ?>
						</div>
				</div>

				<div class="modal fade-scale" id="boardModal-<?php echo $sectionIndex . '-' . $i; ?>" tabindex="-1" aria-hidden="true">
						<div class="modal-dialog modal-lg">
								<div class="modal-content">
										<div class="modal-top">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<i class="fal fa-times"></i>
											</button>

											<?php
											echo wp_get_attachment_image(
												get_sub_field('image')['ID'],
												'large',
												'',
												array(
													'class'  => 'img-fluid image',
													'srcset' => get_sub_field('image')['sizes']['large'] . ' 1024w,' . get_sub_field('image')['sizes']['medium_large'] . ' 768w,' . get_sub_field('image')['sizes']['medium'] . ' 300w',
													'sizes'  => '(min-width: 991px) 1024px, 90vw'
												)
											);
											?>

										</div>
										<div class="modal-body">
												<h1><?php the_sub_field('name'); ?></h1>
												<?php the_sub_field('full_description'); ?>
										</div>
										<div class="modal-footer justify-content-center">
												<button type="button" class="btn btn-link" data-dismiss="modal"><i class="fal fa-times mr-1"></i> Stäng </button>
										</div>
								</div>
						</div>
				</div>

		<?php $i ++; endwhile; ?>
		</div>
</div>
