<div>

		<?php
		$i = 0;

		while (have_rows('item')) : the_row(); ?>

				<div class="">

					<?php if (get_sub_field('title_or_accordion') == "title"){ ?>

					<?php

						$cssHeadline = "";
						if(get_sub_field('graphic_background')){

							$cssHeadline = "graphic_headline_background";

						}

					?>

					<div class="<?php echo $cssHeadline; ?>">

						<?php if(get_sub_field('bigger_headline')){ ?>
							<h2 class="mt-4"><?php the_sub_field('title'); ?></h2>
						<?php }else{ ?>
							<h4 class="mt-4"><?php the_sub_field('title'); ?></h4>
						<?php } ?>


						<?php if(get_sub_field('accordion_lead')){ ?>
							<div><?php the_sub_field('accordion_lead'); ?></div>
						<?php } ?>

					</div>

					<?php }else if (get_sub_field('title_or_accordion') == "id"){ ?>

						<div id="<?php the_sub_field('id'); ?>"></div>

					<?php }else if (get_sub_field('title_or_accordion') == "accordion"){ ?>

							<div class="accordion-group">
										<?php $j = 0; ?>
									 <?php while (have_rows('accordion')) : the_row(); $i++; ?>
										 <div class="accordion">
											 <button class="accordion-title" data-toggle="collapse" data-target="#collapse-<?php echo $j . "-" . $i ?>" aria-expanded="false">
												 <h3><?php the_sub_field('title'); ?></h3>
												</button>
											 <div id="collapse-<?php echo $j . "-" . $i ?>" class="collapse">
													 <div class="accordion-content">
															 <div class="rte-content">
																	 <?php the_sub_field('content'); ?>
															 </div>
														</div>
											 </div>
										 </div>
									 <?php $j++; endwhile; ?>
							</div>


					<?php } ?>

				</div>

		<?php $i ++; endwhile; ?>
</div>
