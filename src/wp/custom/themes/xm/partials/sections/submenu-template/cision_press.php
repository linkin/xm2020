<?php
	$releases = cision_newsfeed();

	if (count($releases) > 0) : ?>

							<div>
									<div>
											<?php if (get_sub_field('title')) : ?>
													<div class="col-8" style="padding-bottom: 0;">
															<h2><?php the_sub_field('title'); ?></h2>
													</div>
											<?php endif; ?>
											<div>
													<script>
															jQuery(function ($) {

																	function getParameterByName(name, url) {
																			if (!url) {
																					url = window.location.href;
																			}
																			name = name.replace(/[\[\]]/g, "\\$&");
																			var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
																					results = regex.exec(url);
																			if (!results) return null;
																			if (!results[2]) return '';
																			return decodeURIComponent(results[2].replace(/\+/g, " "));
																	}

																	jQuery(document).ready(function ($) {
																			if (getParameterByName('subscription')) {
																					$('html, body').animate({
																							scrollTop: $("#subscription").offset().top
																					}, 500);
																					$('#subscribeExpand').addClass("show");
																			}
																	})


																	$("#postFilter").change(function () {

																			$("#pressExpand").contents().unwrap();
																			// $("#pressExpand").addClass("in");
																			$(".expand-hide").hide();

																			var posttype = $(this).val();

																			if (posttype) {
																					$("#releases .post").each(function () {
																							if (!$(this).hasClass(posttype)) {
																									$(this).addClass("d-none");
																							}
																							else {
																									$(this).removeClass("d-none");
																							}
																					})
																			} else {
																					$("#releases .post").removeClass("d-none");
																			}
																	});

																	$("#postFilter a").click(function (e) {
																			e.preventDefault();

																			$("#pressExpand").contents().unwrap();
																			// $("#pressExpand").addClass("in");
																			$(".expand-hide").hide();

																			$("#postFilter a").removeClass("active");
																			$(this).addClass("active");

																			var posttype = $(this).attr("data-value");

																			if (posttype) {
																					$("#releases .post").each(function () {
																							if (!$(this).hasClass(posttype)) {
																									$(this).addClass("d-none");
																							}
																							else {
																									$(this).removeClass("d-none");
																							}
																					})
																			} else {
																					$("#releases .post").removeClass("d-none");
																			}
																	});

																	$('.press-collapsible').click(function () {
																			$(this).hide();
																	});

															});
													</script>

													<!-- <select id="postFilter" style="margin-top: 10px; width: 100%;">
														<option value="">Alla</option>
														<option value="regulatory">Börsmeddelanden</option>
														<option value="press">Pressmeddelanden</option>
													</select> -->

													<ul id="postFilter" class="nav nav-pills button-style left-aligned mb-4">
															<li class="nav-item"><a href="" data-value="" class="nav-link active">Alla</a></li>
															<li class="nav-item"><a href="" data-value="regulatory" class="nav-link">Regulatoriska</a>
															</li>
															<li class="nav-item"><a href="" data-value="press"
																											class="nav-link">Nyheter</a></li>
													</ul>
											</div>
									</div>

									<div id="releases" class="">

				<?php $i = 0; ?>
				<?php foreach ($releases as $release) : $i ++; ?>

					<?php if ($i == 4) : ?>
															<div id="pressExpand" class="collapse">
					<?php endif; ?>

													<div class="post <?php if ($release->IsRegulatory) : ?>regulatory<?php else : ?>press<?php endif; ?>"

															 data-type="<?php if ($release->IsRegulatory) : ?>regulatory<?php else : ?>press<?php endif; ?>">

						<?php
						$pageUrl        = "";
						$fileUrl        = "";
						$pageUrl        = $release->CisionWireUrl;
						$read_more_text = "Läs mer";

						$reportFileNames = "Årsredovisning|Delårsrapport|Kvartalsrapport|Bokslutskommuniké|Release|Annual|Interim";

						if (count($release->Files) > 0) {
							foreach ($release->Files as $file) {
								if (preg_match('(' . $reportFileNames . ')', $file->Title) === 1) {
									$fileUrl        = $file->Url;
									$read_more_text = "Ladda ner";
								}
							}
						}

						?>
															<div class="row">
																	<div class="col-12">
																			<p class="small text-muted mb-3"><?php echo date_i18n('j F, Y', strtotime($release->PublishDate)); ?></p>
																			<h3 class="mt-0">
									<?php if ($pageUrl) : ?><a href="<?php echo $pageUrl; ?>"
																																		 target="_blank"><?php echo $release->Title; ?></a><?php else : ?><?php echo $release->Title; ?><?php endif; ?>
																			</h3>
																			<p>
									<?php echo $release->Intro; ?>
																			</p>
								<?php if ($fileUrl) : ?><strong><a href="<?php echo $fileUrl; ?>" target="_blank"><?php echo $read_more_text; ?></a></strong><?php endif; ?>

																	</div>
															</div>

													</div>
					<?php if ($i > 3 && $i == count($releases)) : ?>
															</div>
					<?php endif; ?>
				<?php endforeach; ?>
									</div>
									<div class="text-center">
											<a href="#pressExpand" data-toggle="collapse"
												 class="btn btn-primary collapse-hide collapsed press-collapsible">Visa fler</a>
									</div>
							</div>


							<div id="subscription" class="bg-light mt-5 p-5 text-center">
									<h2 class="mt-0 mb-2"><?php the_sub_field('subscribe_title'); ?></h2>
									<p><?php the_sub_field('subscribe_text'); ?></p>
									<a href="#subscribeExpand" data-toggle="collapse"
										 class="btn btn-outline-secondary"><?php the_sub_field('subscribe_button_text'); ?></a>
							</div>
							<div id="subscribeExpand" class="collapse">
									<div class="bg-light border-top p-5">


											<div class="">
					<?php

					if (isset($_GET['subscription'])) :
						$status = trim($_GET['subscription']);
						if ($status === 'success') : ?>
																	<div class="alert alert-success">
																			Tack för att du prenumererar!
																	</div>
						<?php else : ?>
																	<div class="alert alert-warning">
																			Tyvärr så gick någonting fel vid prenumeration, kontrollera uppgifter eller
																			försök igen senare.
																	</div>
						<?php endif;

					endif;

					?>
													<form method="post" action=" http://publish.ne.cision.com/Subscription/Subscribe"
																name="pageForm">
															<input type="hidden" name="subscriptionUniqueIdentifier" value="ec74501fa8"/>
															<input type="hidden" name="redirectUrlSubscriptionSuccess"
																		 value="<?php echo get_permalink(get_the_ID()); ?>?subscription=success"/>
															<input type="hidden" name="redirectUrlSubscriptionFailed"
																		 value="<?php echo get_permalink(get_the_ID()); ?>?subscription=fail"/>
															<input type="hidden" name="Replylanguage" value="sv"/>
															<div class="row">
																	<div class="col-sm-6">
																			<p><strong>Language:</strong></p>
																			<p>
																					<input type="checkbox" name="Language" value="sv" id="lang_sv">
																					<label for="lang_sv">Swedish</label>
																					<br/>
																					<input type="checkbox" name="Language" value="en" id="lang_en">
																					<label for="lang_en">English</label>
																			</p>
																	</div>
																	<div class="col-sm-6">
																			<p><strong>Information type:</strong></p>
																			<p>
																					<input type="checkbox" name="informationtype" value="kmk,rpt"
																								 id="interim"/>
																					<label for="interim">Interim reports</label>
																					<br/>
																					<input type="checkbox" name="informationtype" value="rdv" id="annual"/>
																					<label for="annual">Annual reports</label>
																					<br/>
																					<input type="checkbox" name="informationtype" value="prm" id="press"/>
																					<label for="press">Press releases</label>
																			</p>
																	</div>
															</div>
															<div class="row">
																	<div class="col-sm-6">
																			<label for="name"><strong>Name:</strong></label>
																			<input type="text" name="Name" id="name" class="form-control"/>
																	</div>
																	<div class="col-sm-6">
																			<label for="companyname"><strong>CompanyName:</strong></label>
																			<input type="text" name="CompanyName" id="companyname"
																						 class="form-control"/>
																	</div>
															</div>
															<div class="row">
																	<div class="col-sm-6">
																			<label for="email"><strong>Email:</strong></label>
																			<input type="text" name="Email" id="email" class="form-control"/>
																	</div>
																	<div class="col-sm-6">
																			<label for="SMS"><strong>SMS:</strong> (+46709123456)</label>
																			<input type="text" name="Cellphone" id="SMS" class="form-control"/>
																	</div>
															</div>
															<div class="text-center pt-5">
																	<input type="submit" value="<?php the_sub_field('subscribe_button_text'); ?>"
																				 class="btn btn-secondary"/>
															</div>
													</form>
											</div>
									</div>
							</div>

	<?php endif; ?>
