<?php

$sectionClass = "section";
$sectionId = "";
$sectionId = get_sub_field('anchorlink_id');
$top_margin = get_sub_field_object('top_margin')['value'];
$bottom_margin = get_sub_field_object('bottom_margin')['value'];
$sectionBackground = get_sub_field_object('background')['value'];

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<?php if (get_sub_field('title')) : ?>
		<div class="section-header">
			<div class="container container-s">
				<?php if (get_sub_field('small_title')) : ?>
					<p class="top-title"><?php echo get_sub_field('small_title'); ?></p>
				<?php endif; ?>

				<?php if (get_sub_field('title')) : ?>
					<h2><?php echo get_sub_field('title'); ?></h2>
				<?php endif; ?>

				<?php if (get_sub_field('lead')) : ?>
				 <div class="lead"><p><?php echo get_sub_field('lead'); ?></p></div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

	<div class="container">

			<?php

			$columns = get_sub_field('column_count');

			$colClass  = "col-12 mb-3 col-md-6";
			$rowClass  = "row";

			if ($columns == "Two") {
				$colClass    = "col-12 mb-3 col-md-6";
				$large_break = '50vw';
			}

			if ($columns == "Three") {
				$colClass    = "col-12 mb-3 col-md-4";
				$large_break = '33vw';
			}

			if ($columns == "Four") {
				$colClass    = "col-12 mb-3 col-md-3";
				$large_break = '25vw';
			}

			?>

			<div class="<?php echo $rowClass ?>">

			<?php while (have_rows('columns')): the_row(); ?>

				<?php
				$image    = "";
				if(get_sub_field('image')){
					$image    = get_sub_field('image')['sizes']['large'];
				}
				$cssClass = "";

				?>

				<div class="<?php echo $colClass; ?>">
					<div class="<?php echo $cssClass; ?>">

						<?php if ($image) {

							echo wp_get_attachment_image(
								get_sub_field('image')['ID'],
								'large',
								'',
								array(
									'class'  => 'img-fluid image mb-3',
									'srcset' => get_sub_field('image')['sizes']['large'] . ' 1024w,' . get_sub_field('image')['sizes']['medium_large'] . ' 768w,' . get_sub_field('image')['sizes']['medium'] . ' 300w',
									'sizes'  => '(min-width: 991px) ' . $large_break . ', 90vw'
								)
							);

						} ?>

						<?php if (get_sub_field('title')) { ?>
							<h4><?php the_sub_field('title'); ?></h4>
						<?php } ?>

						<?php if (get_sub_field('content')) { ?>
							<div><?php the_sub_field('content'); ?></div>
						<?php } ?>

					</div>
				</div>

			<?php endwhile; ?>

		</div>
	</div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
