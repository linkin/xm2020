<?php

$sectionClass = "section";
$sectionId = '';
$sectionId = get_sub_field('anchorlink_id');
$top_margin = get_sub_field_object('top_margin')['value'];
$bottom_margin = get_sub_field_object('bottom_margin')['value'];
$sectionBackground = get_sub_field_object('background')['value'];

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<div class="container">
	<?php if (get_sub_field('title')) : ?>
		<div class="section-header text-left">
			<div class="row align-items-end">
				<div class="col-lg-6">
					<?php if (get_sub_field('small_title')) : ?><p class="top-title"><?php echo get_sub_field('small_title'); ?></p><?php endif; ?>
					<h3><?php echo get_sub_field('title'); ?></h3>
				</div>
				<div class="col-lg-6 text-lg-right d-none d-lg-block">
					<a href="<?php echo esc_url( get_permalink(717) ); ?>" class="arrow">View all articles</a>
				</div>
			</div>
		</div>
	<?php endif; ?>


		<div class="content-list">


				<div class="slick-slider article-slider">
				<?php
				$args = array( 'post_type' => 'article', 'posts_per_page' => 10 );
				$the_query = new WP_Query( $args );
				?>
				<?php if ( $the_query->have_posts() ) : ?>
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

					<?php

					$link       = get_permalink();
					$category   = '';
					$gray_label = '';
					$title      = get_the_title();
					$excerpt    = excerpt(13);
        			$imagesize  = '';
					$image      = '';

					if ( get_field('thumbnail_background') === 'image' && get_field('thumbnail_image') ) {
						$image = get_field('thumbnail_image');
					}

					$terms = get_the_terms(get_the_ID(), 'article_category');

					if ( $terms && ! is_wp_error( $terms ) ){
						$categories = array();

						foreach ( $terms as $term ) {
							$categories[] = $term->name;
						}

						$category = join( ", ", $categories );
					}

					require get_template_directory() . '/partials/component/article-card.php';
					?>

				<?php wp_reset_postdata(); ?>
				<?php endwhile; ?>
				<?php endif; ?>
				</div>



  </div>
	<div class="d-block d-lg-none mt-4 text-center">
		<a href="<?php echo esc_url( get_permalink(717) ); ?>" class="arrow">View all articles</a>
	</div>
</div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
