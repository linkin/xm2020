<?php

$sectionClass = "section";
$sectionId = "";
$sectionId = get_sub_field('anchorlink_id');
$top_margin = get_sub_field_object('top_margin')['value'];
$bottom_margin = get_sub_field_object('bottom_margin')['value'];
$sectionBackground = get_sub_field_object('background')['value'];

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<div class="container">
		<?php

		$quotes_count = count(get_sub_field('quotes'));

		$sliderClass  = "slider text-center";
		$quoteClass  = "quote";

		if ($quotes_count == "1") {
			$sliderClass = "pl-5 pr-5 text-center";
		}
		if (get_sub_field('small_text')) {
			$quoteClass = "quote small";
		}

		?>

		<div class="<?php echo $sliderClass; ?>">

			<?php while (have_rows('quotes')) : the_row(); ?>
				<div>

					<p><?php the_sub_field('title'); ?></p>
					<div class="<?php echo $quoteClass; ?>"><?php the_sub_field('quote'); ?></div>

					<?php if (get_sub_field('link')) : ?>
						<p class="mt-4">

							<a href="<?php echo get_sub_field('link')['url']; ?>">
								<?php echo get_sub_field('link')['title']; ?>
							</a>

						</p>
					<?php endif; ?>


				</div>
			<?php endwhile; ?>

		</div>
	</div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
