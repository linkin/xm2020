<?php

$sectionClass = "section";
$sectionId = "";
$iconSize  = "";
$sectionId = get_sub_field('anchorlink_id');
$top_margin = get_sub_field_object('top_margin')['value'];
$bottom_margin = get_sub_field_object('bottom_margin')['value'];
$sectionBackground = get_sub_field_object('background')['value'];
$layout = get_sub_field_object('layout')['value'];
$iconSize = get_sub_field_object('icon_size')['value'];

if ($top_margin != "regular") {
	$sectionClass = $sectionClass . " " . $top_margin;
}

if ($bottom_margin != "regular") {
	$sectionClass = $sectionClass . " " . $bottom_margin;
}
if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}

?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<?php if (get_sub_field('title')) : ?>
	  <div class="section-header mb-5">
	    <div class="container container-s">
	      <h2><?php echo get_sub_field('title'); ?></h2>
	    </div>
	  </div>
	<?php endif; ?>

	<div class="container container-md">

			<?php


			$colClass  = "col-12 mb-3 col-md-6 mb-md-4";
			$innerColClass  = "col-3 text-right";
			$innerColClass2  = "col-9";
			$rowClass  = "row row-lg";

			if ($layout == "image_above_four_columns") {
				$colClass  = "col-12 mb-3 col-md-3 mb-md-4";
				$innerColClass  = "col-3 col-md-12 text-md-center";
				$innerColClass2  = "col-9 col-md-12 text-md-center";
			}

			?>

			<div class="<?php echo $rowClass ?>">

			<?php while (have_rows('items')): the_row(); ?>

				<?php
				$image    = get_sub_field('icon')['sizes']['large'];
				$cssClass = "text-left";

				?>

				<div class="<?php echo $colClass; ?>">
					<div class="<?php echo $cssClass; ?>">

						<div class="row">

						<?php if ($image) { ?>
							<div class="<?php echo $innerColClass; ?>"><img src="<?php echo get_sub_field('icon')['sizes']['thumbnail']; ?>" loading="lazy" class="icon <?php echo $iconSize; ?>"/></div>
						<?php } ?>

							<div class="<?php echo $innerColClass2; ?>">
						<?php if (get_sub_field('title')) { ?>
							<h4><?php the_sub_field('title'); ?></h4>
						<?php } ?>

						<?php if (get_sub_field('content')) { ?>
							<div><?php the_sub_field('content'); ?></div>
						<?php } ?>
							</div>
						</div>

					</div>
				</div>

			<?php endwhile; ?>

		</div>
	</div>
</div>

<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
