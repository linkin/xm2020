<?php

$sectionClass = "section ";
$sectionId = "";
$sectionId = get_sub_field('anchorlink_id');

$sectionBackground = get_sub_field_object('background')['value'];

if ($sectionBackground != "transparent") {
	$sectionClass = $sectionClass . " " . $sectionBackground;
}
?>

<div class="<?php echo $sectionClass; ?>" id="<?php echo $sectionId; ?>">

	<?php if (get_sub_field('title')) : ?>
	  <div class="section-header">
	    <div class="container text-left">
				<?php if (get_sub_field('small_title')) : ?>
					<p class="top-title"><?php echo get_sub_field('small_title'); ?></p>
				<?php endif; ?>

				<?php if (get_sub_field('title')) : ?>
					<h3><?php echo get_sub_field('title'); ?></h3>
				<?php endif; ?>

				<?php if (get_sub_field('lead')) : ?>
				 <div class="lead"><p><?php echo get_sub_field('lead'); ?></p></div>
				<?php endif; ?>
	    </div>
	  </div>
	<?php endif; ?>

	<?php include get_template_directory() . '/partials/component/price_boxes.php'; ?>


</div>


<?php if (get_sub_field('divider_under_block')) : ?>
	<div class="container"><hr/></div>
<?php endif; ?>
