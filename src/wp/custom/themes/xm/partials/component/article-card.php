
<?php
	if(!isset($target)){

		$target = "";

	}else{
		$target = 'target="'.$target.'"';
	}
?>


<div class="content-list">

	<div class="card article">


		<a href="<?php echo $link; ?>" <?php echo $target; ?> <?php if ($open_in_popup) { echo 'data-toggle="modal" data-target="#feedModal-'.get_the_ID().'"'; }?>>

			<?php if ( is_array( $image ) ) : ?>
				<div class="card-img <?php echo $imagesize; ?>">

					<?php
					echo wp_get_attachment_image(
						$image['ID'],
						'medium_large',
						'',
						array(
							'class'  => 'img-fluid image' . $img_css,
							'srcset' => $image['sizes']['large'] . ' 1024w,' . $image['sizes']['medium_large'] . ' 768w,' . $image['sizes']['medium'] . ' 300w',
							'sizes'  => '(min-width: 991px) 33vw, 90vw'
						)
					);
					?>

				</div>

			<?php else: ?>

				<div class="card-img <?php echo $imagesize; ?>" style="background: url(<?php echo esc_url( $image ) ?>); background-size:cover;"></div>

			<?php endif; ?>



			<div class="card-body">
				<?php if(isset($customer_logo) && $customer_logo != ""){ ?>
					<div class="customer-logo logo-circle" style='background-image: url(<?php echo $customer_logo; ?>)'></div>
				<?php } ?>
				<p class="label">
					<span class="category">
							<?php echo $category ?>
					</span>
					<?php if(isset($gray_label) && $gray_label != ""){ ?><span class="gray dot-divider"><?php echo $gray_label; ?></span></p><?php } ?>

				<h4 class="card-title"><?php echo $title; ?></h4>
				<p class="excerpt"><?php echo $excerpt; ?></p>
			</div>

            <div class="popup_content">

            </div>

		</a>
	</div>
</div>
