
<div class="modal fade-scale" id="newsletter-modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-medium modal-dialog-centered text-center modal-bg-header">
        <div class="modal-content">
            <div class="modal-top modal-top-graphic">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times"></i>
                </button>
                <h1><?php the_field('newsletter_modal_headline','options'); ?></h1>

                <div class="graphic-container">
          				<div class="graphics">
          					<div id="parallax5" class="square yellow medium green pos1"></div>
          					<div id="parallax6" class="square pink small blue pos2"></div>
          					<div id="parallax7" class="square small blue pos2"></div>
          					<div id="parallax8" class="square small lightblue pos2"></div>
                    <div class="dotts dottsarticle"></div>
          				</div>
          			</div>
            </div>
            <div class="modal-body">
              <?php the_field('newsletter_modal_content','options'); ?>
            </div>
        </div>
    </div>
</div>
