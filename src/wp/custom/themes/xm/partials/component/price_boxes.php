
<div class="container">
  <div class="row justify-content-center">


      <?php
        $link = get_field('express_link');
      ?>
      <?php if (get_field('express_show_on_website')) { ?>


      <?php
        $priceboxcss = "";
        if (get_field('express_stand_out')){
          $priceboxcss = "inverted ";
        }
      ?>

      <div class="col-12 col-md-4">
        <div class="card pricebox mb-3 mb-md-0 <?php echo $priceboxcss ?>">
          <div class="card-body">

            <?php  if (get_field('express_banner')){ ?>

              <div class="circle">
                <div class="circle_inner">
                  <div class="circle_wrapper">
                    <div class="circle_content"><?php the_field('express_banner_text'); ?></div>
                  </div>
                </div>
              </div>

            <?php } ?>

            <?php  if (get_field('express_label') && get_field('express_label_text')){ ?>

              <div class="label">
                <?php the_field('express_label_text'); ?>
              </div>

            <?php } ?>

            <?php  if (get_field('express_title_1')){ ?> <h3><?php the_field('express_title_1'); ?></h3><?php } ?>
            <p class="price"><?php the_field('express_title_2'); ?></p>
            <p class="price_unit"><?php the_field('express_title_3'); ?></p>

            <hr/>

            <div class="list">
              <ul>
              <?php while (have_rows('express_features')) : the_row(); ?>
                <li><?php the_sub_field('label'); ?></li>
              <?php endwhile; ?>
              </ul>
            </div>

            <hr/>

            <p class="textunder-list"><?php the_field('express_text_under_feature'); ?></p>

            <?php if ($link) :

              $link_url = $link['url'];
              $link_title = $link['title'];

              $buttonCSS = "btn btn-primary btn-lg mb-3";

              if (get_field('express_outlined')){
                $buttonCSS = "btn btn-outline-primary btn-lg mb-3";
              }
              ?>

              <a href="<?php echo esc_url( $link_url ); ?>" class="<?php echo $buttonCSS; ?>"><?php echo esc_html( $link_title ); ?></a>

            <?php endif; ?>

            <a href="#compbarison" class="scrollTo"><strong>See detailed information</strong></a>

          </div>
        </div>
      </div>

      <?php } ?>

      <?php if (get_field('business_show_on_website')) { ?>

      <?php
        $link = get_field('business_link');
      ?>

      <?php
        $priceboxcss = "";
        if (get_field('business_stand_out')){
          $priceboxcss = "inverted ";
        }
      ?>

      <div class="col-12 col-md-4">
        <div class="card pricebox mb-3 mb-md-0 <?php echo $priceboxcss ?>">
          <div class="card-body">

            <?php  if (get_field('business_banner')){ ?>

              <div class="circle">
                <div class="circle_inner">
                  <div class="circle_wrapper">
                    <div class="circle_content"><?php the_field('business_banner_text'); ?></div>
                  </div>
                </div>
              </div>

            <?php } ?>


            <?php  if (get_field('business_label') && get_field('business_label_text')){ ?>

              <div class="label">
                <?php the_field('business_label_text'); ?>
              </div>

            <?php } ?>

            <?php  if (get_field('business_title_1')){ ?> <h3><?php the_field('business_title_1'); ?></h3><?php } ?>
            <p class="price"><?php the_field('business_title_2'); ?></p>
            <p class="price_unit"><?php the_field('business_title_3'); ?></p>

            <hr/>

            <div class="list">
              <ul>
              <?php while (have_rows('business_features')) : the_row(); ?>
                <li><?php the_sub_field('label'); ?></li>
              <?php endwhile; ?>
              </ul>
            </div>

            <hr/>

            <p class="textunder-list"><?php the_field('business_text_under_feature'); ?></p>



            <?php if ($link) :

              $link_url = $link['url'];
              $link_title = $link['title'];

              $buttonCSS = "btn btn-primary btn-lg mb-3";

              if (get_field('business_outlined')){
                $buttonCSS = "btn btn-outline-primary btn-lg mb-3";
              }
              ?>

              <a href="<?php echo esc_url( $link_url ); ?>" class="<?php echo $buttonCSS; ?>"><?php echo esc_html( $link_title ); ?></a>

            <?php endif; ?>
            <a href="#compbarison" class="scrollTo"><strong>See detailed information</strong></a>
          </div>
        </div>
      </div>


      <?php } ?>


      <?php
        $link = get_field('enterprise_link');
      ?>


      <?php if (get_field('enterprise_show_on_website')) { ?>

        <?php
          $priceboxcss = "";
          if (get_field('enterprise_stand_out')){
            $priceboxcss = "inverted ";
          }
        ?>

        <div class="col-12 col-md-4">
          <div class="card pricebox mb-3 mb-md-0 <?php echo $priceboxcss ?>">
          <div class="card-body">

            <?php  if (get_field('enterprise_banner')){ ?>

              <div class="circle">
                <div class="circle_inner">
                  <div class="circle_wrapper">
                    <div class="circle_content"><?php the_field('enterprise_banner_text'); ?></div>
                  </div>
                </div>
              </div>

            <?php } ?>


            <?php  if (get_field('enterprise_label') && get_field('enterprise_label_text')){ ?>

              <div class="label">
                <?php the_field('enterprise_label_text'); ?>
              </div>

            <?php } ?>

            <?php  if (get_field('enterprise_title_1')){ ?> <h3><?php the_field('enterprise_title_1'); ?></h3><?php } ?>
            <p class="price"><?php the_field('enterprise_title_2'); ?></p>
            <p class="price_unit"><?php the_field('enterprise_title_3'); ?></p>

            <hr/>

            <div class="list">
              <ul>
              <?php while (have_rows('enterprise_features')) : the_row(); ?>
                <li><?php the_sub_field('label'); ?></li>
              <?php endwhile; ?>
              </ul>
            </div>

            <hr/>

            <p class="textunder-list"><?php the_field('enterprise_text_under_feature'); ?></p>

            <?php if ($link) :

              $link_url = $link['url'];
              $link_title = $link['title'];

              $buttonCSS = "btn btn-primary btn-lg mb-3";

              if (get_field('enterprise_outlined')){
                $buttonCSS = "btn btn-outline-primary btn-lg mb-3";
              }
              ?>

              <a href="<?php echo esc_url( $link_url ); ?>" class="<?php echo $buttonCSS; ?>"><?php echo esc_html( $link_title ); ?></a>

            <?php endif; ?>

            <a href="#compbarison" class="scrollTo"><strong>See detailed information</strong></a>

          </div>
        </div>
      </div>
      <?php } ?>

    </div>
  </div>
