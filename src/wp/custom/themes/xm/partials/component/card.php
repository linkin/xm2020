<?php

$cardClass = "card";

if($background != "image"){
  $cardClass = "card white";
}else{
  $cardClass = "card whiteText";
}


?>

<div class="<?php echo $cardClass; ?>">

  <a href="<?php echo $link; ?>">

  <?php if(isset($customer_logo) && $customer_logo != ""){ ?>
      <img src="<?php echo esc_url( $customer_logo ); ?>" class="customer-logo" alt="" loading="lazy" />
  <?php } ?>

  <?php if($background == "image"){ ?>

    <?php if ( is_array( $image ) ) : ?>
				<div class="card-img card-img-bg <?php echo $imagesize; ?>">

					<?php
					echo wp_get_attachment_image(
						$image['ID'],
						'medium_large',
						'',
						array(
							'class'  => 'img-fluid image' . $img_css,
							'srcset' => $image['sizes']['large'] . ' 1024w,' . $image['sizes']['medium_large'] . ' 768w,' . $image['sizes']['medium'] . ' 300w',
							'sizes'  => '(min-width: 991px) 33vw, 90vw'
						)
					);
					?>

				</div>

			<?php else: ?>

				<div class="card-img card-img-bg <?php echo $imagesize; ?>" style="background: url(<?php echo esc_url( $image ) ?>); background-size:cover;"></div>

			<?php endif; ?>

  <?php }else{ ?>

    <div class="card-img card-img-empty"><div class="white"><div class="hover"></div></div></div>

  <?php } ?>

  <div class="card-img-overlay">

    <?php if(isset($category) && $category != ""){ ?><p class="card-category"><?php echo $category; ?></p><?php } ?>

    <?php if(isset($title) && $title != ""){ ?><strong><?php echo $title; ?></strong><?php } ?>

    <?php if(isset($catchphrase) && $catchphrase != ""){ ?><p class="quote"><?php echo $catchphrase; ?></p><?php } ?>


  </div>

  </a>

</div>
