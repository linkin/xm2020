<?php


if (have_rows('second-sections')):
	$sectionIndex = 0;
	while (have_rows('second-sections')) : the_row();
		$sectionIndex += 1;
		switch (get_row_layout()) :

			case 'articles':
				require 'sections/articles.php';
			break;

			case 'whitepapers':
				require 'sections/whitepapers.php';
			break;

			case 'stories_slider':
				require 'sections/content-slider-stories.php';
			break;

			case 'logotypes':
				require 'sections/second-sections/logotypes.php';
			break;

			case 'price_list':
				require 'sections/price_list.php';
			break;

		endswitch;
	endwhile;
endif;
?>
