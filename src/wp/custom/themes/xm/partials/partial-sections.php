<?php


if (have_rows('sections')):
	$sectionIndex = 0;
	while (have_rows('sections')) : the_row();
		$sectionIndex += 1;
		switch (get_row_layout()) :

			case 'columns':
				require 'sections/columns.php';
			break;

			case 'columns_and_rows':
				require 'sections/columns_and_rows.php';
			break;

			case 'file_list':
				require 'sections/file_list.php';
			break;

			case 'icons_columns':
				require 'sections/icons_columns.php';
			break;

			case 'content_grid':
				require 'sections/content-grid.php';
			break;

			case 'content_slider':
				require 'sections/content-slider.php';
			break;

			case 'article_slider':
				require 'sections/content-slider-articles.php';
			break;

			case 'article_slider_rss':
				require 'sections/content-slider-articles-rss.php';
				break;

			case 'stories_slider':
				require 'sections/content-slider-stories.php';
			break;

			case 'logotypes':
				require 'sections/logotypes.php';
			break;

			case 'features':
				require 'sections/features.php';
			break;

			case 'price_list':
				require 'sections/price_list.php';
			break;

			case 'text_block':
				require 'sections/text_block.php';
			break;

			case 'boxes':
				require 'sections/boxes.php';
			break;

			case 'text_image_tabs':
				require 'sections/text-image-tabs.php';
			break;

			case 'text_image':
				require 'sections/text_image.php';
			break;

			case 'page_box_links':
				require 'sections/pagebox-links.php';
			break;

			case 'quotes':
				require 'sections/quotes.php';
			break;

			case 'whitepapers':
				require 'sections/whitepapers.php';
			break;

			case 'videos':
				require 'sections/videos.php';
			break;

		endswitch;
	endwhile;
endif;
?>
