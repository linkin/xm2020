<?php

if (get_field('type_of_cover') == "simple") :

	require 'cover/cover-simple.php';

elseif (get_field('type_of_cover') == "hero") :

	require 'cover/cover-hero.php';

elseif (get_field('type_of_cover') == "video") :

	require 'cover/cover-video.php';

elseif (get_field('type_of_cover') == "backgroundImage") :

	require 'cover/cover-backgroundImage.php';

elseif (get_field('type_of_cover') == "backgroundColor") :

	require 'cover/cover-backgroundColor.php';

elseif (get_field('type_of_cover') == "textAndImage") :

	require 'cover/cover-textAndImage.php';

endif;

?>
