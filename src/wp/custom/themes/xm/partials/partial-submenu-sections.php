<?php


if (have_rows('submenu-sections')):
	$sectionIndex = 0;
	while (have_rows('submenu-sections')) : the_row();
		$sectionIndex += 1;
		switch (get_row_layout()) :

			case 'cision_press':
				require 'sections/submenu-template/cision_press.php';
			break;

			case 'cision_reports':
				require 'sections/submenu-template/cision_reports.php';
			break;

			case 'contacts':
				require 'sections/submenu-template/contacts.php';
			break;

			case 'text_block':
				require 'sections/submenu-template/text_block.php';
			break;

			case 'cookie_opt':
				require 'sections/submenu-template/cookie_opt.php';
			break;

			case 'accordion':
				require 'sections/submenu-template/accordion.php';
			break;

		endswitch;
	endwhile;
endif;
?>
