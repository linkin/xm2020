<?php

$title = get_the_title();
$smallTitle = get_field('small_title');
$lead = get_field('lead');
$cta = get_field('cta');
$background_image = get_field('background_image');

if (get_field('alt_title')) {
	$title = get_field('alt_title');
}

?>

<div class="page-cover bg-image">
	<div class="cover-content">
    <div class="container container-cover-title">

      <?php if ($smallTitle) : ?>
        <p class="top-title"><?php echo $smallTitle; ?></p>
      <?php endif; ?>

      <?php if ($title) : ?>
        <h1><?php echo $title; ?></h1>
      <?php endif; ?>

      <?php if ($lead) : ?>
       <div class="lead"><p><?php echo $lead; ?></p></div>
      <?php endif; ?>

			<?php if ($cta) : ?>
				<?php while (have_rows('cta')) : the_row(); ?>

						<?php
						$buttonCSS = "btn btn-primary btn-xl mb-2";

						if (get_sub_field('outlined')){
							$buttonCSS = "btn btn-outline-primary btn-xl mb-2";
						}
						?>
						<a href="<?php the_sub_field('link'); ?>" class="<?php echo $buttonCSS; ?>">
							<?php the_sub_field('name'); ?>
						</a>

				<?php endwhile; ?>
			<?php endif; ?>




		</div>
	</div>



	<?php
		echo wp_get_attachment_image(
			$background_image['ID'],
			'large',
			'',
			array(
				'class'  => 'img-fluid image',
				'srcset' => $background_image['sizes']['large'] . ' 1024w,' . $background_image['sizes']['large'] . ' 768w,' . $background_image['sizes']['medium_large'] . ' 300w',
				'sizes'  => '(min-width: 991px) 1024px, 90vw'
			)
		);
	?>
</div>
