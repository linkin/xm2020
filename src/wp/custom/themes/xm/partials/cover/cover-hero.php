<?php

$title = get_the_title();
$smallTitle = get_field('small_title');
$lead = get_field('lead');

if (get_field('alt_title')) {
	$title = get_field('alt_title');
}

?>

<div class="hero">

  <div class="hero-content">
    <div class="container container-md">

      <?php if ($smallTitle) : ?>
        <p class="top-title"><?php echo $smallTitle; ?></p>
      <?php endif; ?>

      <?php if ($title) : ?>
        <h1><?php echo $title; ?></h1>
      <?php endif; ?>

      <?php if ($lead) : ?>
       <div class="lead"><p><?php echo $lead; ?></p></div>
      <?php endif; ?>

      <a href="#" class="btn btn-primary btn-lg btn-arrow" data-toggle="modal" data-target="#book-demo-modal-helpdesk">Try Now for free</a>
    </div>
  </div>

  <a class="scrollTo" href="#site-content"><div class="arrow-down"><span></span><span></span><span></span></div></a>

  <video src="https://player.vimeo.com/external/378535146.hd.mp4?s=9af3246e32feba445cb85c53d0f339ced161021d&amp;profile_id=175" autoplay="" playsinline="" loop="" muted=""></video>

</div>
<div class="usp-box">
	<div class="container">
		<div class="inner">
			<div class="row">

			<?php while (have_rows('usp')) : the_row(); ?>
				<div class="col-12 col-md">
					<div class="checklist">
						<strong><?php echo get_sub_field('title'); ?></strong>
						<p><?php echo get_sub_field('text'); ?></p>
					</div>
				</div>
			<?php endwhile; ?>

			</div>
		</div>
	</div>
</div>
