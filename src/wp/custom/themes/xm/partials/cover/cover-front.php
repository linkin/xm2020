<?php

$title = get_the_title();
$smallTitle = get_field('small_title');
$lead = get_field('lead');
$cta = get_field('cta');

if (get_field('alt_title')) {
    $title = get_field('alt_title');
}

?>

<style>

    #cover-image-frontpage {
        position: absolute;
        pointer-events: none;
        z-index: -1;
        opacity: 1;
        top: 50%;
        left: 50%;
        width: 100%;
        height: 100%;
        transform: translate(-50%, -50%);
        object-fit: cover;
        padding-top: 5rem;
        display: none;
        filter: brightness(50%);
    }
    #cover-image-frontpage-mobile {
        position: absolute;
        pointer-events: none;
        z-index: -1;
        opacity: 1;
        top: 50%;
        left: 50%;
        width: 100%;
        height: 100%;
        transform: translate(-50%, -50%);
        object-fit: cover;
        padding-top: 5rem;
        filter: brightness(70%);
    }

    .text-white {
        color: #FFF;
        text-align: left;
    }

    @media (max-width: 1100px) {


        .page-cover.front {
            padding-top: 9rem;
            padding-bottom: 9rem;
        }
    }

    @media (min-width: 1100px) {

        #cover-image-frontpage {
            display: block;
        }

        #cover-image-frontpage-mobile {
            display: none;
        }

        .page-cover.front {
            padding-top: 22rem;
            padding-bottom: 11rem;
        }
    }


</style>
<div class="page-cover front full-width-graphics text-white">
    <div class="cover-content">
        <div class="container container-lg">

            <?php if ($smallTitle) : ?>
                <p class="top-title"><?php echo $smallTitle; ?></p>
            <?php endif; ?>

            <?php if ($title) : ?>
                <h1><?php echo $title; ?></h1>
            <?php endif; ?>

            <?php if ($lead) : ?>
                <div class="lead"><?php echo $lead; ?></div>
            <?php endif; ?>

            <?php if ($cta) : ?>
                <?php while (have_rows('cta')) : the_row(); ?>

                    <?php
                    $buttonCSS = "btn btn-primary mb-2";

                    if (get_sub_field('outlined')){
                        $buttonCSS = "btn btn-outline-primary mb-2";
                    }
                    ?>
                    <a href="<?php the_sub_field('link'); ?>" class="<?php echo $buttonCSS; ?>">
                        <?php the_sub_field('name'); ?>
                    </a>

                <?php endwhile; ?>
            <?php endif; ?>


            <?php if (false) : ?>
                <div class="graphic-container">
                    <div class="graphics">
                        <div id="parallax5" class="square yellow medium green pos1 d-none d-lg-block"></div>
                        <div id="parallax6" class="square pink small blue pos2"></div>
                        <div id="parallax7" class="square small blue pos2"></div>
                        <div id="parallax8" class="square small lightblue pos2"></div>
                        <div class="dotts dottsarticle"></div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <img id="cover-image-frontpage" src="https://xmreality.com/custom/uploads/2022/02/XMReality_9nov_TekniskaVerken_0220-3-1536x1025.jpg">
    <img id="cover-image-frontpage-mobile" src="https://xmreality.com/custom/uploads/2022/02/XMReality_10nov_hololens_0357-4-768x1151.jpg">
</div>

<div class="usp-box">
    <div class="container">
        <div class="inner">
            <div class="row">

                <?php while (have_rows('usp')) : the_row(); ?>
                    <div class="col-12 col-md">
                        <div class="checklist">

                            <?php
                            $link = get_sub_field('link');
                            if( $link ){

                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>

                                <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                                    <strong><?php echo get_sub_field('title'); ?></strong>
                                    <p><?php echo get_sub_field('text'); ?></p>
                                </a>

                            <?php }else{ ?>
                                <strong><?php echo get_sub_field('title'); ?></strong>
                                <p><?php echo get_sub_field('text'); ?></p>
                            <?php } ?>


                        </div>
                    </div>
                <?php endwhile; ?>

            </div>
        </div>
    </div>
</div>
