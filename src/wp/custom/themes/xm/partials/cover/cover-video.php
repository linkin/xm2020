<?php

$title = get_the_title();
$smallTitle = get_field('small_title');
$lead = get_field('lead');
$cta = get_field('cta');

if (get_field('alt_title')) {
	$title = get_field('alt_title');
}

$coverClass = "page-cover";
$coverBackground = get_field('Background_color');

if ($coverBackground) {
	$coverClass = $coverClass . " " . $coverBackground;
}

?>

<div class="<?php echo $coverClass; ?>">
	<div class="cover-content">
    <div class="container container-md">

      <?php if ($smallTitle) : ?>
        <p class="top-title"><?php echo $smallTitle; ?></p>
      <?php endif; ?>

      <?php if ($title) : ?>
        <h1><?php echo $title; ?></h1>
      <?php endif; ?>

      <?php if ($lead) : ?>
       <div class="lead"><p><?php echo $lead; ?></p></div>
      <?php endif; ?>

			<?php if ($cta) : ?>
				<?php while (have_rows('cta')) : the_row(); ?>

						<?php
						$buttonCSS = "btn btn-primary btn-xl mb-2";

						if (get_sub_field('outlined')){
							$buttonCSS = "btn btn-outline-primary btn-xl mb-2";
						}
						?>
						<a href="<?php the_sub_field('link'); ?>" class="<?php echo $buttonCSS; ?>">
							<?php the_sub_field('name'); ?>
						</a>

				<?php endwhile; ?>
			<?php endif; ?>


		</div>
	</div>
  <div class="container container-md">
		<div class="videoWrapper smallVideo">


		<?php
		$url = get_field('video');
		preg_match('/src="(.+?)"/', $url, $matches_url );
		$src = $matches_url[1];

		preg_match('/embed(.*?)?feature/', $src, $matches_id );
		$id = $matches_id[1];
		$id = str_replace( str_split( '?/' ), '', $id );

		//var_dump( $id );
		//<img src="http://img.youtube.com/vi/ echo $id; /hqdefault.jpg">

    $iframe = get_field('video');

    echo $iframe;

    ?>
		</div>
	</div>
</div>
