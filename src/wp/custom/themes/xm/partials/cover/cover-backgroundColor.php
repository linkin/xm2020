<?php

$title = get_the_title();
$smallTitle = get_field('small_title');
$lead = get_field('lead');
$quick_menu = get_field('quick_menu');
$cta = get_field('cta');

$coverClass = "page-cover";
$coverBackground = get_field('Background_color');

if (get_field('alt_title')) {
	$title = get_field('alt_title');
}

if ($coverBackground) {
	$coverClass = $coverClass . " " . $coverBackground;
}

?>

<div class="<?php echo $coverClass; ?>">
	<div class="cover-content smallGraphics">
    <div class="container container-cover-title">

			<div class="graphic-container">

				<div class="graphics centerTop">
					<div id="" class="square small blue position4"></div>
				</div>

				<div class="to-front">
      <?php if ($smallTitle) : ?>
        <p class="top-title"><?php echo $smallTitle; ?></p>
      <?php endif; ?>

      <?php if ($title) : ?>
        <h1><?php echo $title; ?></h1>
      <?php endif; ?>

      <?php if ($lead) : ?>
       <div class="lead"><p><?php echo $lead; ?></p></div>
      <?php endif; ?>


			<?php if ($cta) : ?>
				<?php while (have_rows('cta')) : the_row(); ?>

						<?php
						$buttonCSS = "btn btn-primary btn-xl mb-2";

						if (get_sub_field('outlined')){
							$buttonCSS = "btn btn-outline-primary btn-xl mb-2";
						}
						?>
						<a href="<?php the_sub_field('link'); ?>" class="<?php echo $buttonCSS; ?>">
							<?php the_sub_field('name'); ?>
						</a>

				<?php endwhile; ?>
			<?php endif; ?>


      <?php if ($quick_menu) : ?>
				<ul class="nav mt-5 justify-content-center">
	 			<?php while (have_rows('quick_menu')) : the_row(); ?>

	 					<li class="nav-item"><a href="#<?php the_sub_field('anchorlink_id'); ?>" class="nav-link scrollTo"><?php the_sub_field('linkname'); ?></a></li>

	 			<?php endwhile; ?>
				</ul>
      <?php endif; ?>
		</div>



				<div class="graphics centerBottom">
					<div id="" class="square yellow medium green position1"></div>
					<div id="" class="square pink small blue position2"></div>
          <div class="dotts"></div>
				</div>
			</div>



		</div>
	</div>
</div>
