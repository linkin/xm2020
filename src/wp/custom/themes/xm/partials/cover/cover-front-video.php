<?php

$title = get_the_title();
$smallTitle = get_field('small_title');
$lead = get_field('lead');
$cta = get_field('cta');
$video_file = get_field('video_file');

if (get_field('alt_title')) {
	$title = get_field('alt_title');
}

?>

<div class="page-cover front video">
	<div class="cover-content">
    <div class="container container-md">

      <?php if ($smallTitle) : ?>
        <p class="top-title"><?php echo $smallTitle; ?></p>
      <?php endif; ?>

      <?php if ($title) : ?>
        <h1><?php echo $title; ?></h1>
      <?php endif; ?>

      <?php if ($lead) : ?>
       <div class="lead"><?php echo $lead; ?></div>
      <?php endif; ?>

      <?php if ($cta) : ?>
	 			<?php while (have_rows('cta')) : the_row(); ?>

						<?php
						$buttonCSS = "btn btn-primary btn-xl mb-2";

						if (get_sub_field('outlined')){
							$buttonCSS = "btn btn-outline-primary btn-xl mb-2";
						}
						?>
						<a href="<?php the_sub_field('link'); ?>" class="<?php echo $buttonCSS; ?>">
							<?php the_sub_field('name'); ?>
						</a>

	 			<?php endwhile; ?>
      <?php endif; ?>



		</div>
	</div>
	<video src="<?php echo $video_file; ?>" autoplay="" playsinline="" loop="" muted=""></video>
</div>

<div class="usp-box">
	<div class="container">
		<div class="inner">
			<div class="row">

			<?php while (have_rows('usp')) : the_row(); ?>
				<div class="col-12 col-md">
					<div class="checklist">

						<?php
						$link = get_sub_field('link');
						if( $link ){

				    $link_url = $link['url'];
				    $link_title = $link['title'];
				    $link_target = $link['target'] ? $link['target'] : '_self';
				    ?>

				    <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
						<strong><?php echo get_sub_field('title'); ?></strong>
						<p><?php echo get_sub_field('text'); ?></p>
						</a>

						<?php }else{ ?>
							<strong><?php echo get_sub_field('title'); ?></strong>
							<p><?php echo get_sub_field('text'); ?></p>
						<?php } ?>

					</div>
				</div>
			<?php endwhile; ?>

			</div>
		</div>
	</div>
</div>
