<?php

$title = get_the_title();
$smallTitle = get_field('small_title');
$lead = get_field('lead');
$content = get_field('content');
$image = get_field('image');
$cta = get_field('cta');
$round_image = get_field('round_image');

if (get_field('alt_title')) {
	$title = get_field('alt_title');
}

?>

<div class="page-cover text-and-image lightblue">
	<div class="cover-content">
    <div class="container">

			<div class="row row-lg align-items-center">

				<div class="card bg-transparent col-lg-6 order-lg-2 mb-5 mb-lg-0">

					<?php if ($round_image) { ?>
						<div class="card-img round-image">
						<?php
							echo wp_get_attachment_image(
								$image['ID'],
								'medium_large',
								'',
								array(
									'class'  => 'img-fluid image',
									'srcset' => $image['sizes']['large'] . ' 1024w,' . $image['sizes']['medium_large'] . ' 768w,' . $image['sizes']['medium'] . ' 300w',
									'sizes'  => '(min-width: 991px) 1024px, 90vw'
								)
							);
						?>
						</div>
					<?php } else {

						echo wp_get_attachment_image(
							$image['ID'],
							'large',
							'',
							array(
								'class'  => 'img-fluid image',
								'srcset' => $image['sizes']['large'] . ' 1024w,' . $image['sizes']['medium_large'] . ' 768w,' . $image['sizes']['medium'] . ' 300w',
								'sizes'  => '(min-width: 991px) 1024px, 90vw'
							)
						);

					} ?>

				</div>

				<div class="col-lg-6 text-left">

		      <?php if ($smallTitle) : ?>
		        <p class="top-title"><?php echo $smallTitle; ?></p>
		      <?php endif; ?>

		      <?php if ($title) : ?>
		        <h1><?php echo $title; ?></h1>
		      <?php endif; ?>

		      <?php if ($lead) : ?>
		       <div class="lead"><p><?php echo $lead; ?></p></div>
		      <?php endif; ?>

		      <?php if ($content) : ?>
		       <div class="mb-4"><p><?php echo $content; ?></p></div>
		      <?php endif; ?>

		      <?php if ($cta) : ?>
			 			<?php while (have_rows('cta')) : the_row(); ?>

								<?php
								$buttonCSS = "btn btn-primary btn-xl mb-2";

								if (get_sub_field('outlined')){
									$buttonCSS = "btn btn-outline-primary btn-xl mb-2";
								}
								?>
								<a href="<?php the_sub_field('link'); ?>" class="<?php echo $buttonCSS; ?>">
									<?php the_sub_field('name'); ?>
								</a>

			 			<?php endwhile; ?>
		      <?php endif; ?>

				</div>

			</div>

		</div>
	</div>
</div>
