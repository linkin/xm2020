<?php

$title = get_the_title();
$smallTitle = get_field('small_title');
$lead = get_field('lead');
$cta = get_field('cta');
$backgroundGraphics = get_field_object('graphics')['value'];

if (get_field('alt_title')) {
	$title = get_field('alt_title');
}

?>

<div class="">
<div class="page-cover simple">
	<div class="cover-content">
    <div class="container container-cover-title">

      <?php if ($smallTitle) : ?>
        <p class="top-title"><?php echo $smallTitle; ?></p>
      <?php endif; ?>

      <?php if ($title) : ?>
        <h1><?php echo $title; ?></h1>
      <?php endif; ?>

      <?php if ($lead) : ?>
       <div class="lead"><p><?php echo $lead; ?></p></div>
      <?php endif; ?>

      <?php if ($cta) : ?>
	 			<?php while (have_rows('cta')) : the_row(); ?>

						<?php
						$buttonCSS = "btn btn-primary btn-xl mb-2";

						if (get_sub_field('outlined')){
							$buttonCSS = "btn btn-outline-primary btn-xl mb-2";
						}
						?>
						<a href="<?php the_sub_field('link'); ?>" class="<?php echo $buttonCSS; ?>">
							<?php the_sub_field('name'); ?>
						</a>

	 			<?php endwhile; ?>
      <?php endif; ?>

		</div>
	</div>
</div>

<?php if ($backgroundGraphics == "simple-cover") : ?>
	<div class="graphic-container">
	<div class="graphics <?php echo $backgroundGraphics; ?>">
		<div id="" class="square yellow medium green position1"></div>
		<div id="" class="square small pink position2"></div>
		<div id="" class="square small blue position3"></div>
		<div id="" class="square small blue position4"></div>
		<div class="dotts"></div>
	</div>
	</div>
<?php endif; ?>

<?php if ($backgroundGraphics == "simple-cover-only-top") : ?>
	<div class="graphic-container">
	<div class="graphics <?php echo $backgroundGraphics; ?>">
		<div id="" class="square small blue position4"></div>
	</div>
	</div>
<?php endif; ?>
</div>
