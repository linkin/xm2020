<?php
/**
 * The template for displaying single video.
 *
 * @package XMreality
 * @since Twenty Fifteen 1.1
 */


get_header();



if ( have_posts() ) :

    while( have_posts()) : the_post();

    ?>

    <div class="page-cover article-cover no-cover-image full-width-graphics lightblue">
    	<div class="cover-content">
        	<div class="container container-xl">

				<div class="row row-lg">
					<div class="col-md-7 offset-md-2">
						<h1><?php the_title(); ?></h1>
					</div>
				</div><!-- /.row -->

				<div class="graphic-container">
					<div class="graphics">
						<div id="parallax5" class="square yellow medium green pos1"></div>
						<div id="parallax6" class="square pink small blue pos2"></div>
						<div id="parallax7" class="square small blue pos2"></div>
						<div id="parallax8" class="square small lightblue pos2"></div>
						<div class="dotts dottsarticle"></div>
					</div>
				</div><!-- /.graphic-container -->

    		</div><!-- /.container -->
    	</div>
    </div><!-- /.page-cover  -->


    <div class="page-article">
		<div class="container container-xl">
			<div class="row row-lg">

				<div class="col-md-7 offset-md-2">
					<div class="page-article-inner">

						<?php if ( ! post_password_required() ) : ?>

							<?php the_content(); ?>

							<?php if ( get_field( 'video' ) ) : ?>
								<div class="videoWrapper"><?php the_field( 'video' ); ?></div>
							<?php endif; ?>

							<?php if ( get_field( 'video_text' ) ) : ?>
								<p><?php the_field( 'video_text' ); ?></p>
							<?php endif; ?>

						<?php endif; ?>

						<hr class="end"/>

						<p class="small-text mb-1">SHARE ON SOCIAL MEDIA</p>
						<div class="social-share">
							<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink() ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
							<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink() ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
							<a href="https://twitter.com/home?status=<?php echo get_permalink() ?>" target="_blank"><i class="fab fa-twitter"></i></a>
							<a href="mailto:info@example.com?&subject=&body=<?php echo get_permalink() ?>" target="_blank"><i class="fa fa-envelope"></i></a>
							<a href="javascript:void(0)" onclick="CopyURL();"><i class="fa fa-link"></i><span class="text">Copy link</span></a>
						</div>

					</div><!-- /.page-article-inner -->
				</div>

				<div class="col-md-3">
					<div class="right-column">
						<?php the_field('right_column_article', 'options'); ?>
					</div>
				</div>

			</div><!-- /.row -->
		</div><!-- /.container -->
    </div><!-- /.page-article -->

<?php
	endwhile; // while have_posts.
endif; // if have_posts.
?>


<?php get_footer();