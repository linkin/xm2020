
jQuery(function ($) {

    var isMobile = false;
    if (navigator.userAgent.match(/(iPod|iPhone)/)) {
        isMobile = true;
        $("video").each(function () {
            $(this).get(0).play();
        });
    }

    function onCookieOptIn() {

        $('#cookie-bar').addClass('shown');
        window.localStorage.setItem('cookie-accept', (Date.now() / 1000) + (3600 * 7 * 365));

        var $cookieOpt = $('.cookie-opt');
        if ($cookieOpt.length) {
            $cookieOpt.find('.cookie-opt-out').removeClass('d-none');
            $cookieOpt.find('.cookie-opt-in').addClass('d-none');
        }

        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WCLV955');

    }

    function onCookieOptOut() {
        $('#cookie-bar').removeClass('shown');
        window.localStorage.removeItem('cookie-accept');

        var $cookieOpt = $('.cookie-opt');
        if ($cookieOpt.length) {
            $cookieOpt.find('.cookie-opt-in').removeClass('d-none');
            $cookieOpt.find('.cookie-opt-out').addClass('d-none');
        }

        document.cookie = '_ga=; path=/; domain=.xmreality.com; expires=' + new Date(0).toUTCString();
        document.cookie = '_gid=; path=/; domain=.xmreality.com; expires=' + new Date(0).toUTCString();
        document.cookie = '_gat_UA-30215882-1=; path=/; domain=.xmreality.com; expires=' + new Date(0).toUTCString();
        console.log('out');
    }



    $('#cookie-accept').click(function () {
          onCookieOptIn();
    });

    $('#cookie-decline').click(function() {
          onCookieOptOut();
    });

    if (!window.localStorage.getItem('cookie-accept') || (Date.now() / 1000) > parseInt(window.localStorage.getItem('cookie-accept'))) {
        onCookieOptOut();

    } else {
        onCookieOptIn();
    }

    $('.cookie-opt-in-button').click(function() {
        onCookieOptIn();
    });

    $('.cookie-opt-out-button').click(function() {
        onCookieOptOut();
    });

    $('.section-video').viewportChecker({
        classToAdd: "in",
        offset: '15%',
        repeat: true,
        callbackFunction: function (elem, action) {

            if ($(elem).attr('data') == 'video') {

                var myVideo = $(elem).find("video").get(0);

                if ((action == "add") && !isMobile) {
                    if (Math.floor(myVideo.currentTime) == 0) {
                        myVideo.play();
                    }
                } else {
                    myVideo.pause();
                    myVideo.currentTime = 0;
                }
            }
        }
    });

  	$(".scrollTo").click( function(event){
  		event.preventDefault();

      var extraMargin = 87;

      if (isMobile){
        extraMargin = 60;
      }

  		$('html, body').stop(true, true).animate({
          scrollTop: $( $.attr(this, 'href') ).offset().top - extraMargin
      	}, 1200);

  		return false;
  	});


  	$(".sub-menu a[href^='#']").click( function(event){
  		event.preventDefault();

      var extraMargin = 137;//137

      if (isMobile){
        extraMargin = 87;
      }

  		$('html, body').stop(true, true).animate({
          scrollTop: $( $.attr(this, 'href') ).offset().top - extraMargin
      	}, 1200);
        console.log(extraMargin);
  		return false;
  	});


    $("#menuToggle").click(function () {
        $(this).toggleClass("toggled");
        $(".site-header").toggleClass("open");
    });

    /* $(".has-menu > a").click(function (e) {

        e.preventDefault();

        if ($(".site-header").hasClass("open")) {
          $(this).parent().toggleClass("open");
        }
    }); */

    $(".submenu-arrow").click(function (e) {

        e.preventDefault();

        if ($(".site-header").hasClass("open")) {
          $(this).parent().toggleClass("open");
        }
    });


    function stickyHeader() {
        var $this = $(this),
            $head = $('.site-header');
        if ($this.scrollTop() > 42) {
            $head.addClass('sticky');
        } else {
            $head.removeClass('sticky');
        }
    }

    $(window).scroll(function () {
        stickyHeader();
    });

    stickyHeader();

    $('.tooltip').tooltipster({
        contentCloning: true,
        side: 'right',
        size: {
          width: '300px'
        },
        theme: 'tooltipster-light'
    });

    $('.slider').slick({
        dots: true,
        arrows: true,
        adaptiveHeight: true,
        prevArrow: '<button class="slick-prev"><i class="fal fa-arrow-left"></i></button>',
        nextArrow: '<button class="slick-next"><i class="fal fa-arrow-right"></i></button>'
    });


    $('.article-slider').slick({
        dots: false,
        arrows: true,
        slidesToShow: 3,
        adaptiveHeight: true,
        prevArrow: '<button class="slick-prev small-prev"><i class="fal fa-arrow-left"></i></button>',
        nextArrow: '<button class="slick-next small-next"><i class="fal fa-arrow-right"></i></button>',
        responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1,
            arrows: false,
            slidesToScroll: 1,
            infinite: true,
            dots:true
          }
        }
      ]
    });


    $('.stories-slider').slick({
        dots: true,
        arrows: false,
        slidesToShow: 2,
        adaptiveHeight: true,
        prevArrow: '<button class="slick-prev small-prev"><i class="fal fa-arrow-left"></i></button>',
        nextArrow: '<button class="slick-next small-next"><i class="fal fa-arrow-right"></i></button>',
        responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1,
            arrows: false,
            slidesToScroll: 1,
            infinite: true,
            dots:true
          }
        }
      ]
    });



    $(".feature a").click(function(e) {
        e.preventDefault();
        var imageElement = "#feature-image-" + $(this).attr("data-id");

        $("#screens .content").removeClass("show");
        $(imageElement).addClass("show");

        $("#feature-list-left a").removeClass("active");
        $("#feature-list-right a").removeClass("active");
        $(this).addClass("active");

        var myVideo = $(imageElement).find("video").get(0);

        $("#screens").find("video").get(0).currentTime = 0;

        if (!isMobile) {
            if (Math.floor(myVideo.currentTime) == 0) {
                myVideo.play();
                console.log("first");
            }else{
                myVideo.currentTime = 0;
                myVideo.play();
                console.log("second");
            }
        } else {
        }
    });



    $(".pricelist_feature_header").click(function(e) {
        e.preventDefault();
        var pricelistFeatures = $(this).attr("data-comparison-feature-trigger");

        $('#features-list .feature[data-comparison-feature-group-id=' + pricelistFeatures + ']').toggleClass("show");
        $('#features-list .feature[data-comparison-feature-group-id=' + pricelistFeatures + ']').toggleClass("fadeInFast");
        $(this).toggleClass("active");


    });

    $("#expand-pricetable").click(function(e) {
        e.preventDefault();

        $('#comparison-table').addClass("showAll");
    });


    var element = document.querySelector('#tabNav');

    if (element) {
      if (element.offsetWidth < element.scrollWidth) {
        $(element).parent().addClass("has-scroll");
      } else {
        $(element).parent().removeClass("has-scroll");
      }
    }

    $('#tabNav a').on('click', function (e) {

        var contentElement = "#tab-content-" + $(this).attr("data-id");
        var imageElement = "#tab-image-" + $(this).attr("data-id");


        if (!$(this).hasClass("active")) {

          $(".tab-pane").removeClass("show");
          $("#tabNav a.active").removeClass("active");
          $(this).addClass("active");
          $(contentElement).addClass("show");
          $(imageElement).addClass("show");


          var myVideo = $(imageElement).find("video").get(0);

          $("#pills-tabContent").find("video").get(0).currentTime = 0;

          if (!isMobile) {
              if (Math.floor(myVideo.currentTime) == 0) {
                  myVideo.play();
                  console.log("first");
              }else{
                  myVideo.currentTime = 0;
                  myVideo.play();
                  console.log("second");
              }
          } else {
          }

        }


      e.preventDefault()
    })


    $('.next-tab').on('click', function (e) {

      var dataId = $(this).attr("data-id");
      $('#tabNav a[data-id=' + dataId + ']').trigger('click');

      e.preventDefault()
    })

    $(".nav-pills-toggle li a").click(function(e){
      e.preventDefault();
      if ($(this).hasClass("default")) {
        $(".nav-pills-toggle li a").removeClass("active");
        $(this).addClass("active");
      } else {
        $(".nav-pills-toggle li a.default").removeClass("active");
        $(this).toggleClass("active");
      }
    })


    if ($('#trigger-parallax1').length){

    var controllerParallax = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

  	new ScrollMagic.Scene({triggerElement: "#trigger-parallax1", offset: "0"})
		.setTween("#parallax1", {y: "600", ease: Linear.easeNone})
		.addTo(controllerParallax);

  	new ScrollMagic.Scene({triggerElement: "#trigger-parallax1", offset: "50"})
		.setTween("#parallax2", {y: "300%", ease: Linear.easeNone})
		.addTo(controllerParallax);

  	new ScrollMagic.Scene({triggerElement: "#trigger-parallax1", offset: "0"})
		.setTween("#parallax3", {y: "800", ease: Linear.easeNone})
		.addTo(controllerParallax);

  	new ScrollMagic.Scene({triggerElement: "#trigger-parallax1", offset: "700"})
		.setTween("#parallax4", {y: "300%", ease: Linear.easeNone})
		.addTo(controllerParallax);
    }



});

function CopyURL(){
  var dummy = document.createElement('input'),
  text = window.location.href;

  document.body.appendChild(dummy);
  dummy.value = text;
  dummy.select();
  document.execCommand('copy');
  document.body.removeChild(dummy);
  return false;
}
